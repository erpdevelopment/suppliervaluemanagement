package com.oracle.mbc.svm.model.eo;

import com.oracle.mbc.svm.model.vo.svmEvaluationvoRowImpl;

import java.math.BigDecimal;

import java.util.Map;

import oracle.adf.share.ADFContext;

import oracle.jbo.AttributeList;
import oracle.jbo.Key;
import oracle.jbo.domain.Number;
import oracle.jbo.server.EntityDefImpl;
import oracle.jbo.server.EntityImpl;
import oracle.jbo.server.SequenceImpl;
import oracle.jbo.server.TransactionEvent;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Sun May 20 14:55:46 GST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class svmEvalEvaluatorsEOImpl extends EntityImpl {
    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        EvalEmployeeId,
        FkEvalId,
        FkEmployeeId,
        ResponseCompleted,
        Branch,
        svmEvaluationvo;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }


    public static final int EVALEMPLOYEEID = AttributesEnum.EvalEmployeeId.index();
    public static final int FKEVALID = AttributesEnum.FkEvalId.index();
    public static final int FKEMPLOYEEID = AttributesEnum.FkEmployeeId.index();
    public static final int RESPONSECOMPLETED = AttributesEnum.ResponseCompleted.index();
    public static final int BRANCH = AttributesEnum.Branch.index();
    public static final int SVMEVALUATIONVO = AttributesEnum.svmEvaluationvo.index();

    /**
     * This is the default constructor (do not remove).
     */
    public svmEvalEvaluatorsEOImpl() {
    }

    /**
     * @return the definition object for this instance class.
     */
    public static synchronized EntityDefImpl getDefinitionObject() {
        return EntityDefImpl.findDefObject("com.oracle.mbc.svm.model.eo.svmEvalEvaluatorsEO");
    }


    /**
     * Gets the attribute value for EvalEmployeeId, using the alias name EvalEmployeeId.
     * @return the value of EvalEmployeeId
     */
    public oracle.jbo.domain.Number getEvalEmployeeId() {
        return (oracle.jbo.domain.Number) getAttributeInternal(EVALEMPLOYEEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for EvalEmployeeId.
     * @param value value to set the EvalEmployeeId
     */
    public void setEvalEmployeeId(oracle.jbo.domain.Number value) {
        setAttributeInternal(EVALEMPLOYEEID, value);
    }

    /**
     * Gets the attribute value for FkEvalId, using the alias name FkEvalId.
     * @return the value of FkEvalId
     */
    public BigDecimal getFkEvalId() {
        return (BigDecimal) getAttributeInternal(FKEVALID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FkEvalId.
     * @param value value to set the FkEvalId
     */
    public void setFkEvalId(BigDecimal value) {
        setAttributeInternal(FKEVALID, value);
    }

    /**
     * Gets the attribute value for FkEmployeeId, using the alias name FkEmployeeId.
     * @return the value of FkEmployeeId
     */
    public BigDecimal getFkEmployeeId() {
        return (BigDecimal) getAttributeInternal(FKEMPLOYEEID);
    }

    /**
     * Sets <code>value</code> as the attribute value for FkEmployeeId.
     * @param value value to set the FkEmployeeId
     */
    public void setFkEmployeeId(BigDecimal value) {
        setAttributeInternal(FKEMPLOYEEID, value);
    }

    /**
     * Gets the attribute value for ResponseCompleted, using the alias name ResponseCompleted.
     * @return the value of ResponseCompleted
     */
    public String getResponseCompleted() {
        return (String) getAttributeInternal(RESPONSECOMPLETED);
    }

    /**
     * Sets <code>value</code> as the attribute value for ResponseCompleted.
     * @param value value to set the ResponseCompleted
     */
    public void setResponseCompleted(String value) {
        setAttributeInternal(RESPONSECOMPLETED, value);
    }


    /**
     * Gets the attribute value for Branch, using the alias name Branch.
     * @return the value of Branch
     */
    public String getBranch() {
        return (String) getAttributeInternal(BRANCH);
    }

    /**
     * Sets <code>value</code> as the attribute value for Branch.
     * @param value value to set the Branch
     */
    public void setBranch(String value) {
        setAttributeInternal(BRANCH, value);
    }

    /**
     * Uses the link svmevalevaluatorvl to return rows of svmEvalEvaluatorsvo
     */
    public svmEvaluationvoRowImpl getsvmEvaluationvo() {
        return (svmEvaluationvoRowImpl) getAttributeInternal(SVMEVALUATIONVO);
    }


    /**
     * @param evalEmployeeId key constituent

     * @return a Key object based on given key constituents.
     */
    public static Key createPrimaryKey(Number evalEmployeeId) {
        return new Key(new Object[] { evalEmployeeId });
    }

    /**
     * Add attribute defaulting logic in this method.
     * @param attributeList list of attribute names/values to initialize the row
     */
    protected void create(AttributeList attributeList) {
        super.create(attributeList);
        SequenceImpl seq=new SequenceImpl("SVM_EVAL_EVALUATORS_seq", this.getDBTransaction());
        this.setEvalEmployeeId(seq.getSequenceNumber());
        this.setResponseCompleted("N");
        // Set Branch 
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        String BusinessUnit = (String) sessionscope.get("pBusinessUnit");
        this.setBranch(BusinessUnit);
    }

    /**
     * Add entity remove logic in this method.
     */
    public void remove() {
        super.remove();
    }

    /**
     * Add locking logic here.
     */
    public void lock() {
        super.lock();
    }

    /**
     * Custom DML update/insert/delete logic here.
     * @param operation the operation type
     * @param e the transaction event
     */
    protected void doDML(int operation, TransactionEvent e) {
        super.doDML(operation, e);
    }
}

