package com.oracle.mbc.svm.model.vo;

import com.oracle.mbc.svm.model.eo.svmEvaluationTemplateEOImpl;

import java.math.BigDecimal;

import oracle.jbo.Row;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ViewRowImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon May 21 17:33:05 GST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class svmEvaluationTemplatevoRowImpl extends ViewRowImpl {
    public static final int ENTITY_SVMEVALUATIONTEMPLATEEO = 0;

    /**
     * AttributesEnum: generated enum for identifying attributes and accessors. DO NOT MODIFY.
     */
    public enum AttributesEnum {
        EvalTemplateId,
        FkEvalId,
        FkTemplateId,
        QuestionValue,
        Indicator,
        svmEvaluationvo;
        private static AttributesEnum[] vals = null;
        private static final int firstIndex = 0;

        public int index() {
            return AttributesEnum.firstIndex() + ordinal();
        }

        public static final int firstIndex() {
            return firstIndex;
        }

        public static int count() {
            return AttributesEnum.firstIndex() + AttributesEnum.staticValues().length;
        }

        public static final AttributesEnum[] staticValues() {
            if (vals == null) {
                vals = AttributesEnum.values();
            }
            return vals;
        }
    }
    public static final int EVALTEMPLATEID = AttributesEnum.EvalTemplateId.index();
    public static final int FKEVALID = AttributesEnum.FkEvalId.index();
    public static final int FKTEMPLATEID = AttributesEnum.FkTemplateId.index();
    public static final int QUESTIONVALUE = AttributesEnum.QuestionValue.index();
    public static final int INDICATOR = AttributesEnum.Indicator.index();
    public static final int SVMEVALUATIONVO = AttributesEnum.svmEvaluationvo.index();

    /**
     * This is the default constructor (do not remove).
     */
    public svmEvaluationTemplatevoRowImpl() {
    }

    /**
     * Gets svmEvaluationTemplateEO entity object.
     * @return the svmEvaluationTemplateEO
     */
    public svmEvaluationTemplateEOImpl getsvmEvaluationTemplateEO() {
        return (svmEvaluationTemplateEOImpl) getEntity(ENTITY_SVMEVALUATIONTEMPLATEEO);
    }

    /**
     * Gets the attribute value for EVAL_TEMPLATE_ID using the alias name EvalTemplateId.
     * @return the EVAL_TEMPLATE_ID
     */
    public Number getEvalTemplateId() {
        return (Number) getAttributeInternal(EVALTEMPLATEID);
    }

    /**
     * Sets <code>value</code> as attribute value for EVAL_TEMPLATE_ID using the alias name EvalTemplateId.
     * @param value value to set the EVAL_TEMPLATE_ID
     */
    public void setEvalTemplateId(Number value) {
        setAttributeInternal(EVALTEMPLATEID, value);
    }

    /**
     * Gets the attribute value for FK_EVAL_ID using the alias name FkEvalId.
     * @return the FK_EVAL_ID
     */
    public Number getFkEvalId() {
        return (Number) getAttributeInternal(FKEVALID);
    }

    /**
     * Sets <code>value</code> as attribute value for FK_EVAL_ID using the alias name FkEvalId.
     * @param value value to set the FK_EVAL_ID
     */
    public void setFkEvalId(Number value) {
        setAttributeInternal(FKEVALID, value);
    }

    /**
     * Gets the attribute value for FK_TEMPLATE_ID using the alias name FkTemplateId.
     * @return the FK_TEMPLATE_ID
     */
    public Number getFkTemplateId() {
        return (Number) getAttributeInternal(FKTEMPLATEID);
    }

    /**
     * Sets <code>value</code> as attribute value for FK_TEMPLATE_ID using the alias name FkTemplateId.
     * @param value value to set the FK_TEMPLATE_ID
     */
    public void setFkTemplateId(Number value) {
        setAttributeInternal(FKTEMPLATEID, value);
    }

    /**
     * Gets the attribute value for the calculated attribute QuestionValue.
     * @return the QuestionValue
     */
    public String getQuestionValue() {
        return (String) getAttributeInternal(QUESTIONVALUE);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute QuestionValue.
     * @param value value to set the  QuestionValue
     */
    public void setQuestionValue(String value) {
        setAttributeInternal(QUESTIONVALUE, value);
    }

    /**
     * Gets the attribute value for the calculated attribute Indicator.
     * @return the Indicator
     */
    public String getIndicator() {
        return (String) getAttributeInternal(INDICATOR);
    }

    /**
     * Sets <code>value</code> as the attribute value for the calculated attribute Indicator.
     * @param value value to set the  Indicator
     */
    public void setIndicator(String value) {
        setAttributeInternal(INDICATOR, value);
    }

    /**
     * Gets the associated <code>Row</code> using master-detail link svmEvaluationvo.
     */
    public Row getsvmEvaluationvo() {
        return (Row) getAttributeInternal(SVMEVALUATIONVO);
    }

    /**
     * Sets the master-detail link svmEvaluationvo between this object and <code>value</code>.
     */
    public void setsvmEvaluationvo(Row value) {
        setAttributeInternal(SVMEVALUATIONVO, value);
    }
}

