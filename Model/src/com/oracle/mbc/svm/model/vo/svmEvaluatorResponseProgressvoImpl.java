package com.oracle.mbc.svm.model.vo;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Mon Jun 04 14:34:25 GST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class svmEvaluatorResponseProgressvoImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public svmEvaluatorResponseProgressvoImpl() {
    }

    /**
     * Returns the bind variable value for userid.
     * @return bind variable value for userid
     */
    public String getuserid() {
        return (String) getNamedWhereClauseParam("userid");
    }

    /**
     * Sets <code>value</code> for bind variable userid.
     * @param value value to bind as userid
     */
    public void setuserid(String value) {
        setNamedWhereClauseParam("userid", value);
    }

    /**
     * Returns the bind variable value for evalid.
     * @return bind variable value for evalid
     */
    public String getevalid() {
        return (String) getNamedWhereClauseParam("evalid");
    }

    /**
     * Sets <code>value</code> for bind variable evalid.
     * @param value value to bind as evalid
     */
    public void setevalid(String value) {
        setNamedWhereClauseParam("evalid", value);
    }


    /**
     * Returns the bind variable value for supplierid.
     * @return bind variable value for supplierid
     */
    public String getsupplierid() {
        return (String) getNamedWhereClauseParam("supplierid");
    }

    /**
     * Sets <code>value</code> for bind variable supplierid.
     * @param value value to bind as supplierid
     */
    public void setsupplierid(String value) {
        setNamedWhereClauseParam("supplierid", value);
    }

    /**
     * Returns the bind variable value for pBusinessUnit.
     * @return bind variable value for pBusinessUnit
     */
    public String getpBusinessUnit() {
        return (String) getNamedWhereClauseParam("pBusinessUnit");
    }

    /**
     * Sets <code>value</code> for bind variable pBusinessUnit.
     * @param value value to bind as pBusinessUnit
     */
    public void setpBusinessUnit(String value) {
        setNamedWhereClauseParam("pBusinessUnit", value);
    }
}

