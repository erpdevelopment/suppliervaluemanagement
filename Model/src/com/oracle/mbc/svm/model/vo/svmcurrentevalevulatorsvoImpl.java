package com.oracle.mbc.svm.model.vo;

import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Wed Nov 14 13:29:06 GST 2018
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class svmcurrentevalevulatorsvoImpl extends ViewObjectImpl {
    /**
     * This is the default constructor (do not remove).
     */
    public svmcurrentevalevulatorsvoImpl() {
    }

    /**
     * Returns the bind variable value for v_userid.
     * @return bind variable value for v_userid
     */
    public String getv_userid() {
        return (String) getNamedWhereClauseParam("v_userid");
    }

    /**
     * Sets <code>value</code> for bind variable v_userid.
     * @param value value to bind as v_userid
     */
    public void setv_userid(String value) {
        setNamedWhereClauseParam("v_userid", value);
    }


    /**
     * Returns the bind variable value for evalid.
     * @return bind variable value for evalid
     */
    public String getevalid() {
        return (String) getNamedWhereClauseParam("evalid");
    }

    /**
     * Sets <code>value</code> for bind variable evalid.
     * @param value value to bind as evalid
     */
    public void setevalid(String value) {
        setNamedWhereClauseParam("evalid", value);
    }

    /**
     * Returns the bind variable value for pBusinessUnit.
     * @return bind variable value for pBusinessUnit
     */
    public String getpBusinessUnit() {
        return (String) getNamedWhereClauseParam("pBusinessUnit");
    }

    /**
     * Sets <code>value</code> for bind variable pBusinessUnit.
     * @param value value to bind as pBusinessUnit
     */
    public void setpBusinessUnit(String value) {
        setNamedWhereClauseParam("pBusinessUnit", value);
    }
}

