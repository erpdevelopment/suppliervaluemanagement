package com.oracle.mbc.svm.view.servlets;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.sql.Blob;
import java.sql.Clob;

import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.http.*;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;

public class ImageServlet extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
   

        try {
            HttpSession session = request.getSession(true);
            String userId=(String)session.getAttribute("userId");
            getImage(userId,request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    public static void getImage(String userId ,HttpServletRequest request,HttpServletResponse response) throws IOException, SQLException {
        OutputStream os=response.getOutputStream();
      BindingContext bindingContext = BindingContext.getCurrent();
      DCBindingContainer bindings = bindingContext.findBindingContainer("ServletDef");
      
      OperationBinding oper = (OperationBinding)bindings.getOperationBinding("getImage");
      oper.getParamsMap().put("userId",userId);
      oper.invoke(); 
      
        Blob img = (Blob)oper.getResult();
         if (img!=null)
         {
             System.out.println("inside  image servelet");
        BufferedInputStream in = new BufferedInputStream(img.getBinaryStream());
        int b;
        byte [] buffer =new byte[10240];
        
            while ((b = in.read(buffer, 0, 10240)) != -1) {
                os.write(buffer, 0, b);

            }
       
        os.close();
        in.close();
         }
         else
         {
             //System.out.println("serv"+ request.getServletPath());
             System.out.println("inside else image servelet");
                 ServletContext servletCtx = request.getServletContext();
                 InputStream is = servletCtx.getResourceAsStream("/resources/assets/layouts/layout/img/avatar3_small.png");
                 BufferedInputStream in = new BufferedInputStream(is);
                 int b;
                 byte[] buffer = new byte[10240];
                 while ((b = in.read(buffer, 0, 10240)) != -1) {
                     os.write(buffer, 0, b);
                 }
                 os.close();
                in.close();
             }
   
    }
    
}
