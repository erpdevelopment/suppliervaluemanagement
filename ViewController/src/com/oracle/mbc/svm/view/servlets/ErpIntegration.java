package com.oracle.mbc.svm.view.servlets;


import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import java.sql.*;

import java.util.Map;

import javax.servlet.*;
import javax.servlet.http.*;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;

import oracle.adf.model.binding.DCBindingContainer;

import oracle.jdbc.OracleDriver;

public class ErpIntegration extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession(true);
       // String uri = req.getRequestURI();
        String icxSessionId = request.getParameter("ICXSESSIONID");
        String isAdmin = request.getParameter("pProfileSvm");
        String respId = request.getParameter("pRespId");
        String BusinessUnit=request.getParameter("pBusinessUnit");
        if (icxSessionId==null ||isAdmin == null ||respId==null  )
        {
             
                showError("UnAuthorized", out);
            }
        else
        {
            if (session!=null)
            {
            // check icx session
                String userId=null;
                String userName=null;
                String userData=checkICXSession(icxSessionId);
                if ("N".equalsIgnoreCase(userData))
                {
                        session.removeAttribute("icxSessionId");
                        session.removeAttribute("svmMenu");
                        showError("Invalid Session", out);
                       
                    return;
                    }
                else
                {
                if (userData!=null)
                    {
                        userId=userData.split("!")[0];
                        userName=userData.split("!")[1];
                        System.out.println("id is "+userId);
                        System.out.println("Name is "+userName);
                    }
                    }
                
            
            // get menu
               
               String data= getMenue(respId);
                String menu=data.split("!")[0];
             
                String pages=data.split("!")[1];
             
           
                
          
                
                
            //set sesson attributes 
            session.setAttribute("svmMenu", menu);
            session.setAttribute("pages", pages); 
            session.setAttribute("userId", userId);
            session.setAttribute("userName", userName);    
            session.setAttribute("isAdmin", isAdmin);
            session.setAttribute("respId", respId);  
            session.setAttribute("icxSessionId", icxSessionId);
           session.setAttribute("pBusinessUnit",BusinessUnit);
           // session.setAttribute("img", img); 
            out.close();
            response.sendRedirect(request.getContextPath() + "/faces/pages/Home.jsf");    
            }
            else
            {
                  showError("No Session", out);
                   return;
                }
        }
        
        
        
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>ErpIntegration</title></head>");
        out.println("<body>");
        out.println("<p>The servlet has received a POST. This is the reply.</p>");
        out.println("</body></html>");
        out.close();
    }

    
    public static String checkICXSession(String pICXsession)
    {
        
       
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer bindings = bindingContext.findBindingContainer("ServletDef");
        
        OperationBinding oper = (OperationBinding)bindings.getOperationBinding("checkICXSession");
        oper.getParamsMap().put("pICXsession",pICXsession);
        oper.invoke(); 
        String result = (String)oper.getResult();
        System.out.println("end check");
        return result;
    }
 

   // public static void main(String[] args) {
       //String x=getMenue("");
      
      // String menu=(x.split("!"))[0];
   //  String pages=x.split("!")[1];
    //  System.out.println("x"+pages);
    //    String p="/svm/faces/pages/Home.jsf";
    //    String all=";#AllQuestions.jsf#,#AllEvals.jsf#,#AllSurveys.jsf#,#EvalScoreSummary.jsf#,#EvaluationReport.jsf#,";
     //   String currentPage=p.substring(p.lastIndexOf('/') + 1);
      //      System.out.println(currentPage);
       //     System.out.println(all.contains("AllEvalsx.jsf"));
   //}
  
      public static String getMenue(String pRespid)
    {
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer bindings = bindingContext.findBindingContainer("ServletDef");
        
        OperationBinding oper = (OperationBinding)bindings.getOperationBinding("getMenue");
        oper.getParamsMap().put("pRespid",pRespid);
        oper.invoke(); 
        String result = (String)oper.getResult();
        System.out.println("end get menue");
        return result;     
    }
      


    
    public static void showError(String errorMessage , PrintWriter out)
    {
        
        //PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Error</title></head>");
        out.println("<body>");
        out.println("<H1>"+errorMessage+"</H1>");
        out.println("</body></html>");
        out.close();
    }
}