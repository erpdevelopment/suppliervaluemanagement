package com.oracle.mbc.svm.view.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.*;
import javax.servlet.http.*;

public class SvmValidation extends HttpServlet {
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        /// get all paramters
        //check with notification id  
        //if exist go to eval form view 
               
       // HttpSession session = request.getSession(true);
        String vntfId = request.getParameter("ntfId");
        String vittype = request.getParameter("ittype");
        String vitkey = request.getParameter("itkey");
        String vshowSend=request.getParameter("showsend");
        String vevalId=request.getParameter("evId");;
        String vusrId=request.getParameter("uId");
        String vssId=request.getParameter("ssId");
        String Businessunit=  "MBC - "+request.getParameter("pBusinessUnit");
        //set sesson attributes 
//        session.setAttribute("svmMenu", menu);

//        out.close();
       if(vittype.equals("SVM_EVAL"))
       {
       response.sendRedirect(request.getContextPath() + "/faces/EvalForm?evId="+vevalId+"&uId="+vusrId+"&ssId="+vssId+"&ntfId="+vntfId+"&ittype="+vittype+"&itkey="+vitkey+"&showsend="+vshowSend+"&pBusinessUnit="+Businessunit); 
       }
        else 
       {
        showError("You are not Authorized to access this Notification", out);
       }
        
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>SvmValidation</title></head>");
        out.println("<body>");
        out.println("<p>The servlet has received a POST. This is the reply.</p>");
        out.println("</body></html>");
        out.close();
    }
    public static void showError(String errorMessage , PrintWriter out)
    {
        
        //PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head><title>Error</title></head>");
        out.println("<body>");
        out.println("<H1>"+errorMessage+"</H1>");
        out.println("</body></html>");
        out.close();
    }
}
