package com.oracle.mbc.svm.view.utils;

import javax.faces.context.FacesContext;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;

public class JSutils {
    public JSutils() {
        super();
    }
    public static void callJavaScriptMethod(String jsfunction )
    {
        
            FacesContext fctx = FacesContext.getCurrentInstance();
             ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);      
            erks.addScript(fctx, jsfunction);     
        
        }
    
    public static void callJavaScriptSuccessMessage( )
    {
        
            FacesContext fctx = FacesContext.getCurrentInstance();
             ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);      
            erks.addScript(fctx, "successlertmessage()"); 
            
        
        }
    
    public static void callJavaScriptWarningMessage( )
    {
        
            FacesContext fctx = FacesContext.getCurrentInstance();
             ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);      
            erks.addScript(fctx, "warninglertmessage()");     
        
        }
    public static void callJavaScriptErrorMessage( )
    {
        
            FacesContext fctx = FacesContext.getCurrentInstance();
             ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);      
            erks.addScript(fctx, "dangeralertmessage()");     
        
        }
    public static void callJavaScriptDeleteMessage( )
    {
        
            FacesContext fctx = FacesContext.getCurrentInstance();
             ExtendedRenderKitService erks = Service.getRenderKitService(fctx, ExtendedRenderKitService.class);      
            erks.addScript(fctx, "deletemessage()"); 
            
        
        }
    
    public static void showSweetAlertHtmlmessage(String title , String text , String type)
      {
          
        String className="";
         if ("success".equalsIgnoreCase(type))
         {
            className ="btn-success";
              text= "<ul class=\"msg-details\"><li><span>"+text+ "<li><span></ul>";
          }
         else if
         ("error".equalsIgnoreCase(type))
         {
              className ="btn-danger";
              text= "<ul class=\"msg-error-details\">"+text+ "</ul>";
          }
         else
         {
                 className ="btn-warning";
                 text= "<ul class=\"msg-details\"><li><span>"+text+ "<li><span></ul>";
             }
          StringBuilder alerttext=new StringBuilder();
             alerttext.append("customalertmessageHTML('");
             alerttext.append(title);
             alerttext.append("','");
             alerttext.append(text);
               alerttext.append("','");
               alerttext.append(type);
                   alerttext.append("','");
                      alerttext.append("N");
                  alerttext.append("','");
                  alerttext.append(className);
              alerttext.append("')");
            JSutils.callJavaScriptMethod(alerttext.toString());     
            System.out.println("h");
      }
  
}
