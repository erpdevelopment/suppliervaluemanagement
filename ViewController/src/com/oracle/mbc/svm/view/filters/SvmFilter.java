package com.oracle.mbc.svm.view.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import oracle.adf.model.BindingContext;
import oracle.adf.model.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;

public class SvmFilter implements Filter {
    private FilterConfig _filterConfig = null;

    public void init(FilterConfig filterConfig) throws ServletException {
        _filterConfig = filterConfig;
    }

    public void destroy() {
        _filterConfig = null;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
                                                                                                     ServletException {
       if (1==1)
       {
               chain.doFilter(request, response);
               return;
           }
        HttpServletRequest req = (HttpServletRequest)request;
        HttpServletResponse res = (HttpServletResponse)response;
        HttpSession session = req.getSession(false);
        String uri = req.getRequestURI();
        String userId= (String)session.getAttribute("userId"); 
        String pages= (String)session.getAttribute("pages"); 
        
     System.out.println("uri is "+uri);
                    if (uri.endsWith("EvalForm.jsf") || uri.endsWith("SurveyForm.jsf")||  uri.endsWith("EvalForm") ||  uri.endsWith("SurveyForm")  ||  uri.endsWith(".js") ||  uri.endsWith(".png") ||  uri.endsWith(".jpg") ||  uri.endsWith(".css")) 
                    {
                            chain.doFilter(request, response);
                            return;
                        }
                    
                if (session!=null)
                {
                       if (userId==null)
                        {
                                res.sendRedirect(req.getContextPath() + "/erpintegration"); 
                                return;
                        }
                       else
                       { 
                            
                           String icxSessionId= (String)session.getAttribute("icxSessionId");
                           String chk=checkICXSession(icxSessionId);
                           if ("N".equalsIgnoreCase(chk))
                           {
                                   res.sendRedirect(req.getContextPath() + "/erpintegration");
                                   return;
                               }
                           
                        
                           //check page access
                           String currentPage=uri.substring(uri.lastIndexOf('/') + 1);
                           currentPage="#"+currentPage+"#";
                           if (pages==null || !(pages.toUpperCase().contains(currentPage.toUpperCase())))
                           {
                                   res.sendRedirect(req.getContextPath() + "/erpintegration");
                                   return; 
                              
                               }
                       }
            }
            System.out.println("jjj");
        chain.doFilter(request, response);
    }
    
    public static String checkICXSession(String pICXsession)
    {
        
       
        BindingContext bindingContext = BindingContext.getCurrent();
        DCBindingContainer bindings = bindingContext.findBindingContainer("ServletDef");
        
        OperationBinding oper = (OperationBinding)bindings.getOperationBinding("checkICXSession");
        oper.getParamsMap().put("pICXsession",pICXsession);
        oper.invoke(); 
        String result = (String)oper.getResult();
        System.out.println("end check");
        return result;
    }
}
