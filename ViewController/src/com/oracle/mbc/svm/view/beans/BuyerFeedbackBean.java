package com.oracle.mbc.svm.view.beans;


import java.util.Map;

import javax.faces.event.ActionEvent;

import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class BuyerFeedbackBean {
    private String cost;
    private String delivery;
    private String quality;
    private String service;
    private String notes;
    private String mode="create";
    private String searchYear;
    private String searchFeedback;
    private String searchsuppliers;

    public void setSearchYear(String searchYear) {
        this.searchYear = searchYear;
    }

    public String getSearchYear() {
        return searchYear;
    }

    public void setSearchFeedback(String searchFeedback) {
        this.searchFeedback = searchFeedback;
    }

    public String getSearchFeedback() {
        return searchFeedback;
    }

    public void setSearchsuppliers(String searchsuppliers) {
        this.searchsuppliers = searchsuppliers;
    }

    public String getSearchsuppliers() {
        return searchsuppliers;
    }
    


   

    public void setDelivery(String delivery) {
        this.delivery = delivery;
    }

    public String getDelivery() {
        return delivery;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getQuality() {
        return quality;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService() {
        return service;
    }

    public BuyerFeedbackBean() {
    }

    public void onSearchSuppliers(ClientEvent clientEvent) {
        // Add event code here...
        String value =clientEvent.getParameters().get("val").toString();
        System.out.println("value is "+value);
        DCBindingContainer dcbindings =
                          (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
      //  DCIteratorBinding iterator =dcbindings.findIteratorBinding("");
        OperationBinding operationBinding2 = dcbindings.getOperationBinding("ExecuteWithParams");
                          operationBinding2.getParamsMap().put("PName",value);
                      
        operationBinding2.execute();
        System.out.println("finished");
    }

    public String editBuyerFeedBackDetails() {
        // Add event code here...
        this.setCost("test1");
        return "buyerFeedbackDetails.jsf";
    }

    public void setCost(String cost) {
        this.cost = cost;
    }

    public String getCost() {
        return cost;
    }

   

    public String onSave() {
        // Add event code here...
        DCBindingContainer dcbindings =(DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        
        AttributeBinding suppleirIdBinding = (AttributeBinding) dcbindings.get("SuppleirId");
        AttributeBinding numberOfOredersBinding = (AttributeBinding) dcbindings.get("NumberOfOreders");
        AttributeBinding segmentationBinding = (AttributeBinding) dcbindings.get("Segmentation");
        AttributeBinding totalSpend = (AttributeBinding) dcbindings.get("TotalSpend");
         
       
       
       
        DCIteratorBinding iterator =dcbindings.findIteratorBinding("SvmBuyerFeedbackeoVo1Iterator");
        Row r =iterator.getRowSetIterator().createRow();
        r.setAttribute("SupplierId", suppleirIdBinding.getInputValue());
        r.setAttribute("Cost",cost);
        r.setAttribute("Delivery", delivery);
        r.setAttribute("Quality", quality);
        r.setAttribute("Service", service);
        r.setAttribute("Notes", notes);
        r.setAttribute("NumberOfOrders", numberOfOredersBinding.getInputValue());
        r.setAttribute("TotalSpend", totalSpend.getInputValue());
        r.setAttribute("SupplierSegmentation",segmentationBinding.getInputValue());
        
        OperationBinding operationBinding = dcbindings.getOperationBinding("Commit");
        operationBinding.execute();
        iterator.executeQuery();
        this.cost=null;
        this.delivery=null;
        this.quality=null;
        this.service=null;
        this.notes=null;
        //this.mode=null;
       
        return "buyerFeedback.jsf";
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getMode() {
        return mode;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getNotes() {
        return notes;
    }

    public void onSearchFeddbakForms(ClientEvent clientEvent) {
        // Add event code here...
    
        String value =clientEvent.getParameters().get("val").toString();
        String year =clientEvent.getParameters().get("year").toString();
        System.out.println("value is "+value);
        System.out.println("year is "+year);
        DCBindingContainer dcbindings =
                          (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        //  DCIteratorBinding iterator =dcbindings.findIteratorBinding("");
        OperationBinding operationBinding2 = dcbindings.getOperationBinding("ExecuteWithParams");
        operationBinding2.getParamsMap().put("PVendorName",value);
        if (!"0".equalsIgnoreCase(year))
        {
            operationBinding2.getParamsMap().put("pYear",year);
        } 
        operationBinding2.execute();
        System.out.println("finished");
    }

    public String onCreateFeedBackForm() {
        // Add event code here...
        this.cost=null;
        this.delivery=null;
        this.quality=null;
        this.service=null;
        this.notes=null;
        DCBindingContainer dcbindings =
                          (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
      //  DCIteratorBinding iterator =dcbindings.findIteratorBinding("SvmFeedbaksupplier1Iterator");
       // iterator.executeQuery();
       OperationBinding operationBinding2 = dcbindings.getOperationBinding("ExecuteWithParams1");
                      //   operationBinding2.getParamsMap().put("PVendorName",value);
                     
       operationBinding2.execute();
        this.mode="create";
        return "buyerFeedbackDetails.jsf";
        
    }

    public String onViewFeedbackForm() {
        // Add event code here...
        DCBindingContainer dcbindings =
                          (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
       
          DCIteratorBinding iterator =dcbindings.findIteratorBinding("SvmBuyerFeedbackeoVo1Iterator");
        Row r = iterator.getCurrentRow();
        if (r==null)
        {
            System.out.println("roww is null");
            return null;
            }
        
       
        OperationBinding operationBinding2 = dcbindings.getOperationBinding("ExecuteWithParams2");
        operationBinding2.getParamsMap().put("PDate",r.getAttribute("CreationDateF"));
        operationBinding2.getParamsMap().put("PSupplier",r.getAttribute("SupplierId"));
        operationBinding2.execute();
        
        
        this.cost=(String)r.getAttribute("Cost");
        this.delivery=(String)r.getAttribute("Delivery");
        this.quality=(String)r.getAttribute("Quality");
        this.service=(String)r.getAttribute("Service");
        this.notes=(String)r.getAttribute("Notes");
        this.mode="view";
        return "buyerFeedbackDetails.jsf";
    }

    public String onBack() {
        // Add event code here...
        return "buyerFeedback.jsf";
        
     // String x="<li class=\"sidebar-search-wrapper\"></li><li class=\"#{attrs.HomeMenuItem}\"><a href=\"Home.jsf\" class=\"nav-link nav-toggle\"><i class=\"icon-home\"></i> <span class=\"title\">Home</span><span class=\"selected\"></span></a> </li><li class=\"heading\"> <h3 class=\"uppercase\">Navigation</h3><li class=\"nav-item\"> <a href=\"\" class=\"nav-link nav-toggle\"> <i class=\"icon-diamond\"></i> <span class=\"title\">Create</span> <span class=\"arrow\"></span></a> <ul class=\"sub-menu\"><li class=\"nav-item\"> <a href=\"AllQuestions.jsf\" class=\"nav-link \"><span class=\"title\">Question Repository</span></a></li><li class=\"nav-item\"> <a href=\"AllEvals.jsf\" class=\"nav-link \"><span class=\"title\">Evaluation</span></a></li><li class=\"nav-item\"> <a href=\"AllSurveys.jsf\" class=\"nav-link \"><span class=\"title\">Survey</span></a></li> </ul></li><li class=\"nav-item\"> <a href=\"\" class=\"nav-link nav-toggle\"> <i class=\"icon-settings\"></i> <span class=\"title\">Reports</span> <span class=\"arrow\"></span></a> <ul class=\"sub-menu\"><li class=\"nav-item\"> <a href=\"EvalScoreSummary.jsf\" class=\"nav-link \"><span class=\"title\">Evaluation Score Summary</span></a></li><li class=\"nav-item\"> <a href=\"EvaluationReport.jsf\" class=\"nav-link \"><span class=\"title\">Supplier Evaluation Scorecard</span></a></li> </ul></li>";
    //  Map sessionScope = ADFContext.getCurrent().getSessionScope();
      //  sessionScope.put("menuedd", x);
        
      //  return null;
    }

  
}

