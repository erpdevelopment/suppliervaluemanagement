package com.oracle.mbc.svm.view.beans;

import com.oracle.mbc.svm.view.utils.ADFUtils;
import com.oracle.mbc.svm.view.utils.JSFUtils;
import com.oracle.mbc.svm.view.utils.JSutils;

import javax.faces.event.PhaseEvent;
import javax.faces.event.ValueChangeEvent;
import oracle.adf.model.AttributeBinding;
import oracle.adf.model.BindingContext;

import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.Number;

public class SurveyBean {
    public SurveyBean() {
    }

    public String CreateSurvey() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsertSurvey");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        
        
        return "Survey.jsf";
    }
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }
    
    
    
    
    public String deleteSurvey() {
        BindingContainer bindings = getBindings();
        Number Id=  (Number) ADFUtils.getBoundAttributeValue("SurveyId");  
      String isStarted=  (String) ADFUtils.getBoundAttributeValue("IsStarted");
      System.out.println("Started:"+isStarted+Id.stringValue());
    if(isStarted.equals("Y"))
    {
    String vtitle="Warning";
                   String   vtext="Survey has been Started ,delete  is not allowed";
                   StringBuilder alerttext=new StringBuilder();
                      alerttext.append("customdangeralertmessage('");
                      alerttext.append(vtitle);
                      alerttext.append("','");
                      alerttext.append(vtext);
                       alerttext.append("')");
                     JSutils.callJavaScriptMethod(alerttext.toString()); 
    
        
      }
    else
    {
        
        
        OperationBinding oper = ADFUtils.findOperation("deleteSurvey");
       
        oper.getParamsMap().put("SurveyId", Id);
         oper.execute();
       if (!oper.getErrors().isEmpty()) {
            return null;
        }
        else 
            
        {
            
             
                String vtitle="Information";
                  String   vtext="Survey Deleted Successfully ";
                  StringBuilder alerttext=new StringBuilder();
                    alerttext.append("customsuccesslertmessage('");
                    alerttext.append(vtitle);
                    alerttext.append("','");
                    alerttext.append(vtext);
                                      alerttext.append("')");
                   JSutils.callJavaScriptMethod(alerttext.toString());  
            
            }
        
    }
        return null;
    }
    
    public String getSurveyStatus() {
        BindingContainer bindings = getBindings();
        OperationBinding oper = bindings.getOperationBinding("getSurveyStarted");
        Number SurveyId = (Number) ADFUtils.getBoundAttributeValue("SurveyId");
        oper.getParamsMap().put("SurveyId", SurveyId);
        String result = oper.execute().toString();
        System.out.println(result+"result");
                        
        if (!oper.getErrors().isEmpty()) {
        
                          return null;
              
                           
              
        }
        return result;
        
        
    }
    
    public String commitSurvey_action() {
        
        boolean check= checkSuraveyData("save"); 
               if (!check)
               {   
                return null;
               }
        String x=this.getSurveyStatus();
        System.out.println(x);
        if(this.getSurveyStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Survey has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        else
        
        {
        
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
      
          else 
        {
         
                    String vtitle="Information";
                      String   vtext="Survey Data Saved Successfully ";
                    
                       StringBuilder alerttext=new StringBuilder();
                        alerttext.append("customsuccesslertmessage('");
                        alerttext.append(vtitle);
                        alerttext.append("','");
                        alerttext.append(vtext);
                         alerttext.append("')");
                       JSutils.callJavaScriptMethod(alerttext.toString());  
                    

        return null;
        }
            
        }
    }
    
    public String cancelSurvey() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return "AllSurveys.jsf";
    }
    
    public String startSurvey_action() {
        boolean check= checkSuraveyData("start"); 
               if (!check)
               {   
                return null;
               }
        BindingContainer bindings = getBindings();
        OperationBinding oper = bindings.getOperationBinding("startSurvey");
        Number SurveyId = (Number) ADFUtils.getBoundAttributeValue("SurveyId");
        oper.getParamsMap().put("SurveyId", SurveyId);
        String result = oper.execute().toString();
        System.out.println(result+"result"+String.valueOf(SurveyId));
                        
        if (!oper.getErrors().isEmpty()) {
        
                          return null;
              
                           
              
        }
//        else 
//        {
//                String vtitle="Inofrmation";
//                                                         String   vtext=result ;
//                                                         StringBuilder alerttext=new StringBuilder();
//                                                            alerttext.append("customsuccesslertmessage('");
//                                                            alerttext.append(vtitle);
//                                                            alerttext.append("','");
//                                                            alerttext.append(vtext);
//                                                             alerttext.append("')");
//                                                           JSutils.callJavaScriptMethod(alerttext.toString());
//                
//                
//            
//            }
        else if (result.equals("S"))
                 {
                     
                         String vtitle="Inofrmation";
                                         String   vtext="Survey Started Successfully";
                                         StringBuilder alerttext=new StringBuilder();
                                            alerttext.append("customsuccesslertmessage('");
                                            alerttext.append(vtitle);
                                            alerttext.append("','");
                                            alerttext.append(vtext);
                                             alerttext.append("')");
                                           JSutils.callJavaScriptMethod(alerttext.toString()); 
                     
                     
                     
                     }
        
        else if (result.equals("N"))
                 {
                     
                         String vtitle="Warning";
              String   vtext="Survey has already Started ";
                StringBuilder alerttext=new StringBuilder();
                 alerttext.append("customdangeralertmessage('");
                  alerttext.append(vtitle);
                  alerttext.append("','");
                   alerttext.append(vtext);
                    alerttext.append("')");
                   JSutils.callJavaScriptMethod(alerttext.toString()); 
                     
                     
                     
                     }
        
        else if (result.equals("U"))
                 {
                     
                         String vtitle="Warning";
                                         String   vtext="Survey Start Date is not Today";
                                         StringBuilder alerttext=new StringBuilder();
                                            alerttext.append("customdangeralertmessage('");
                                            alerttext.append(vtitle);
                                            alerttext.append("','");
                                            alerttext.append(vtext);
                                             alerttext.append("')");
                                           JSutils.callJavaScriptMethod(alerttext.toString()); 
                     
                     
                     
                     }
    
       return null;
    } 
    



    public String Delete_Survey_Supplier() {     
                if(this.getSurveyStatus().equals("Y"))
                {
                    
                        String vtitle="Warning";
                                        String   vtext="Survey has been Started ,updates  are  not allowed";
                                        StringBuilder alerttext=new StringBuilder();
                                           alerttext.append("customdangeralertmessage('");
                                           alerttext.append(vtitle);
                                           alerttext.append("','");
                                           alerttext.append(vtext);
                                            alerttext.append("')");
                                          JSutils.callJavaScriptMethod(alerttext.toString());  
                    
                    
                   return null; 
                    }
                
                else
                {
        
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("DeleteSurveySupplier");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
                }
        return null;
    }

    public String InsertSurveySupplier() {
        
        
        if(this.getSurveyStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Survey has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        
        else
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
            
        }
        return null;
    }

    public String commitSupplier_action() {
        
        if(this.getSurveyStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Survey has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        
        else
        {
        
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
          else
        {
          
                String vtitle="Information";
                  String   vtext="Supplier List Saved Successfully ";
                   StringBuilder alerttext=new StringBuilder();
                    alerttext.append("customsuccesslertmessage('");
                    alerttext.append(vtitle);
                    alerttext.append("','");
                    alerttext.append(vtext);
                   alerttext.append("')");
                   JSutils.callJavaScriptMethod(alerttext.toString());  
                

                return null;
          
            
            }
            
        }

    }
    
    
    public String commitSurveyTemplate_action() {
      
        if(this.getSurveyStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Survey has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        
        else
        {
        OperationBinding oper = ADFUtils.findOperation("createSurveyTemplaterow");
        Number TemplateId = (Number) ADFUtils.getBoundAttributeValue("TemplateId");
        Number survey_id = (Number) ADFUtils.getBoundAttributeValue("SurveyId");
        oper.getParamsMap().put("survey_id", survey_id);
        oper.getParamsMap().put("template_id", TemplateId);
       //oper.execute();
        String result = oper.execute().toString();
        if (!oper.getErrors().isEmpty()) {
       
           

              return null;
        }
        
          else 
        {
      
    //            FacesMessage fm=new FacesMessage(result);
    //                      fm.setSeverity(FacesMessage.SEVERITY_INFO);
    //                     FacesContext fc = FacesContext.getCurrentInstance();
    //                     fc.addMessage(null, fm);
       if(result.equals("N"))
       {
           
               String vtitle="Information";
                 String   vtext="Survey has Template ,kindly Delete the Template ";
               StringBuilder alerttext=new StringBuilder();
                   alerttext.append("customdangeralertmessage('");
                   alerttext.append(vtitle);
                   alerttext.append("','");
                   alerttext.append(vtext);
                   alerttext.append("')");
                  JSutils.callJavaScriptMethod(alerttext.toString()); 

           
           
           }
            
            else    if(result.equals("Y"))
       {

                  
            String vtitle="Information";
            String   vtext="Template Applied Successfully ";
            StringBuilder alerttext=new StringBuilder();
            alerttext.append("customsuccesslertmessage('");
            alerttext.append(vtitle);
            alerttext.append("','");
            alerttext.append(vtext);
            alerttext.append("')");
            JSutils.callJavaScriptMethod(alerttext.toString()); 
       }
                 
                  
         
                           
        return null;
        }
        }
    }
    
    
    public String deleteSurveyTemplate() {
        BindingContainer bindings = getBindings();
        Number Id=  (Number) ADFUtils.getBoundAttributeValue("SurveyId");  
     // String isStarted=  (String) ADFUtils.getBoundAttributeValue("IsStarted");
     // System.out.println("Started:"+isStarted+Id.stringValue());
        if(this.getSurveyStatus().equals("Y"))
    {
    String vtitle="Warning";
                   String   vtext="Survey has been Started ,delete template is not allowed";
                   StringBuilder alerttext=new StringBuilder();
                      alerttext.append("customdangeralertmessage('");
                      alerttext.append(vtitle);
                      alerttext.append("','");
                      alerttext.append(vtext);
                       alerttext.append("')");
                     JSutils.callJavaScriptMethod(alerttext.toString()); 
    
        
      }
    else
    {
        
        
        OperationBinding oper = ADFUtils.findOperation("deleteSurveyTemplate");
       
        oper.getParamsMap().put("SurveyId", Id);
         oper.execute();
       if (!oper.getErrors().isEmpty()) {
            return null;
        }
        else 
            
        {
            
             
                String vtitle="Information";
                  String   vtext="Template Deleted Successfully ";
                  StringBuilder alerttext=new StringBuilder();
                    alerttext.append("customsuccesslertmessage('");
                    alerttext.append(vtitle);
                    alerttext.append("','");
                    alerttext.append(vtext);
                                      alerttext.append("')");
                   JSutils.callJavaScriptMethod(alerttext.toString());  
            
            }
        
    }
        return null;
    }
    
   
    
    private Double gaugeValue = 3.0;
    public void ratingVCE(ValueChangeEvent e) {
        String ResponseComplete=  ADFUtils.getBoundAttributeValue("ResponseCompleted").toString();
        
        if(ResponseComplete.equals("N"))
        {
        if (e.getNewValue() != null) {
           gaugeValue = (Double) e.getNewValue();
          int Id =0;
       System.out.println("You clicked on " + gaugeValue + " stars");
    DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("SvmSurveyResponseValuevo1Iterator");
    Id =Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("ResponseValueId").toString());
    System.out.println("You clicked on " +String.valueOf(Id) + " Id");
      OperationBinding oper = ADFUtils.findOperation("updateSurveyRatingGuage");
        //(Number) ADFUtils.getBoundAttributeValue("ResponseRank");
            oper.getParamsMap().put("RespnoseId", Id);
            oper.getParamsMap().put("RankValue",gaugeValue);
               oper.execute();
           DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmSurveyResponseProgressvo1Iterator");
           
           //Amr --Updates-----//
     
                    iter.executeQuery();
                    ///------End Amr---updates////
           String vtitle="Information";
             String   vtext=(iter2.getViewObject().getCurrentRow().getAttribute("ResponseStatus").toString());
           String   vtext2=(iter2.getViewObject().getCurrentRow().getAttribute("SurveyScoreProgress").toString());
          StringBuilder alerttext=new StringBuilder();
         alerttext.append("updateallsurvey('");
              alerttext.append(vtext);
               alerttext.append("','");
              alerttext.append(vtext2);
           alerttext.append("')");
              JSutils.callJavaScriptMethod(alerttext.toString());  

      
       }
        }// end response compeleted condition
        
//        else 
//        {
//            
//                String vtitle="Warning";
//                               String   vtext="You cannot update ranking ,Notification closed";
//                               StringBuilder alerttext=new StringBuilder();
//                                  alerttext.append("customdangeralertmessage('");
//                                  alerttext.append(vtitle);
//                                  alerttext.append("','");
//                                  alerttext.append(vtext);
//                                   alerttext.append("')");
//                                 JSutils.callJavaScriptMethod(alerttext.toString());  
//            
//            }
    }
    public String btn_search_survey_action() {
        // Add event code here...
        DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("svmsurveySearchvo1Iterator");
       int  Id =Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("SurveyId").toString());
//  
        System.out.println("You clicked on " +String.valueOf(Id) + " Id");
        DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmyearVo1Iterator");
        int  Year =Integer.parseInt(iter2.getViewObject().getCurrentRow().getAttribute("Year").toString());
        System.out.println("You clicked on " +Year + " Year");
        DCIteratorBinding iter3 = (DCIteratorBinding) getBindings().get("svmsurveysuppliersearchvo1Iterator");
        int  SupplierId =Integer.parseInt(iter3.getViewObject().getCurrentRow().getAttribute("VendorId").toString());
        //
//
        OperationBinding oper = ADFUtils.findOperation("loadSurveySummarySearchvo");
        oper.getParamsMap().put("survey_id", new Number(Id));
        oper.getParamsMap().put("year",new Number(Year));
        oper.getParamsMap().put("vendorId",new Number(SupplierId));
        oper.execute();
        JSFUtils.storeOnSession("psyear", Year);
       JSFUtils.storeOnSession("Psurveyid", Id);
        JSFUtils.storeOnSession("Psuppid", SupplierId);
        return "SurveysearchDetails.jsf";
       //return null;
    }


    public void setGaugeValue(Double gaugeValue) {
        this.gaugeValue = gaugeValue;
    }

    public Double getGaugeValue() {
        return gaugeValue;
    }
    
    
    public String btn_search_survey_drill_action() {
        // Add event code here...
        DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("svmSurveyvo2Iterator");
       int  Id =Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("SurveyId").toString());
    //
        System.out.println("You clicked on " +String.valueOf(Id) + " Id");
       // DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmyearVo1Iterator");
        int  Year =0;
        int SupplierId=0;
            //Integer.parseInt(iter2.getViewObject().getCurrentRow().getAttribute("Year").toString());
      //  System.out.println("You clicked on " +Year + " Year");

    //
    OperationBinding oper = ADFUtils.findOperation("loadSurveySummarySearchvo");
    oper.getParamsMap().put("survey_id", new Number(Id));
    oper.getParamsMap().put("year",new Number(Year));
    oper.getParamsMap().put("vendorId",new Number(SupplierId));
    oper.execute();
    JSFUtils.storeOnSession("psyear", Year);
    JSFUtils.storeOnSession("Psurveyid", Id);
    JSFUtils.storeOnSession("Psuppid", SupplierId);
    return "SurveysearchDetails.jsf";
       //return null;
    }
    
    public boolean checkSuraveyData(String type)
        {
          
           String error="";
           String messageType="saved";
            BindingContainer bindings = getBindings();
            
            AttributeBinding surveyNameBinding = (AttributeBinding) bindings.get("SurveyName"); 
            Object surveyName = surveyNameBinding.getInputValue();
            
            AttributeBinding surveyStartDateBinding = (AttributeBinding) bindings.get("SurveyStartDate"); 
            Object surveyStartDate = surveyStartDateBinding.getInputValue();
            
            AttributeBinding surveyEndDateBinding = (AttributeBinding) bindings.get("SurveyEndDate"); 
            Object surveyEndDate = surveyEndDateBinding.getInputValue();
            
            DCIteratorBinding svmSurveySuppliervo1Iterator =((DCBindingContainer)bindings).findIteratorBinding("svmSurveySuppliervo1Iterator"); 
            
            DCIteratorBinding svmSurveyTemplatevo1Iterator =((DCBindingContainer)bindings).findIteratorBinding("svmSurveyTemplatevo1Iterator"); 
            
            AttributeBinding evalYearBinding = (AttributeBinding) bindings.get("SurveyYear"); 
            Object evalYear = evalYearBinding.getInputValue();
            
            
            AttributeBinding evalQBinding = (AttributeBinding) bindings.get("SurveyQuarter"); 
            Object evalQ = evalQBinding.getInputValue();
            
            if (surveyName==null)
                error=error+"<li><span>Survey name is required</span></li>";
            
           if (surveyStartDate==null)
               error=error+"<li><span>Survey start date is required</span></li>";
           
            if (surveyEndDate==null)
                error=error+"<li><span>Survey end date is required</span></li>";
            
            
            if (evalYear==null|| "0".equalsIgnoreCase(evalYear+""))
                error=error+"<li><span>Survey  year is required</span></li>";
            
            
            if (evalQ==null|| "0".equalsIgnoreCase(evalQ+""))
                error=error+"<li><span>Survey quarter is required</span></li>";
            
            if ("start".equalsIgnoreCase(type))
            {
            if (svmSurveySuppliervo1Iterator.getEstimatedRowCount()==0)
                error=error+"<li><span>At least one supplier is required</span></li>";
            
            if (svmSurveyTemplatevo1Iterator.getEstimatedRowCount()==0)
                error=error+"<li><span>Survey template is required</span></li>";
                messageType="started";
            }
          if (error.length()>0)
          {
          JSutils.showSweetAlertHtmlmessage("Survey can not be "+ messageType+" with the following  errors", error, "error");
         return false;
          }
       
             
          
          System.out.println("finish");
            return true;
        }
    
    public void makeASelection(PhaseEvent phaseEvent) {
       RichSelectOneChoice soc;
       soc = (RichSelectOneChoice)JSFUtils.findComponentInRoot("soc1");
       if (soc != null && soc.getValue() == null)
         soc.setValue(new Integer(0));
       
     }
    
    
    
}
