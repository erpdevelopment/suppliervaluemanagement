package com.oracle.mbc.svm.view.beans;


import com.oracle.mbc.svm.model.am.SupplierValueManagementAMImpl;
import com.oracle.mbc.svm.model.vo.svmEvaluationScoreSummaryvoImpl;
import com.oracle.mbc.svm.view.utils.ADFUtils;

import com.oracle.mbc.svm.view.utils.JSFUtils;
import com.oracle.mbc.svm.view.utils.JSutils;


import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import oracle.adf.model.BindingContext;
import oracle.binding.BindingContainer;
import javax.faces.event.ValueChangeEvent;


import oracle.adf.model.binding.DCDataControl;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.component.rich.output.RichOutputFormatted;

import oracle.adfinternal.view.faces.model.binding.FacesCtrlAttrsBinding;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.Number;
import oracle.jbo.uicli.binding.JUCtrlListBinding;

public class EvalBean {
    private RichOutputFormatted outputtext1;


    public EvalBean() {
    }
    public String EvalId;
    public String UsrId ;

   

    
    public BindingContainer getBindings() {
          return BindingContext.getCurrent().getCurrentBindingsEntry();
      }
    
    private Double gaugeValue = 3.0;
    public void ratingVCE(ValueChangeEvent e) {
        if (e.getNewValue() != null) {
           gaugeValue = (Double) e.getNewValue();
          int Id =0;
        System.out.println("You clicked on " + gaugeValue + " stars");
        DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("svmEvalResponseValuesvo2Iterator");
        Id =Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("ResponseValueId").toString());
           DCIteratorBinding suppiter = (DCIteratorBinding) getBindings().get("svmSuppEvaluationResponsevo1Iterator");
         int  SupplierId =Integer.parseInt(suppiter.getViewObject().getCurrentRow().getAttribute("FkSupplierId").toString());
        System.out.println("You clicked on " +String.valueOf(Id) + " Id");
         
        OperationBinding oper = ADFUtils.findOperation("updateRatingGuage");
         oper.getParamsMap().put("RespnoseId", Id);
            oper.getParamsMap().put("RankValue",gaugeValue);
           oper.getParamsMap().put("SupplierId",SupplierId);
            oper.execute();
 

              
           DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmEvaluatorResponseProgressvo1Iterator");
           
           
           //Amr --Updates-----//

              iter.executeQuery();
                    ///------End Amr---updates////
           String vtitle="Information";
             String   vtext=(iter2.getViewObject().getCurrentRow().getAttribute("ResponseStatus").toString());
           String   vtext2=(iter2.getViewObject().getCurrentRow().getAttribute("SupplierProgress").toString());
           String   vtext3=(iter2.getViewObject().getCurrentRow().getAttribute("SupplierscoreProgress").toString());
                             StringBuilder alerttext=new StringBuilder();
               alerttext.append("updatealleval('");
              alerttext.append(vtext);
               alerttext.append("','");
              alerttext.append(vtext2);
              alerttext.append("','");
               alerttext.append(vtext3);
               alerttext.append("')");
              JSutils.callJavaScriptMethod(alerttext.toString());  

      
       }
    }


    public void setUsrId(String UsrId) {
        this.UsrId = UsrId;
    }

    public String getUsrId() {
        return UsrId;
    }

    public void setOutputtext1(RichOutputFormatted outputtext1) {
        this.outputtext1 = outputtext1;
    }

    public RichOutputFormatted getOutputtext1() {
        return outputtext1;
    }

    public void setGaugeValue(Double gaugeValue) {
        this.gaugeValue = gaugeValue;
    }

    public Double getGaugeValue() {
        return gaugeValue;
    }

    public String btn_response_coparison_action() {
        // Add event code here...
        DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("svmEvaluationsearchvo1Iterator");
       int  Id =Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("EvaluationId").toString());
      // Number n =new Number(Id);
        System.out.println("You clicked on " +String.valueOf(Id) + " Id");
        DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmyearVo1Iterator");
        int  Year =Integer.parseInt(iter2.getViewObject().getCurrentRow().getAttribute("Year").toString());
        System.out.println("You clicked on " +Year + " Year");
        DCIteratorBinding iter3 = (DCIteratorBinding) getBindings().get("svmevalSupplierSearchvo1Iterator");
        int  SupplierId =Integer.parseInt(iter3.getViewObject().getCurrentRow().getAttribute("VendorId").toString());
        System.out.println("You clicked on " +String.valueOf(SupplierId) + " SupplierId");
        DCIteratorBinding iter4 = (DCIteratorBinding) getBindings().get("svmQuartervo1Iterator");
        int  QuarterNum =Integer.parseInt(iter4.getViewObject().getCurrentRow().getAttribute("Value").toString());
        System.out.println("You clicked on " +String.valueOf(QuarterNum) + " Quarter");
        
        OperationBinding oper = ADFUtils.findOperation("loadEvaluationSummarySearchvo");
          //(Number) ADFUtils.getBoundAttributeValue("ResponseRank");
        oper.getParamsMap().put("evaluation_id", new Number(Id));
        oper.getParamsMap().put("supplierid",new Number(SupplierId));
        oper.getParamsMap().put("Quarter",new Number(QuarterNum));
        oper.getParamsMap().put("year",new Number(Year));
        oper.execute();
        // Below sessions for Question Rank Details 
        JSFUtils.storeOnSession("PQevalId", new Number(Id));
        JSFUtils.storeOnSession("pqyear", new Number(Year));
        JSFUtils.storeOnSession("pqQuarter", new Number(QuarterNum));
        JSFUtils.storeOnSession("PQvendorId", new Number(SupplierId));
        return "EvalSummaryDetails.jsf";
       //return null;
    }
    
    public String btn_evaluation_result_action() {
        // Add event code here...
        DCIteratorBinding iter3 = (DCIteratorBinding) getBindings().get("svmevalSupplierSearchvo1Iterator");
        int  SupplierId =Integer.parseInt(iter3.getViewObject().getCurrentRow().getAttribute("VendorId").toString());
       // System.out.println("You clicked on " +String.valueOf(SupplierId) + " SupplierId");
        if(SupplierId!=0)
        {
            String SupplierName=iter3.getViewObject().getCurrentRow().getAttribute("VendorName").toString();
        DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("svmEvaluationsearchvo1Iterator");
       int  Id =Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("EvaluationId").toString());
      // Number n =new Number(Id);
       // System.out.println("You clicked on " +String.valueOf(Id) + " Id");
        DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmyearVo1Iterator");
        int  Year =Integer.parseInt(iter2.getViewObject().getCurrentRow().getAttribute("Year").toString());
      //  System.out.println("You clicked on " +Year + " Year");
      
        DCIteratorBinding iter4 = (DCIteratorBinding) getBindings().get("svmQuartervo1Iterator");
        int  QuarterNum =Integer.parseInt(iter4.getViewObject().getCurrentRow().getAttribute("Value").toString());
       // System.out.println("You clicked on " +String.valueOf(QuarterNum) + " Quarter");
       JSFUtils.storeOnSession("SupplierId", SupplierId);
       JSFUtils.storeOnSession("SupplierName", SupplierName);
            JSFUtils.storeOnSession("Pyear", Year);
            JSFUtils.storeOnSession("PQuarter", QuarterNum);
            JSFUtils.storeOnSession("Pevalid", Id);
            return "EvaluationResponse.jsf";
       
        }
//        OperationBinding oper = ADFUtils.findOperation("loadEvaluationSummarySearchvo");
//          //(Number) ADFUtils.getBoundAttributeValue("ResponseRank");
//        oper.getParamsMap().put("evaluation_id", new Number(Id));
//        oper.getParamsMap().put("supplierid",new Number(SupplierId));
//        oper.getParamsMap().put("Quarter",new Number(QuarterNum));
//        oper.getParamsMap().put("year",new Number(Year));
//        oper.execute();
      //  this.getEvalResultButton().setRendered(false);
        else 
        {
            
            String vtitle="Warning";
                            String   vtext="Please Select Supplier";
                            StringBuilder alerttext=new StringBuilder();
                               alerttext.append("customwarninglertmessage('");
                               alerttext.append(vtitle);
                               alerttext.append("','");
                               alerttext.append(vtext);
                                alerttext.append("')");
                              JSutils.callJavaScriptMethod(alerttext.toString()); 
       return null;
        }
    }
    
    public String btn_EvaluationReport_Search_action() {
        // Add event code here...
       // DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("svmEvaluationsearchvo1Iterator");
       int  Id =0;
           //Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("EvaluationId").toString());
      // Number n =new Number(Id);
        System.out.println("You clicked on " +String.valueOf(Id) + " Id");
        DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmyearVo1Iterator");
        int  Year =Integer.parseInt(iter2.getViewObject().getCurrentRow().getAttribute("Year").toString());
        System.out.println("You clicked on " +Year + " Year");
       // DCIteratorBinding iter3 = (DCIteratorBinding) getBindings().get("svmevalSupplierSearchvo1Iterator");
        int  SupplierId =0;
            //Integer.parseInt(iter3.getViewObject().getCurrentRow().getAttribute("VendorId").toString());
        System.out.println("You clicked on " +String.valueOf(SupplierId) + " SupplierId");
        DCIteratorBinding iter4 = (DCIteratorBinding) getBindings().get("svmQuartervo1Iterator");
        int  QuarterNum =Integer.parseInt(iter4.getViewObject().getCurrentRow().getAttribute("Value").toString());
        System.out.println("You clicked on " +String.valueOf(QuarterNum) + " Quarter");
        
        OperationBinding oper = ADFUtils.findOperation("loadEvaluationReport");
//          //(Number) ADFUtils.getBoundAttributeValue("ResponseRank");
        oper.getParamsMap().put("evaluation_id", new Number(Id));
        oper.getParamsMap().put("supplierid",new Number(SupplierId));
        oper.getParamsMap().put("Quarter",new Number(QuarterNum));
        oper.getParamsMap().put("year",new Number(Year));
       oper.execute();
//      //  this.getEvalResultButton().setRendered(false);
//        return "EvalSummaryDetails.jsf";
       return null;
    }
    


   //// Drill down Evaluation/Survey Actions ---As Per Functional Conslustant Request 7-October-2018
     public String btn_eval_drill_action() {
        // Add event code here...
        DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("svmEvaluationvo2Iterator");
       int  Id =Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("EvaluationId").toString());
      // Number n =new Number(Id);
        System.out.println("You clicked on " +String.valueOf(Id) + " Id");
      //  DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmyearVo1Iterator");
        int  Year =0;
            //Integer.parseInt(iter2.getViewObject().getCurrentRow().getAttribute("Year").toString());
       // System.out.println("You clicked on " +Year + " Year");
       // DCIteratorBinding iter3 = (DCIteratorBinding) getBindings().get("svmevalSupplierSearchvo1Iterator");
        int  SupplierId =0;
            //Integer.parseInt(iter3.getViewObject().getCurrentRow().getAttribute("VendorId").toString());
       // System.out.println("You clicked on " +String.valueOf(SupplierId) + " SupplierId");
       // DCIteratorBinding iter4 = (DCIteratorBinding) getBindings().get("svmQuartervo1Iterator");
        int  QuarterNum =0;
            //Integer.parseInt(iter4.getViewObject().getCurrentRow().getAttribute("Value").toString());
      //  System.out.println("You clicked on " +String.valueOf(QuarterNum) + " Quarter");
        
        OperationBinding oper = ADFUtils.findOperation("loadEvaluationSummarySearchvo");
          //(Number) ADFUtils.getBoundAttributeValue("ResponseRank");
        oper.getParamsMap().put("evaluation_id", new Number(Id));
        oper.getParamsMap().put("supplierid",new Number(SupplierId));
        oper.getParamsMap().put("Quarter",new Number(QuarterNum));
        oper.getParamsMap().put("year",new Number(Year));
        oper.execute();

        return "EvalSummaryDetails.jsf";
       //return null;
    }

 
    public void Supplier_ValueChange(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
  
  
        OperationBinding oper = ADFUtils.findOperation("updateRatingGuageValueChange");
 
           oper.getParamsMap().put("SupplierName",valueChangeEvent.getNewValue());
            oper.execute();
           DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmEvaluatorResponseProgressvo1Iterator");
          // String vtitle="Information";
             String   vtext=(iter2.getViewObject().getCurrentRow().getAttribute("ResponseStatus").toString());
           String   vtext2=(iter2.getViewObject().getCurrentRow().getAttribute("SupplierProgress").toString());
           String   vtext3=(iter2.getViewObject().getCurrentRow().getAttribute("SupplierscoreProgress").toString());
                             StringBuilder alerttext=new StringBuilder();
               alerttext.append("updatealleval('");
              alerttext.append(vtext);
               alerttext.append("','");
              alerttext.append(vtext2);
              alerttext.append("','");
               alerttext.append(vtext3);
               alerttext.append("')");
              JSutils.callJavaScriptMethod(alerttext.toString());  
    
//        String vtitle="Information";
//          String   vtext=String.valueOf(valueChangeEvent.getNewValue());
//           StringBuilder alerttext=new StringBuilder();
//            alerttext.append("customsuccesslertmessage('");
//            alerttext.append(vtitle);
//            alerttext.append("','");
//            alerttext.append(vtext);
//          
//            alerttext.append("')");
//           JSutils.callJavaScriptMethod(alerttext.toString());  
   
   
   
    }
    
    
    
    public String btn_EvaluationScoreReport_Search_action() {
        // Add event code here...

                
      DCIteratorBinding iter = (DCIteratorBinding) getBindings().get("svmEvaluationsearchvo1Iterator");
       int  Id =Integer.parseInt(iter.getViewObject().getCurrentRow().getAttribute("EvaluationId").toString());
      // Number n =new Number(Id);
        System.out.println("You clicked on " +String.valueOf(Id) + " Id");
        DCIteratorBinding iter2 = (DCIteratorBinding) getBindings().get("svmyearVo1Iterator");
        int  Year =Integer.parseInt(iter2.getViewObject().getCurrentRow().getAttribute("Year").toString());
        System.out.println("You clicked on " +Year + " Year");
        DCIteratorBinding iter3 = (DCIteratorBinding) getBindings().get("svmevalSupplierSearchvo1Iterator");
        int  SupplierId =Integer.parseInt(iter3.getViewObject().getCurrentRow().getAttribute("VendorId").toString());
        System.out.println("You clicked on " +String.valueOf(SupplierId) + " SupplierId");
        DCIteratorBinding iter4 = (DCIteratorBinding) getBindings().get("svmQuartervo1Iterator");
        int  QuarterNum =Integer.parseInt(iter4.getViewObject().getCurrentRow().getAttribute("Value").toString());
        System.out.println("You clicked on " +String.valueOf(QuarterNum) + " Quarter");
        
        
        OperationBinding oper = ADFUtils.findOperation("loadEvaluationScoreReport");
    //          //(Number) ADFUtils.getBoundAttributeValue("ResponseRank");
        oper.getParamsMap().put("evaluation_id", new Number(Id));
        oper.getParamsMap().put("supplierid",new Number(SupplierId));
        oper.getParamsMap().put("Quarter",new Number(QuarterNum));
        oper.getParamsMap().put("year",new Number(Year));
       oper.execute();
  
       return null;
    }
    
    
    public SupplierValueManagementAMImpl getApplicationModule()
    {
        
            FacesContext fctx = FacesContext.getCurrentInstance(); 
            BindingContext bindingContext = BindingContext.getCurrent();
            DCDataControl dc = bindingContext.findDataControl("SupplierValueManagementAMDataControl");
            SupplierValueManagementAMImpl am = (SupplierValueManagementAMImpl)dc.getDataProvider();
          return am;
        }
    
    
    public String sendEmail()
    {
        

        
                    OperationBinding oper = ADFUtils.findOperation("sendSuppliersEmails");
                    oper.execute();
                   String retvalue  = oper.getResult().toString();
                      if(retvalue.equals("0"))
    
        {
        String vtitle="Warning";
        String   vtext="No E-mails selected";
        StringBuilder alerttext=new StringBuilder();
        alerttext.append("customwarninglertmessage('");
        alerttext.append(vtitle);
        alerttext.append("','");
        alerttext.append(vtext);
        
        alerttext.append("')");
        JSutils.callJavaScriptMethod(alerttext.toString());         
                      
    }
                      else
                      
                      {
                   
                           String vtitle="Information";
             //String   vtext=retvalue+" Suppliers Received Emails ";
            String   vtext=retvalue+" Notifications were sent ";
           StringBuilder alerttext=new StringBuilder();
            alerttext.append("customsuccesslertmessage('");
            alerttext.append(vtitle);
            alerttext.append("','");
            alerttext.append(vtext);
          
            alerttext.append("')");
           JSutils.callJavaScriptMethod(alerttext.toString()); 
                      
                      }
                      
            return null;
        }
    public String btn_send_notify() {
        Map sessionscope = ADFContext.getCurrent().getSessionScope();
        String vitkey = sessionscope.get("itkey").toString();
        String vittype =  sessionscope.get("ittype").toString();
        String vntfId =  sessionscope.get("ntfId").toString();

        
        OperationBinding oper = ADFUtils.findOperation("sendNotificationWorkflow");
        oper.getParamsMap().put("ntfId", vntfId);
        oper.getParamsMap().put("itemkey", vitkey);
        oper.getParamsMap().put("itemtype", vittype);
        oper.execute();
        String retvalue  = oper.getResult().toString();
          if(retvalue.equals("UNCOMPLETE"))
        
        {
        String vtitle="Warning";
        String   vtext="You  need to Rank whole Evaluation Questions";
        StringBuilder alerttext=new StringBuilder();
        alerttext.append("customwarninglertmessage('");
        alerttext.append(vtitle);
        alerttext.append("','");
        alerttext.append(vtext);
        
        alerttext.append("')");
        JSutils.callJavaScriptMethod(alerttext.toString());
          
        }
          else
          
          {
        
               String vtitle="Information";
        //String   vtext=retvalue+" Suppliers Received Emails ";
        String   vtext=retvalue+" Evaluaiton Compeleted Succesfully ";
        StringBuilder alerttext=new StringBuilder();
        alerttext.append("customsuccesslertmessage('");
        alerttext.append(vtitle);
        alerttext.append("','");
        alerttext.append(vtext);
        
        alerttext.append("')");
        JSutils.callJavaScriptMethod(alerttext.toString());
          
          }
          
        return null;
    }
}
