package com.oracle.mbc.svm.view.beans;

import com.oracle.mbc.svm.view.utils.JSutils;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.domain.Number;

import org.apache.myfaces.trinidad.render.ExtendedRenderKitService;
import org.apache.myfaces.trinidad.util.Service;
import com.oracle.mbc.svm.view.utils.ADFUtils;

import java.io.IOException;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import javax.faces.context.ExternalContext;

import oracle.adf.controller.ControllerContext;
import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.render.ClientEvent;
import oracle.adf.model.AttributeBinding;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

public class settingBean {
    public settingBean() {
    } 
  public Number isdelete ;
  private String cYear=getCurrentYear();
    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public void setCYear(String cYear) {
        this.cYear = cYear;
    }

    public String getCYear() {
        return getCurrentYear();
    }


    public String getCurrentYear()
    {
            Calendar now = Calendar.getInstance();
        return now.get(Calendar.YEAR)+"";
        }

    public String createNewTemplate() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        
        
        return "Questions.jsf";
    }

    public String cancelTemplate() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return "AllQuestions.jsf";
    }

    public String commitTemplate_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
      
          else 
        {
         
                    String vtitle="Information";
                      String   vtext="Template Data Saved Successfully ";
                       StringBuilder alerttext=new StringBuilder();
                        alerttext.append("customsuccesslertmessage('");
                        alerttext.append(vtitle);
                        alerttext.append("','");
                        alerttext.append(vtext);
                      
                        alerttext.append("')");
                       JSutils.callJavaScriptMethod(alerttext.toString());  
                    

        return null;
        }
    }
    
    
    
    public String commitQuestion_action() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
      
          else 
        {
         
                    String vtitle="Information";
                     String   vtext="Questions Data Saved Successfully ";
                    StringBuilder alerttext=new StringBuilder();
                        alerttext.append("customsuccesslertmessage('");
                        alerttext.append(vtitle);
                        alerttext.append("','");
                        alerttext.append(vtext);
                      alerttext.append("')");
                       JSutils.callJavaScriptMethod(alerttext.toString());  
               

        return null;
        }
    }
    
    //// Evaluation ///
    
    public String createNewEvaluation() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsert");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        
        
        return "Eval.jsf";
    }

    public String cancelEvaluation() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Rollback");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return "AllEvals.jsf";
    }
    
  

    public String commitEvaluation_action() {
        
        
       
        
        
        boolean check= checkEvalutionData("save"); 
               if (!check)
               {   
                return null;
               }
        String x=this.getEvaluationStatus();
        System.out.println(x);
        if(this.getEvaluationStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Evaluation has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        else
        
        {
        
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
      
          else 
        {
         
                    String vtitle="Information";
                      String   vtext="Evaluation Data Saved Successfully ";
                    
                       StringBuilder alerttext=new StringBuilder();
                        alerttext.append("customsuccesslertmessage('");
                        alerttext.append(vtitle);
                        alerttext.append("','");
                        alerttext.append(vtext);
                         alerttext.append("')");
                       JSutils.callJavaScriptMethod(alerttext.toString());  
                    

        return null;
        }
            
        }
    }
    public String commitSupplier_action() {
        
        
        if(this.getEvaluationStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Evaluation has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        else
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
      
          else 
        {
         
                    String vtitle="Information";
                      String   vtext="Supplier List Saved Successfully ";
                       StringBuilder alerttext=new StringBuilder();
                        alerttext.append("customsuccesslertmessage('");
                        alerttext.append(vtitle);
                        alerttext.append("','");
                        alerttext.append(vtext);
                       alerttext.append("')");
                       JSutils.callJavaScriptMethod(alerttext.toString());  
                    

        return null;
        }
        }
    }
    public String commitEvaluationTeam_action() {
        
        if(this.getEvaluationStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Evaluation has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        else
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
      
          else 
        {
         
                    String vtitle="Information";
                      String   vtext="Evaluation List Saved Successfully ";
                    StringBuilder alerttext=new StringBuilder();
                        alerttext.append("customsuccesslertmessage('");
                        alerttext.append(vtitle);
                        alerttext.append("','");
                        alerttext.append(vtext);
                        alerttext.append("')");
                       JSutils.callJavaScriptMethod(alerttext.toString());  
                    

        return null;
        }
            
        }
    }
    
    
    public String commitEvalTemplate_action() {
      
        if(this.getEvaluationStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Evaluation has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        
        else
        {
        OperationBinding oper = ADFUtils.findOperation("createEvaluationTemplaterow");
        Number TemplateId = (Number) ADFUtils.getBoundAttributeValue("TemplateId");
        Number EvalId = (Number) ADFUtils.getBoundAttributeValue("EvaluationId");
        oper.getParamsMap().put("eval_id", EvalId);
        oper.getParamsMap().put("template_id", TemplateId);
       //oper.execute();
        String result = oper.execute().toString();
        if (!oper.getErrors().isEmpty()) {
       
           

              return null;
        }
        
          else 
        {
      
//            FacesMessage fm=new FacesMessage(result);
//                      fm.setSeverity(FacesMessage.SEVERITY_INFO);
//                     FacesContext fc = FacesContext.getCurrentInstance();
//                     fc.addMessage(null, fm);
       if(result.equals("N"))
       {
           
               String vtitle="Information";
                 String   vtext="Evaluation has Template ,kindly Delete the Template ";
               StringBuilder alerttext=new StringBuilder();
                   alerttext.append("customdangeralertmessage('");
                   alerttext.append(vtitle);
                   alerttext.append("','");
                   alerttext.append(vtext);
                   alerttext.append("')");
                  JSutils.callJavaScriptMethod(alerttext.toString()); 

           
           
           }
            
            else    if(result.equals("Y"))
       {

                  
            String vtitle="Information";
            String   vtext="Template Applied Successfully ";
            StringBuilder alerttext=new StringBuilder();
            alerttext.append("customsuccesslertmessage('");
            alerttext.append(vtitle);
            alerttext.append("','");
            alerttext.append(vtext);
            alerttext.append("')");
            JSutils.callJavaScriptMethod(alerttext.toString()); 
       }
                 
                  
         
                           
        return null;
        }
        }
    }


    public void setIsdelete(Number isdelete) {
        this.isdelete = isdelete;
    }

    public Number getIsdelete() {
        return isdelete;
    }



    public String deleteEvaluationTemplate() {
        BindingContainer bindings = getBindings();
        Number Id=  (Number) ADFUtils.getBoundAttributeValue("EvaluationId");  
      String isStarted=  (String) ADFUtils.getBoundAttributeValue("IsStarted");
     // System.out.println("Started:"+isStarted+Id.stringValue());
        if(this.getEvaluationStatus().equals("Y"))
    {
   String vtitle="Warning";
                   String   vtext="Evaluation has been Started ,delete template is not allowed";
                   StringBuilder alerttext=new StringBuilder();
                      alerttext.append("customdangeralertmessage('");
                      alerttext.append(vtitle);
                      alerttext.append("','");
                      alerttext.append(vtext);
                       alerttext.append("')");
                     JSutils.callJavaScriptMethod(alerttext.toString()); 
   
        
      }
    else
    {
        
        
        OperationBinding oper = ADFUtils.findOperation("deleteEvaluationTemplate");
       
        oper.getParamsMap().put("EvalId", Id);
         oper.execute();
       if (!oper.getErrors().isEmpty()) {
            return null;
        }
        else 
            
        {
            
             
                String vtitle="Information";
                  String   vtext="Template Deleted Successfully ";
                  StringBuilder alerttext=new StringBuilder();
                    alerttext.append("customsuccesslertmessage('");
                    alerttext.append(vtitle);
                    alerttext.append("','");
                    alerttext.append(vtext);
                                      alerttext.append("')");
                   JSutils.callJavaScriptMethod(alerttext.toString());  
            
            }
        
    }
        return null;
    }
    
    
    public String deleteEvaluation() {
        BindingContainer bindings = getBindings();
        Number Id=  (Number) ADFUtils.getBoundAttributeValue("EvaluationId");  
      String isStarted=  (String) ADFUtils.getBoundAttributeValue("IsStarted");
      System.out.println("Started:"+isStarted+Id.stringValue());
    if(isStarted.equals("Y"))
    {
    String vtitle="Warning";
                   String   vtext="Evaluation has been Started ,delete  is not allowed";
                   StringBuilder alerttext=new StringBuilder();
                      alerttext.append("customdangeralertmessage('");
                      alerttext.append(vtitle);
                      alerttext.append("','");
                      alerttext.append(vtext);
                       alerttext.append("')");
                     JSutils.callJavaScriptMethod(alerttext.toString()); 
    
        
      }
    else
    {
        
        
        OperationBinding oper = ADFUtils.findOperation("deleteEvaluation");
       
        oper.getParamsMap().put("EvalId", Id);
         oper.execute();
       if (!oper.getErrors().isEmpty()) {
            return null;
        }
        else 
            
        {
            
             
                String vtitle="Information";
                  String   vtext="Evaluation Deleted Successfully ";
                  StringBuilder alerttext=new StringBuilder();
                    alerttext.append("customsuccesslertmessage('");
                    alerttext.append(vtitle);
                    alerttext.append("','");
                    alerttext.append(vtext);
                                      alerttext.append("')");
                   JSutils.callJavaScriptMethod(alerttext.toString());  
            
            }
        
    }
        return null;
    }
    
    
    
    public String startEvaluation_action() {
        boolean check= checkEvalutionData("start"); 
               if (!check)
               {   
                return null;
               }
        BindingContainer bindings = getBindings();
        OperationBinding oper = bindings.getOperationBinding("startEvaluation");
        Number EvalId = (Number) ADFUtils.getBoundAttributeValue("EvaluationId");
        oper.getParamsMap().put("EvaluationId", EvalId);
        String result = oper.execute().toString();
        System.out.println(result+"result");
                        
        if (!oper.getErrors().isEmpty()) {
        
                          return null;
              
                           
              
        }
        else if (result.equals("S"))
                 {
                     
                         String vtitle="Inofrmation";
                                         String   vtext="Evaluation Started Successfully";
                                         StringBuilder alerttext=new StringBuilder();
                                            alerttext.append("customsuccesslertmessage('");
                                            alerttext.append(vtitle);
                                            alerttext.append("','");
                                            alerttext.append(vtext);
                                             alerttext.append("')");
                                           JSutils.callJavaScriptMethod(alerttext.toString()); 
                     
                     
                     
                     }
        
        else if (result.equals("N"))
                 {
                     
                         String vtitle="Warning";
                                         String   vtext="Evaluation has already Started ";
                                         StringBuilder alerttext=new StringBuilder();
                                            alerttext.append("customdangeralertmessage('");
                                            alerttext.append(vtitle);
                                            alerttext.append("','");
                                            alerttext.append(vtext);
                                             alerttext.append("')");
                                           JSutils.callJavaScriptMethod(alerttext.toString()); 
                     
                     
                     
                     }
        
        else if (result.equals("U"))
                 {
                     
                         String vtitle="Warning";
                                         String   vtext="Evaluation Start Date is not Today";
                                         StringBuilder alerttext=new StringBuilder();
                                            alerttext.append("customdangeralertmessage('");
                                            alerttext.append(vtitle);
                                            alerttext.append("','");
                                            alerttext.append(vtext);
                                             alerttext.append("')");
                                           JSutils.callJavaScriptMethod(alerttext.toString()); 
                     
                     
                     
                     }
   
       return null;
    } 
//

    public String getEvaluationStatus() {
        BindingContainer bindings = getBindings();
        OperationBinding oper = bindings.getOperationBinding("getEvaluationStarted");
        Number EvalId = (Number) ADFUtils.getBoundAttributeValue("EvaluationId");
        oper.getParamsMap().put("EvaluationId", EvalId);
        String result = oper.execute().toString();
        System.out.println(result+"result");
                        
        if (!oper.getErrors().isEmpty()) {
        
                          return null;
              
                           
              
        }
        return result;
        
        
    }


    public String Delete_Evaluation_Supplier() {
        
        if(this.getEvaluationStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Evaluation has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        
        else
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Delete");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
            
        }
    }

    public String deleteEvaluationEvalutors() {
        if(this.getEvaluationStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Evaluation has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        
        else
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("Delete1");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
        }
    }

    public String InsertEvalSupplier() {
        if(this.getEvaluationStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Evaluation has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        else
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsertEvalSupplier");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
        }
    }

    public String InsertEvaluationEvaluators() {
        if(this.getEvaluationStatus().equals("Y"))
        {
            
                String vtitle="Warning";
                                String   vtext="Evaluation has been Started ,updates  are  not allowed";
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(vtext);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            
            
           return null; 
            }
        else
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("CreateInsertEvalEmployee");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
        }
    }

    public String deleteTemplate() {
        
        String delete_allowed="Y";
        
        String Templatetype =ADFUtils.getBoundAttributeValue("TemplateType").toString() ;
       if(Templatetype.equals("Evaluation"))
       {
               int TemplateEvaluation = Integer.parseInt(ADFUtils.getBoundAttributeValue("Countevaluationtemplate").toString()) ;
            if(TemplateEvaluation>0)
            {
                    String vtitle="Warning";
                                    String   vtext="Template is assigned to Evaluations ,delete is not allowed";
                                    StringBuilder alerttext=new StringBuilder();
                                       alerttext.append("customdangeralertmessage('");
                                       alerttext.append(vtitle);
                                       alerttext.append("','");
                                       alerttext.append(vtext);
                                        alerttext.append("')");
                                      JSutils.callJavaScriptMethod(alerttext.toString());  
                    
                    delete_allowed="N" ;
                
                }
           
           }
       else if(Templatetype.equals("Survey"))
       {
               int TemplateSurvey = Integer.parseInt(ADFUtils.getBoundAttributeValue("Countsurveytemplate").toString()) ;  
               if(TemplateSurvey>0)
               {
                       String vtitle="Warning";
                                       String   vtext="Template is assigned to Surveys ,delete is not allowed";
                                       StringBuilder alerttext=new StringBuilder();
                                          alerttext.append("customdangeralertmessage('");
                                          alerttext.append(vtitle);
                                          alerttext.append("','");
                                          alerttext.append(vtext);
                                           alerttext.append("')");
                                         JSutils.callJavaScriptMethod(alerttext.toString());  
                       
                       delete_allowed="N" ;
                   
                   }
           
           
           
           }
       
        if(delete_allowed.equals("Y"))
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("DeleteTemplate");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty())
        {
            return null;
        }
        
        else
        {
            OperationBinding operationBinding2 = bindings.getOperationBinding("Commit");
            Object result2 = operationBinding2.execute();
            OperationBinding operationBinding3 = bindings.getOperationBinding("ExecuteTemplate");
            Object result3 = operationBinding3.execute();
        
            String vtitle="Inofrmation";
                            String   vtext="Template deleted Successfully";
                            StringBuilder alerttext=new StringBuilder();
                               alerttext.append("customsuccesslertmessage('");
                               alerttext.append(vtitle);
                               alerttext.append("','");
                               alerttext.append(vtext);
                                alerttext.append("')");
                              JSutils.callJavaScriptMethod(alerttext.toString()); 
            
        
        
        }
        
        }
        return null;
    }

    public String delete_Question() {
        
        String delete_allowed="Y";
        int TemplateEvaluation=0;
        int TemplateSurvey=0;
        String Templatetype =ADFUtils.getBoundAttributeValue("TemplateType").toString() ;
        if(Templatetype.equals("Evaluation"))
        {
            
            Object obj=ADFUtils.getBoundAttributeValue("Countevaluationtemplate");
            if (obj!=null)
               TemplateEvaluation = Integer.parseInt(ADFUtils.getBoundAttributeValue("Countevaluationtemplate").toString()) ;
          
          
            if(TemplateEvaluation>0)
            {
                    String vtitle="Warning";
                                    String   vtext="Template is assigned to Evaluations ,delete is not allowed";
                                    StringBuilder alerttext=new StringBuilder();
                                       alerttext.append("customdangeralertmessage('");
                                       alerttext.append(vtitle);
                                       alerttext.append("','");
                                       alerttext.append(vtext);
                                        alerttext.append("')");
                                      JSutils.callJavaScriptMethod(alerttext.toString());  
                    
                    delete_allowed="N" ;
                
                }
           
           }
        else if(Templatetype.equals("Survey"))
        {
             Object obj2=ADFUtils.getBoundAttributeValue("Countsurveytemplate");
             if(obj2!=null)
               TemplateSurvey = Integer.parseInt(ADFUtils.getBoundAttributeValue("Countsurveytemplate").toString()) ;  
               if(TemplateSurvey>0)
               {
                       String vtitle="Warning";
                                       String   vtext="Template is assigned to Surveys ,delete is not allowed";
                                       StringBuilder alerttext=new StringBuilder();
                                          alerttext.append("customdangeralertmessage('");
                                          alerttext.append(vtitle);
                                          alerttext.append("','");
                                          alerttext.append(vtext);
                                           alerttext.append("')");
                                         JSutils.callJavaScriptMethod(alerttext.toString());  
                       
                       delete_allowed="N" ;
                   
                   }
           
           
           
           }
        
        if(delete_allowed.equals("Y"))
        {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding = bindings.getOperationBinding("DeleteQuestion");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty())
        {
            return null;
        }
        
        else
        {
            OperationBinding operationBinding2 = bindings.getOperationBinding("Commit");
            Object result2 = operationBinding2.execute();
            OperationBinding operationBinding3 = bindings.getOperationBinding("ExecuteQuestion");
            Object result3 = operationBinding3.execute();
        
            String vtitle="Inofrmation";
                            String   vtext="Question deleted Successfully";
                            StringBuilder alerttext=new StringBuilder();
                               alerttext.append("customsuccesslertmessage('");
                               alerttext.append(vtitle);
                               alerttext.append("','");
                               alerttext.append(vtext);
                                alerttext.append("')");
                              JSutils.callJavaScriptMethod(alerttext.toString()); 
            
        
        
        }
        
        }
        return null;
    }
    
    public String onQuestionDrill() {
            // Add event code here...
            
            return "questionRankDetails";
        }

        public void OnQuestionSurvayDril(ClientEvent clientEvent) {
            // Add event code here...
            Map sessionScope = ADFContext.getCurrent().getSessionScope();
//            if (  "N".equalsIgnoreCase((  String)sessionScope.get("isAdmin"))    )
//            {
//                System.out.println("not Admin");
//                return;
//                }
            String questionId =clientEvent.getParameters().get("Qid").toString();
            String feeback =clientEvent.getParameters().get("feeback").toString();
            String questionValue=clientEvent.getParameters().get("QuestionValue").toString();
            String qSource=clientEvent.getParameters().get("QSource").toString();
            String all=clientEvent.getParameters().get("all").toString();
            String pageName =clientEvent.getParameters().get("pageName").toString();
          //  String evaluationId =clientEvent.getParameters().get("EvId").toString();
            System.out.println(questionId);
            System.out.println(feeback);
            
            String minRank="0";
            String maxRank="0";
            if ("Satisified".equalsIgnoreCase(feeback))
            {
                minRank="3";
                maxRank="5";       
            }
            
            if ("UnSatisfied".equalsIgnoreCase(feeback))
            {
                minRank="1";
                maxRank="2";
            }
            
            if ("Over Exceptations".equalsIgnoreCase(feeback))
            {
                minRank="5";
                maxRank="5";
            }
            
            if ("Meet Exceptations".equalsIgnoreCase(feeback))
            {
                minRank="3";
                maxRank="4";
            }
            
            if ("Under Exceptations".equalsIgnoreCase(feeback))
            {
                minRank="1";
                maxRank="2";
            }
           // Map sessionScope = ADFContext.getCurrent().getSessionScope();
            sessionScope.put("sessionQuestion", questionValue);
            sessionScope.put("sessionQuestionId", questionId);
            sessionScope.put("sessionQSource", qSource);
            sessionScope.put("sessionPageSourceName", pageName);
            sessionScope.put("sessionResposeValueMin", minRank);
            sessionScope.put("sessionResposeValueMax", maxRank);
            if ("yes".equalsIgnoreCase(all))
            {       //Commented By Heba
                    //sessionScope.put("psyear", "0");
                    sessionScope.put("Psurveyid", "0");
                
                }
            // commented By Heba
            //sessionScope.put("pQuarter", "0");
            
            FacesContext fctx = FacesContext.getCurrentInstance();
          //  UIViewRoot root = fctx.getViewRoot();
           // String id = root.getViewId();
            ExternalContext ectx = fctx.getExternalContext();
             
               String viewId = "/pages/questionRankDetails.jsf";
               System.out.println(viewId);
               ControllerContext controllerCtx = null;
               controllerCtx = ControllerContext.getInstance();
               String activityURL = controllerCtx.getGlobalViewActivityURL(viewId);try{
                 ectx.redirect(activityURL);} catch (IOException e) {//Can't redirect
                 e.printStackTrace();}
        }
        
    public boolean checkEvalutionData(String type)
           {
             
               String error="";
                      String messageType="saved";
                       BindingContainer bindings = getBindings();
                       
                       AttributeBinding evaluationNameBinding = (AttributeBinding) bindings.get("EvaluationName"); 
                       Object evaluationName = evaluationNameBinding.getInputValue();
                       
                       AttributeBinding evaluationStartDateBinding = (AttributeBinding) bindings.get("EvaluationStartDate"); 
                       Object evaluationStartDate = evaluationStartDateBinding.getInputValue();
                       
                       AttributeBinding evaluationEndDateBinding = (AttributeBinding) bindings.get("EvaluationEndDate"); 
                       Object evaluationEndDate = evaluationEndDateBinding.getInputValue();
                       
                       DCIteratorBinding svmEvalSuppliersvo2Iterator =((DCBindingContainer)bindings).findIteratorBinding("svmEvalSuppliersvo2Iterator"); 
                       
                       DCIteratorBinding svmEvalEvaluatorsvo2Iterator =((DCBindingContainer)bindings).findIteratorBinding("svmEvalEvaluatorsvo2Iterator"); 
               
                       DCIteratorBinding svmEvaluationTemplatevo2Iterator =((DCBindingContainer)bindings).findIteratorBinding("svmEvaluationTemplatevo2Iterator"); 
                       
               AttributeBinding evalYearBinding = (AttributeBinding) bindings.get("EvalYear"); 
               Object evalYear = evalYearBinding.getInputValue();
               
               
               AttributeBinding evalQBinding = (AttributeBinding) bindings.get("EvalQuarter"); 
               Object evalQ = evalQBinding.getInputValue();
                       
                       if (evaluationName==null)
                           error=error+"<li><span>Evaluation name is required</span></li>";
                       
                      if (evaluationStartDate==null)
                          error=error+"<li><span>Evaluation start date is required</span></li>";
                      
                       if (evaluationEndDate==null)
                           error=error+"<li><span>Evaluation end date is required</span></li>";
                       
               
               if (evalYear==null|| "0".equalsIgnoreCase(evalYear+""))
                   error=error+"<li><span>Evaluation  year is required</span></li>";
               
               
               if (evalQ==null|| "0".equalsIgnoreCase(evalQ+""))
                   error=error+"<li><span>Evaluation quarter is required</span></li>";
                       
                       if ("start".equalsIgnoreCase(type))
                       {
                       if (svmEvalSuppliersvo2Iterator.getEstimatedRowCount()==0)
                           error=error+"<li><span>At least one supplier is required</span></li>";
                       
                       if (svmEvalEvaluatorsvo2Iterator.getEstimatedRowCount()==0)
                               error=error+"<li><span>At least one evaluator is required</span></li>";
                           
                       if (svmEvaluationTemplatevo2Iterator.getEstimatedRowCount()==0)
                           error=error+"<li><span>Evaluation template is required</span></li>";
                           messageType="started";
                       }
                       
              
                       
                     if (error.length()>0)
                     {
                     JSutils.showSweetAlertHtmlmessage("Evaluation can not be "+ messageType+" with the following  errors", error, "error");
                    return false;
                     }
                  
                        
                     
                     System.out.println("finish");
                       return true;
           }
    
    public void onSearchRegistedSuppliers(ClientEvent clientEvent) {
        // Add event code here...
        String value =clientEvent.getParameters().get("val").toString();
        System.out.println("value is "+value);
        DCBindingContainer dcbindings =
                          (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        //  DCIteratorBinding iterator =dcbindings.findIteratorBinding("");
        OperationBinding operationBinding2 = dcbindings.getOperationBinding("ExecuteWithParams");
                          operationBinding2.getParamsMap().put("PSupplierName",value);
                      
        operationBinding2.execute();
        System.out.println("finished");
     
        
    }


    public void onHomePageSearch(ClientEvent clientEvent) {
        // Add event code here...
        
        String year =clientEvent.getParameters().get("year").toString();
        String Q =clientEvent.getParameters().get("q").toString();
        Map sessionScope = ADFContext.getCurrent().getSessionScope();
        
        System.out.println("year is"+year);
        System.out.println("Q  is"+Q );
        
        sessionScope.put("psyear", year);
        sessionScope.put("pQuarter", Q);
        System.out.println("jj");
    }
}
