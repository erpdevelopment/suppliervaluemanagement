package com.oracle.mbc.svm.view.beans;

import com.oracle.mbc.svm.view.utils.JSutils;

import java.util.Map;

import javax.faces.event.ActionEvent;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.share.ADFContext;
import oracle.adf.view.rich.render.ClientEvent;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class ReEvalautionSupplier {
    public ReEvalautionSupplier() {
    }
    private String justification ;
    public String onReEvalaute() {
        // Add event code here...
      
        DCBindingContainer dcbindings =
                          (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        DCIteratorBinding iterator =dcbindings.findIteratorBinding("ReEvalautionSupplier1Iterator");
        Row r =iterator.getRowSetIterator().getCurrentRow();
       if (r!=null)
       {
       
         if (justification==null)
         {
             
             
                 String vtitle="Warning";
                                
                                 StringBuilder alerttext=new StringBuilder();
                                    alerttext.append("customdangeralertmessage('");
                                    alerttext.append(vtitle);
                                    alerttext.append("','");
                                    alerttext.append("Justification is required");
                                     alerttext.append("')");
                                   JSutils.callJavaScriptMethod(alerttext.toString());  
             
             return null;
             }
         
           Map sessionScope = ADFContext.getCurrent().getSessionScope();
        OperationBinding operationBinding = dcbindings.getOperationBinding("startReEvaluation");
           operationBinding.getParamsMap().put("evaluationId",r.getAttribute("EvaluationId"));
           operationBinding.getParamsMap().put("evaluationName",r.getAttribute("EvaluationName"));
           operationBinding.getParamsMap().put("evaluatorUserId",r.getAttribute("UserId"));
           operationBinding.getParamsMap().put("evaluatorUserName",r.getAttribute("UserName"));
           operationBinding.getParamsMap().put("evaluatorFullName",r.getAttribute("FullName"));
           operationBinding.getParamsMap().put("supplierId",r.getAttribute("SupplierId"));
           operationBinding.getParamsMap().put("responseId",r.getAttribute("ResponseId"));
           operationBinding.getParamsMap().put("createdUser",sessionScope.get("userId"));
           operationBinding.getParamsMap().put("justification",justification);
                      
       String  result =(String)operationBinding.execute();
        System.out.println("result is "+result);
        if ("SUCEESS".equalsIgnoreCase(result))
        {
            //show SUCEESS
            String vtitle="Information";
              String   vtext="Supplier "+r.getAttribute("SupplierName")+" is Re-evalauted Successfully";
               StringBuilder alerttext=new StringBuilder();
                alerttext.append(" OnHidepopUp (); customsuccesslertmessage('");
                alerttext.append(vtitle);
                alerttext.append("','");
                alerttext.append(vtext);
              
                alerttext.append("')");
               JSutils.callJavaScriptMethod(alerttext.toString());
            System.out.println("donexx");
           iterator.executeQuery();
           justification=null;
        }
        
        else
        {
            System.out.println("error");
                String vtitle="Warning";
                               
                                StringBuilder alerttext=new StringBuilder();
                                   alerttext.append("customdangeralertmessage('");
                                   alerttext.append(vtitle);
                                   alerttext.append("','");
                                   alerttext.append(result);
                                    alerttext.append("')");
                                  JSutils.callJavaScriptMethod(alerttext.toString());  
            // show error
            }
        
       }
       else
       {
           // show warning message no selected record 
           String vtitle="Warning";
                           String   vtext="select a supplier First";
                           StringBuilder alerttext=new StringBuilder();
                              alerttext.append("customdangeralertmessage('");
                              alerttext.append(vtitle);
                              alerttext.append("','");
                              alerttext.append(vtext);
                               alerttext.append("')");
                             JSutils.callJavaScriptMethod(alerttext.toString());  
           }
       return null;
    }

    public void setJustification(String justification) {
        this.justification = justification;
    }

    public String getJustification() {
        return justification;
    }

    public void onSearchEvaloution(ClientEvent clientEvent) {
        // Add event code here...
    
        String value =clientEvent.getParameters().get("val").toString();
      
        System.out.println("value is "+value);
    
        DCBindingContainer dcbindings =
                          (DCBindingContainer)BindingContext.getCurrent().getCurrentBindingsEntry();
        //  DCIteratorBinding iterator =dcbindings.findIteratorBinding("");
        OperationBinding operationBinding2 = dcbindings.getOperationBinding("ExecuteWithParams");
        operationBinding2.getParamsMap().put("pSearchField",value);
        
        operationBinding2.execute();
        System.out.println("finished");
    }
}
