/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Example of Require.js boostrap javascript
 */


requirejs.config({
// Path mappings for the logical module names
    paths:
    //injector:mainReleasePaths
     {
        'knockout': 'libs/knockout/knockout-3.4.0',
        'jquery': 'libs/jquery/jquery-3.1.1.min',
        'jqueryui-amd': 'libs/jquery/jqueryui-amd-1.12.0.min',
        'promise': 'libs/es6-promise/es6-promise.min',
        'ojs': 'libs/oj/v3.1.0/min',
        'ojL10n': 'libs/oj/v3.1.0/ojL10n',
        'ojtranslations': 'libs/oj/v3.1.0/resources',
        'signals': 'libs/js-signals/signals.min',
        'text': 'libs/require/text',
        'hammerjs': 'libs/hammer/hammer-2.0.8.min',
        'moment': 'libs/moment/moment.min',
        'ojdnd': 'libs/dnd-polyfill/dnd-polyfill-1.0.0.min',
        'customElements': 'libs/webcomponents/CustomElements'
    }
    //endinjector
    ,
    // Shim configurations for modules that do not expose AMD
    shim: {
        'jquery': {
            exports: ['jQuery', '$']
        },
        'maps': {
            deps: ['jquery', 'i18n'],
            exports: ['MVMapView']
        }
    },
    // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
    // resources with a custom translation file.
    // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
    // a path that is relative to the location of this main.js file.
    config: {
        ojL10n: {
            merge: {
                //'ojtranslations/nls/ojtranslations': 'resources/nls/menu'
            }
        }
    }
});
/**
 * A top-level require call executed by the Application.
 * Although 'ojcore' and 'knockout' would be loaded in any case (they are specified as dependencies
 * by the modules themselves), we are listing them explicitly to get the references to the 'oj' and 'ko'
 * objects in the callback
 */

  require(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojgauge', 'ojs/ojchart'],
          function(oj, ko, $)
          {


      function ScoreChartModel() {
        var self = this;

        /* toggle button variables */
        self.orientationValue = ko.observable('vertical');

        /* chart data */
var lineSeries = [{name : "Quality Score", items : [1, 1, 1.5, 3]},
                          {name : "Cost Score", items : [4, 3, 4, 5]},
                          {name : "Service", items : [3, 2, 3, 4]},
                          {name : "Delivery", items : [5,4, 3, 2]}];

        var lineGroups = ["Q1", "Q2", "Q3", "Q4"];


        this.lineSeriesValue = ko.observableArray(lineSeries);
        this.lineGroupsValue = ko.observableArray(lineGroups);
    }

    var scorechartModel = new ScoreChartModel();







       function ChartModel() {
        var self = this;
        self.threeDValue = ko.observable('off');

        /* chart data */
        var pieSeries = [{name: "No of Pos Receipt meets or Equal Promised Date ", items: [30]},
                         {name: "No of Pos After Promised Date", items: [70]}];
             self.color1=ko.observable('#2477ab');
      self.color2=ko.observable('#ed6647');
        this.pieSeriesValue = ko.computed(function() {
       pieSeries[0]['color']=self.color1();
       pieSeries[1]['color']=self.color2();

      return pieSeries;
        });
    }

    var chartModel = new ChartModel();

       function statusmeterGaugeData() {
              var self = this;
              self.value1  = ko.observable(90);
              self.value2  = ko.observable(80);
              self.value3  = ko.observable(85);


            }



            var statusmeterGaugeModel = new statusmeterGaugeData();





    $(
	function(){
            ko.applyBindings(chartModel, document.getElementById('chart-container'));
             ko.applyBindings(scorechartModel, document.getElementById('scorechart-container'));
                   ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container1'));
               ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container2'));
                     ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container3'));

	}
    );






          });