/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Example of Require.js boostrap javascript
 */


requirejs.config({
// Path mappings for the logical module names
    paths:
    //injector:mainReleasePaths
     {
        'knockout': 'libs/knockout/knockout-3.4.0',
        'jquery': 'libs/jquery/jquery-3.1.1.min',
        'jqueryui-amd': 'libs/jquery/jqueryui-amd-1.12.0.min',
        'promise': 'libs/es6-promise/es6-promise.min',
        'ojs': 'libs/oj/v3.1.0/min',
        'ojL10n': 'libs/oj/v3.1.0/ojL10n',
        'ojtranslations': 'libs/oj/v3.1.0/resources',
        'signals': 'libs/js-signals/signals.min',
        'text': 'libs/require/text',
        'hammerjs': 'libs/hammer/hammer-2.0.8.min',
        'moment': 'libs/moment/moment.min',
        'ojdnd': 'libs/dnd-polyfill/dnd-polyfill-1.0.0.min',
        'customElements': 'libs/webcomponents/CustomElements'
    }
    //endinjector
    ,
    // Shim configurations for modules that do not expose AMD
    shim: {
        'jquery': {
            exports: ['jQuery', '$']
        },
        'maps': {
            deps: ['jquery', 'i18n'],
            exports: ['MVMapView']
        }
    },
    // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
    // resources with a custom translation file.
    // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
    // a path that is relative to the location of this main.js file.
    config: {
        ojL10n: {
            merge: {
                //'ojtranslations/nls/ojtranslations': 'resources/nls/menu'
            }
        }
    }
});
/**
 * A top-level require call executed by the Application.
 * Although 'ojcore' and 'knockout' would be loaded in any case (they are specified as dependencies
 * by the modules themselves), we are listing them explicitly to get the references to the 'oj' and 'ko'
 * objects in the callback
 */

  require(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojgauge', 'ojs/ojchart', 'ojs/ojsunburst'],
          function(oj, ko, $)
          {



                  function SunburstModel() {
        var self = this;
        var colorHandler = new oj.ColorAttributeGroupHandler();

        var JE = createNode("Jumbo Electronics", 11.5,"#ed6647");
        var AM = createNode("AXIOM",6, "#237bb1");
        var FT = createNode("FCM Travel", 8.5, "#68c182");
        var TC = createNode("Tecom", 7, "#fad55c");
        var EC = createNode("Emirates Computers",6, "#8561c8");


        addChildNodes(JE, [
          createNode("Delivery", 4, "#08519c"),
          createNode("Complain", 3, "#2171b5"),
          createNode("License", 1, "#4292c6") ,
          createNode("Quality", 2,"#6baed6"),
          createNode("Canceled Po's", 1.5, "#86cff9")
        ]);
        addChildNodes(AM, [
        createNode("Delivery", 2, "#08519c"),
          createNode("Complain", 1.5, "#2171b5"),
          createNode("License", 1, "#4292c6") ,
        createNode("Canceled Po's", 1.5, "#86cff9")
        ]);
        addChildNodes(FT, [
          createNode("Delivery", 3.5, "#08519c"),
          createNode("Complain", 3, "#2171b5"),
        createNode("Quality", 2,"#6baed6"),

        ]);
        addChildNodes(TC, [
          createNode("Delivery", 3, "#08519c"),
              createNode("License", 1, "#4292c6") ,
          createNode("Quality", 2,"#6baed6"),
          createNode("Canceled Po's", 1, "#86cff9")
        ]);
        addChildNodes(EC, [
          createNode("Delivery", 2.5, "#08519c"),
          createNode("Complain", 2, "#2171b5"),
        createNode("Canceled Po's", 1.5, "#86cff9")
        ]);

        var nodes = [JE, AM, FT, TC, EC];
        function createNode(label, value, color) {
          return {label: label,
                id: label,
                value: value,
                color: color,
                shortDesc: "&lt;b&gt;" + label +
                  "&lt;/b&gt;&lt;br/&gt;Value: " + value};
        }

        function addChildNodes(parent, childNodes) {
          parent.nodes = [];
          for (var i = 0; i < childNodes.length; i++) {
            parent.nodes.push(childNodes[i]);
          }
        }

        function getValue() {
            return Math.round(50 + 50 * Math.random());
        }

        function getColor() {
            return colorHandler.getValue(Math.floor(Math.random() * 4));
        }

        self.nodeValues = ko.observableArray(nodes);
        self.selectedNodesValue = ko.observableArray(
            []
        );
        self.selectionValue = ko.observable("single");


        self.selectionInfo = ko.pureComputed(function() {
          var items = "";
          var selection = self.selectedNodesValue();
          if (selection.length > 0) {
            items += "selected nodes:<br/>";
            for(var i = 0; i < selection.length; i++) {
              items += "    " + selection[i] + "<br/>";
              if(selection[i]=="AXIOM")
               window.location.href="SupplierPerformance.html"  
             // alert(selection[i]);
            }
          }
          return items;
        });
    }
            var sunburstModel = new SunburstModel();
            $(
      function(){

       ko.applyBindings(sunburstModel, document.getElementById('sunburst-container'));

      }
    );

          });