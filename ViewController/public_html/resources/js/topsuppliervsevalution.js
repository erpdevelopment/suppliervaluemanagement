/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Example of Require.js boostrap javascript
 */


requirejs.config({
// Path mappings for the logical module names
    paths:
    //injector:mainReleasePaths
     {
        'knockout': 'libs/knockout/knockout-3.4.0',
        'jquery': 'libs/jquery/jquery-3.1.1.min',
        'jqueryui-amd': 'libs/jquery/jqueryui-amd-1.12.0.min',
        'promise': 'libs/es6-promise/es6-promise.min',
        'ojs': 'libs/oj/v3.1.0/min',
        'ojL10n': 'libs/oj/v3.1.0/ojL10n',
        'ojtranslations': 'libs/oj/v3.1.0/resources',
        'signals': 'libs/js-signals/signals.min',
        'text': 'libs/require/text',
        'hammerjs': 'libs/hammer/hammer-2.0.8.min',
        'moment': 'libs/moment/moment.min',
        'ojdnd': 'libs/dnd-polyfill/dnd-polyfill-1.0.0.min',
        'customElements': 'libs/webcomponents/CustomElements'
    }
    //endinjector
    ,
    // Shim configurations for modules that do not expose AMD
    shim: {
        'jquery': {
            exports: ['jQuery', '$']
        },
        'maps': {
            deps: ['jquery', 'i18n'],
            exports: ['MVMapView']
        }
    },
    // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
    // resources with a custom translation file.
    // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
    // a path that is relative to the location of this main.js file.
    config: {
        ojL10n: {
            merge: {
                //'ojtranslations/nls/ojtranslations': 'resources/nls/menu'
            }
        }
    }
});
/**
 * A top-level require call executed by the Application.
 * Although 'ojcore' and 'knockout' would be loaded in any case (they are specified as dependencies
 * by the modules themselves), we are listing them explicitly to get the references to the 'oj' and 'ko'
 * objects in the callback
 */

  require(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojgauge', 'ojs/ojchart'],
          function(oj, ko, $)
          {



                  function scoreChartModel() {
        var self = this;

        /* toggle button variables */
        self.orientationValue = ko.observable('vertical');
        self.dualY = ko.observable('off');

        self.splitterValue = ko.observable(0.9);

        /* chart data */
        var dualYSeries = [{name: "Evalution Score", items: [9, 2.1, 4.5,8.3],color:"#c14141",assignedToY2: "on"},
                           {name: " Risk Score", items: [2, 3, 2.5,5],color:"#267cb1" }];

        var dualYGroups = ["Jumbo Electronics", "Axiom", "Tecom", "FCM Travle"];

        self.barSeriesValue = ko.observableArray(dualYSeries);
        self.barGroupsValue = ko.observableArray(dualYGroups);


    }
            var scorechartModel = new scoreChartModel();
            $(
      function(){

        ko.applyBindings(scoreChartModel, document.getElementById('score-chartcontainer'));
      }
    );





                 function scoreChartModel2() {
        var self = this;

        /* toggle button variables */
        self.orientationValue2 = ko.observable('vertical');
        self.dualY2 = ko.observable('off');

        self.splitterValue2 = ko.observable(0.9);

        /* chart data */
        var dualYSeries2 = [{name: "Spend", items: [1000, 6000,5000,6000],color:"#c14141",assignedToY2: "on"},
                           {name: "Score", items: [1, 1.5,1,2],color:"#267cb1" }];

        var dualYGroups2 = ["Jumbo Electronics", "Axiom", "Tecom", "FCM Travle"];

        self.barSeriesValue2 = ko.observableArray(dualYSeries2);
        self.barGroupsValue2 = ko.observableArray(dualYGroups2);


    }

             var scorechartModel2 = new scoreChartModel2();
            $(
      function(){

        ko.applyBindings(scoreChartModel2, document.getElementById('score-chartcontainer2'));
      }
    );


       function ChartModel() {
        var self = this;
        self.threeDValue = ko.observable('on');

        /* chart data */
        var pieSeries = [{name: "Operational", items: [40]},
                         {name: "Compliance", items: [30]},
                         {name: "Quality", items: [20]},{name: "Strategic", items: [10]}];

        this.pieSeriesValue = ko.observableArray(pieSeries);
    }

    var chartModel = new ChartModel();

    $(
	function(){
            ko.applyBindings(chartModel, document.getElementById('chart-container'));
	}
    );

                      function ChartStackModel2() {
        var self = this;

        /* toggle button variables */
         self.stackValue = ko.observable('on');
        self.stackLabelValue = ko.observable('on');
        self.orientationValue = ko.observable('vertical');
        self.labelPosition = ko.observable('auto');

        /* chart data */
        var comboSeries = [{name: "Operational", items: [{y:3, label:"3"}
                                         , {y:2, label:"2"}
                                                ,{y:1, label:"1"}
                                                ,{y:1,label:"1"}, {y:1, label:"1"}]}
                                 ,
                               {name: "Compliance", items: [{y:2, label:"2"}
                                         , {y:2, label:"2"}
                                                ,{y:1, label:"1"}
                                                ,{y:1,label:"1"}, {y:1, label:"1"}]}

                                                          ,
                               {name: "Quality", items: [{y:4, label:"4"}
                                         , {y:2, label:"2"}
                                                ,{y:1, label:"1"}
                                                ,{y:1,label:"1"}, {y:0, label:"1"}]}
                                                                          ,
                               {name: "Strategic", items: [{y:1, label:"1"}
                                         , {y:2, label:"2"}
                                                ,{y:3, label:"3"}
                                                ,{y:3,label:"3"}, {y:2, label:"2"}]}
                        ];




        var comboGroups =  ["JUMBO ELECTRONICS", "AXIOM ", "FCM TRAVEL ","TECOM","EMIRATES COMPUTERS"];

        this.comboSeriesValue = ko.observableArray(comboSeries);
        this.comboGroupsValue = ko.observableArray(comboGroups);


    }
         var chartStackModel2 = new ChartStackModel2();

    $(
    function(){
            ko.applyBindings(chartStackModel2, document.getElementById('chartstackcontainer'));

    }
    );




          });