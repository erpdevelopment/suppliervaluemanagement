/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 * Example of Require.js boostrap javascript
 */
  function getBusinessUnit()
  {
  
var bg= document.getElementById('businessunit').value;
 return ",pBusinessUnit="+bg;
  
  
  }
function getURL()
{
return  "http://10.10.131.34:7003/svmRest/charts/v1";
//"http://10.10.131.34:7003/svmRest/charts/v1";
//return   "http://10.10.131.34:7003/svmRest/charts/v1";
}
 function getsvmcurrentyear()
 {
 
 return new Date().getFullYear();
 
 } 
  function getComboGroups( values)
  {
var comboGroups= [];
  for (var i = 0;i < values.length;i++) {
    
     comboGroups.push({name: values[i].vname});
    
                }
   return comboGroups;             
  }
 
 function getComboSeries( values)

{
var comboSeries=[];
 if(values.length==5)
{
       comboSeries=       [{name: "", items: [{value:values[0].vSupplierSpend,color:"#267cb1"}, {value:values[1].vSupplierSpend,color:"#68c182"}, {value:values[2].vSupplierSpend,color:"#eecb58"}, {value:values[3].vSupplierSpend,color:"#ed6647"}, {value:values[4].vSupplierSpend,color:"#8561c8"}]}  ,
             {name: "", items: [{value:values[0].vEvaluationScore,color:"#8bc4e5"}, {value:values[1].vEvaluationScore,color:"#8bdda3"}, {value:values[2].vEvaluationScore,color:"#f2d885"}, {value:values[3].vEvaluationScore,color:"#ed927d"}, {value:values[4].vEvaluationScore,color:"#ac91e0"}],assignedToY2: "on"}
             ];
}
else
             if(values.length==4)
{
       comboSeries=       [{name: "", items: [{value:values[0].vSupplierSpend,color:"#267cb1"}, {value:values[1].vSupplierSpend,color:"#68c182"}, {value:values[2].vSupplierSpend,color:"#eecb58"}, {value:values[3].vSupplierSpend,color:"#ed6647"}]}  ,
             {name: "", items: [{value:values[0].vEvaluationScore,color:"#8bc4e5"}, {value:values[1].vEvaluationScore,color:"#8bdda3"}, {value:values[2].vEvaluationScore,color:"#f2d885"}, {value:values[3].vEvaluationScore,color:"#ed927d"}],assignedToY2: "on"}
             ];
}

else
             if(values.length==3)
{
       comboSeries=       [{name: "", items: [{value:values[0].vSupplierSpend,color:"#267cb1"}, {value:values[1].vSupplierSpend,color:"#68c182"}, {value:values[2].vSupplierSpend,color:"#eecb58"}]}  ,
             {name: "", items: [{value:values[0].vEvaluationScore,color:"#8bc4e5"}, {value:values[1].vEvaluationScore,color:"#8bdda3"}, {value:values[2].vEvaluationScore,color:"#f2d885"}],assignedToY2: "on"}
             ];
}
else
             if(values.length==2)
{
       comboSeries=       [{name: "", items: [{value:values[0].vSupplierSpend,color:"#267cb1"}, {value:values[1].vSupplierSpend,color:"#68c182"}]}  ,
             {name: "", items: [{value:values[0].vEvaluationScore,color:"#8bc4e5"}, {value:values[1].vEvaluationScore,color:"#8bdda3"}],assignedToY2: "on"}
             ];
}
else
             if(values.length==1)
{
       comboSeries=       [{name: "", items: [{value:values[0].vSupplierSpend,color:"#267cb1"}]}  ,
             {name: "", items: [{value:values[0].vEvaluationScore,color:"#8bc4e5"}],assignedToY2: "on"}
             ];
}

return comboSeries;
}
 function loadtopProductServices()
{




 var url=
 getURL()+"/svmPrdsrvvo1";




 var ProductServices=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            ProductServices.push({vname:jsonItemAttrs.Prdname,vcount:jsonItemAttrs.Prdcount});
                  });
});

  $.ajaxSetup( {
        async : true
    });
return ProductServices;
}
function productservicesarray()
           {
            
              var series=[];
            var values = loadtopProductServices();
   
    for (var i = 0;i < values.length;i++) {

     series.push({name: values[i].vname, items: [{value:values[i].vcount}]});
                }
                          return series ;
           
           }
 function loadEvaluationStatus(year , quarter)
{ 

if (!year)
year=getsvmcurrentyear();
if (!quarter)
quarter='-1';

 var url=
 getURL()+"/svmevaluationstatus"+"?finder=RowFinder;pYear="+year+",pQuarter="+quarter +getBusinessUnit();
 console.log(url);

 
 var EvaluationStatus=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            EvaluationStatus.push({vname:jsonItemAttrs.Status,vcount:jsonItemAttrs.Statuscount,vselector:jsonItemAttrs.Selector});
                  });
});

  $.ajaxSetup( {
        async : true
    });
return EvaluationStatus;
}
function evaluationstatusarray(year , quarter)
           {
           
              var series=[];
            var values = loadEvaluationStatus(year,quarter);
            var vColor;
   
    for (var i = 0;i < values.length;i++) {
   
   if (values[i].vname=='Completed')
   {
   vColor='#eecb58';
   }
   else if (values[i].vname=='InProcess')
   {
   vColor='#68c182';
   }
  else if (values[i].vname=='Closed')
  {
  vColor='#ea6546';
  }
   else
   {
   vColor='#267db3';
   }
   
   
     series.push({name: values[i].vname, items: [{value:values[i].vcount,id:values[i].vselector}],color:vColor});
                }
                          return series ;
           
           }
           
           
            function loadEvaluationfeedBack(year , quarter)
{


 
   if (!year)
year=getsvmcurrentyear();
if (!quarter)
quarter='-1';

 var url=
 getURL()+"/svmEvaluationfeddBack1"+"?finder=RowFinder;pYear="+year+",pQuarter="+quarter+getBusinessUnit();
 

 var EvaluationFeedBack=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            EvaluationFeedBack.push({vname:jsonItemAttrs.Feedback,vNumofresponse:jsonItemAttrs.Numofresponse,vselector:jsonItemAttrs.Selector});
                  });
});

  $.ajaxSetup( {
        async : true
    });
return EvaluationFeedBack;
}
function evaluationfeedbackarray(year , quarter)
           {
           
              var series=[];
            var values = loadEvaluationfeedBack(year , quarter);
   
    for (var i = 0;i < values.length;i++) {
      if(i==0)
      {
       series.push({name: values[i].vname, items: [{value:values[i].vNumofresponse,id:values[i].vselector}],pieSliceExplode:"0.9"});
      }
      else
      {
     series.push({name: values[i].vname, items: [{value:values[i].vNumofresponse,id:values[i].vselector}]});
      }
                }
    

                          return series ;
           
           }
        
           
            function loadEvaluationResponse(year , quarter)
{

  
		  if (!year)
year=getsvmcurrentyear();
if (!quarter)
quarter='-1';

 var url=
 getURL()+"/svmevaluationresponseanalysisvo1"+"?finder=RowFinder;pYear="+year+",pQuarter="+quarter+getBusinessUnit();
 

 var EvaluationResponse=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            EvaluationResponse.push({vname:jsonItemAttrs.VendorName,vCost:jsonItemAttrs.Cost,vDelivery:jsonItemAttrs.Delivery,vService:jsonItemAttrs.Service,vQuality:jsonItemAttrs.Quality});
                  });
});

  $.ajaxSetup( {
        async : true
    });
return EvaluationResponse;
}
function evaluationresponsearray(year , quarter)
           {
            
            var series=[];
            var costseries=[];
            var deliveryseries=[];
            var qualityseries=[];
            var serviceseries=[];
           
            var values = loadEvaluationResponse(year , quarter);
   
    for (var i = 0;i < values.length;i++) {
    costseries.push({y:values[i].vCost, label:values[i].vCost})
    deliveryseries.push({y:values[i].vDelivery, label:values[i].vDelivery})
    qualityseries.push({y:values[i].vQuality, label:values[i].vQuality})
    serviceseries.push({y:values[i].vService, label:values[i].vService})
    }
    
     series.push({name: "Cost", items: costseries});
     series.push({name: "Delivery", items: deliveryseries});
     series.push({name: "Quality", items: qualityseries});
     series.push({name: "Service", items:serviceseries });

    

                          return series ;
           
           }
        function evaluationresponsesupplierarray(year , quarter)
           {
           
              var series=[];
            var values = loadEvaluationResponse(year,quarter);
   
    for (var i = 0;i < values.length;i++) {
    
     series.push({name: values[i].vname});
    
                }
    

                          return series ;
           
           }
           
   function loadsupplierspendevaluationscore(year , quarter)
{


		  if (!year)
year=getsvmcurrentyear();
if (!quarter)
quarter='-1';

 var url=
 getURL()+"/svmspendanalysisvsevaluationscorevo1"+"?finder=RowFinder;pYear="+year+",pQuarter="+quarter+getBusinessUnit();


 
 

 var EvaluationSpendScore=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            EvaluationSpendScore.push({vname:jsonItemAttrs.SupplierName,vSupplierSpend:jsonItemAttrs.Supplierspend,vEvaluationScore:jsonItemAttrs.Evaluationscore});
                  });
});

  $.ajaxSetup( {
        async : true
    });
return EvaluationSpendScore;
}

           
           
           
           
            function loadSurveyStatus (year , quarter)
{
 
 
 if (!year)
year=getsvmcurrentyear();
if (!quarter)
quarter='-1';

 var url=
 getURL()+"/svmsurveystatus"+"?finder=RowFinder;pYear="+year+",pQuarter="+quarter+getBusinessUnit();
 

 
 

 var SurveyStatus=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            SurveyStatus.push({vname:jsonItemAttrs.Status,vcount:jsonItemAttrs.Statuscount,vselector:jsonItemAttrs.Selector});
                  });
});

  $.ajaxSetup( {
        async : true
    });
return SurveyStatus;
}
function surveystatusarray (year , quarter)
           {
           
              var series=[];
            var values = loadSurveyStatus (year , quarter);
   
    for (var i = 0;i < values.length;i++) {
   
     series.push({name: values[i].vname, items: [{value:values[i].vcount,id:values[i].vselector}]});
                }
                          return series ;
           
           }
           
           
           
             function loadsurveyfeedBack(year , quarter)
{

//(new Date().getFullYear())-1;
//alert(year);
// var url=
// getURL()+"/svmRest/charts/v1/svmsurveyfeedbackvo1?limit=100&finder=RowFinder;psurveyyear="+year;
 
 
 if (!year)
year=getsvmcurrentyear();
if (!quarter)
quarter='-1';

 var url=
 getURL()+"/svmsurveyfeedbackvo1"+"?finder=RowFinder;psurveyyear="+year+",pQuarter="+quarter+getBusinessUnit();
 

 var SurveyFeedBack=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            SurveyFeedBack.push({vname:jsonItemAttrs.Feedback,vNumofresponse:jsonItemAttrs.Numofresponse,vselector:jsonItemAttrs.Selector});
                  });
});

  $.ajaxSetup( {
        async : true
    });
return SurveyFeedBack;
}
function surveyfeedbackarray(year , quarter)
           {
           
              var series=[];
            var values = loadsurveyfeedBack(year , quarter);
   
    for (var i = 0;i < values.length;i++) {
   
     series.push({name: values[i].vname, items: [{value:values[i].vNumofresponse,id:values[i].vselector}]});
     
                }
                                   return series ;
           
           }
        
      
requirejs.config({
// Path mappings for the logical module names
    paths:
    //injector:mainReleasePaths
     {
        'knockout': 'libs/knockout/knockout-3.4.0',
        'jquery': 'libs/jquery/jquery-3.1.1.min',
        'jqueryui-amd': 'libs/jquery/jqueryui-amd-1.12.0.min',
        'promise': 'libs/es6-promise/es6-promise.min',
        'ojs': 'libs/oj/v3.1.0/min',
        'ojL10n': 'libs/oj/v3.1.0/ojL10n',
        'ojtranslations': 'libs/oj/v3.1.0/resources',
        'signals': 'libs/js-signals/signals.min',
        'text': 'libs/require/text',
        'hammerjs': 'libs/hammer/hammer-2.0.8.min',
        'moment': 'libs/moment/moment.min',
        'ojdnd': 'libs/dnd-polyfill/dnd-polyfill-1.0.0.min',
        'customElements': 'libs/webcomponents/CustomElements'
    }
    //endinjector
    ,
    // Shim configurations for modules that do not expose AMD
    shim: {
        'jquery': {
            exports: ['jQuery', '$']
        },
        'maps': {
            deps: ['jquery', 'i18n'],
            exports: ['MVMapView']
        }
    },
    // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
    // resources with a custom translation file.
    // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
    // a path that is relative to the location of this main.js file.
    config: {
        ojL10n: {
            merge: {
                //'ojtranslations/nls/ojtranslations': 'resources/nls/menu'
            }
        }
    }
});
/**
 * A top-level require call executed by the Application.
 * Although 'ojcore' and 'knockout' would be loaded in any case (they are specified as dependencies
 * by the modules themselves), we are listing them explicitly to get the references to the 'oj' and 'ko'
 * objects in the callback
 */
require(['ojs/ojcore',
    'knockout',
    'jquery',
    'utils',
    'ojs/ojrouter',
    'ojs/ojknockout',
    'ojs/ojmodule',
    'ojs/ojbutton',
    'ojs/ojtoolbar',
    'ojs/ojmenu',
    'ojs/ojinputtext','ojs/ojmasonrylayout','ojs/ojchart'  ,'ojs/ojgauge'
],
        function (oj, ko, $) {

            function MainViewModel() {

            }

   




   function BarChartModel() {
        var self = this;
   // Spend Analysis VS Evaluation Score
      
        self.stackValue = ko.observable('off');
        self.orientationValue = ko.observable('vertical');
                self.dualY = ko.observable('off');
                self.splitterValue = ko.observable(0.1);

        /* chart data */
        
        var values = loadsupplierspendevaluationscore();
       var comboSeries = getComboSeries(values);
       
       console.log(values.length);
              

       
//       [{name: "", items: [{value:values[0].vSupplierSpend,color:"#267cb1"}, {value:values[1].vSupplierSpend,color:"#68c182"}, {value:values[2].vSupplierSpend,color:"#eecb58"}, {value:values[3].vSupplierSpend,color:"#ed6647"}, {value:values[4].vSupplierSpend,color:"#8561c8"}]}  ,
//             {name: "", items: [{value:values[0].vEvaluationScore,color:"#8bc4e5"}, {value:values[1].vEvaluationScore,color:"#8bdda3"}, {value:values[2].vEvaluationScore,color:"#f2d885"}, {value:values[3].vEvaluationScore,color:"#ed927d"}, {value:values[4].vEvaluationScore,color:"#ac91e0"}],assignedToY2: "on"}
//             ];
             

         var comboGroups =getComboGroups(values);
         


        this.comboSeriesValue = ko.observableArray(comboSeries);
        this.comboGroupsValue = ko.observableArray(comboGroups);
        
          var converterFactory = oj.Validation.converterFactory('number');
        var decimalConverter = converterFactory.createConverter({minimumFractionDigits: 0,maximumFractionDigits: 1});
         self.numberConverter = ko.observable(decimalConverter);
        self.formatValue = ko.observable('decimal');
      self.formatOptions = [
            {id: 'decimal', label: 'decimal', value: 'decimal'}
        ];
    }

    var barchartModel = new BarChartModel();

    $(
    function(){
            ko.applyBindings(barchartModel, document.getElementById('chart-container'));
    }
    );
           function ChartStackModel1() {
        var self = this;
 // Evaluation Response Analysis
        /* toggle button variables */
         self.stackValue = ko.observable('on');
        self.stackLabelValue = ko.observable('on');
        self.orientationValue = ko.observable('vertical');
        self.labelPosition = ko.observable('auto');

        /* chart data    */
        var comboSeries = evaluationresponsearray();
        var comboGroups =  evaluationresponsesupplierarray();
        this.comboSeriesValue2 = ko.observableArray(comboSeries);
        this.comboGroupsValue2 = ko.observableArray(comboGroups);
        
          var converterFactory = oj.Validation.converterFactory('number');
        var decimalConverter = converterFactory.createConverter({minimumFractionDigits: 0,maximumFractionDigits: 1});
         self.numberConverter = ko.observable(decimalConverter);
        self.formatValue = ko.observable('decimal');
      self.formatOptions = [
            {id: 'decimal', label: 'decimal', value: 'decimal'}
        ];


    }
         var chartStackModel1 = new ChartStackModel1();

    $(
    function(){
            ko.applyBindings(chartStackModel1, document.getElementById('chartstackcontainer1'));

    }
    );

        
         function ChartBarSupplierModel() {
         
         // Supplier Risk Score //
        var self = this;
              self.stackValue = ko.observable('off');
        self.orientationValue = ko.observable('vertical');
 
        var barSeries = [{name: "", items: [5, 4.5,4.2,4,3.9]}];

        var barGroups = ["JUMBO ELECTRONICS44", "AXIOM ", "FCM TRAVEL ","TECOM","EMIRATES COMPUTERS"]

        self.barSeriesValue = ko.observableArray(barSeries);
        self.barGroupsValue = ko.observableArray(barGroups);
        
          var converterFactory = oj.Validation.converterFactory('number');
        var decimalConverter = converterFactory.createConverter({minimumFractionDigits: 0,maximumFractionDigits: 1});
         self.numberConverter = ko.observable(decimalConverter);
        self.formatValue = ko.observable('decimal');
      self.formatOptions = [
            {id: 'decimal', label: 'decimal', value: 'decimal'}
        ];
    }

    var chartbarsModel = new ChartBarSupplierModel();

    $(
	function(){
            ko.applyBindings(chartbarsModel, document.getElementById('chartbars-container'));
	}
    );

 
   ////----------Selection values for drill down ///
       //   doc = AdfPage.PAGE.findComponent('d1');
         var val = document.getElementById('status');//doc.getProperty("drill");
         val=val.value;
       //--------------end selection values//



          function ChartPieModel() {
         var self = this;
          self.threeDValue = ko.observable('on');
          self.modeValue = ko.observable('single');
       
            //Chart1 Survey Response//
                   var pieSeries1 = surveystatusarray();
                  this.pieSeriesValue1 = ko.observableArray(pieSeries1);
                   var converterFactory = oj.Validation.converterFactory('number');
        var decimalConverter = converterFactory.createConverter({minimumFractionDigits: 0,maximumFractionDigits: 1});
         self.numberConverter = ko.observable(decimalConverter);
        self.formatValue = ko.observable('decimal');
      self.formatOptions = [
            {id: 'decimal', label: 'decimal', value: 'decimal'}
        ];
         ////Coloring ----
            //closed
             self.color11=ko.observable('#ea6546');
             //Schedlued 
            self.color21=ko.observable('#267db3');
           //inProcess
            self.color31=ko.observable('#68c182');
          //Completed
          self.color41=ko.observable('#eecb58');
        
        self.pieSeriesValue1 = ko.computed(function() {
      for (var l=0 ;l<pieSeries1.length;l++)
            {
           if(pieSeries1[l]['name']=='Completed')
             {

              pieSeries1[l]['color']=self.color41();
              }
             else  if(pieSeries1[l]['name']=='InProcess')
             {
          
             pieSeries1[l]['color']=self.color31();
             }
              else  if(pieSeries1[l]['name']=='Closed')
              {
            
              pieSeries1[l]['color']=self.color11();
              }
             else    if(pieSeries1[l]['name']=='Scheduled')
             {
               
             pieSeries1[l]['color']=self.color21();
             
             }
              
            
            }


      return pieSeries1;
        });
        
        
        
        
        
        
// in html  value-formats.label.converter="[[numberConverter]]"
              //  value-formats.value.converter="[[numberConverter]]" 
                 ///-----------Selection-------////
        
          var selection = [];
         self.selectionValue = ko.observableArray(selection);
        
          self.selectionInfo = ko.pureComputed(function() {
          var items = "";
          selection = self.selectionValue();
          if (selection.length > 0) {
            items += "items:<br/>";
            for(var i = 0; i < selection.length; i++) {
              items += "    " + selection[i] + "<br/>";
            if (val =='A'||val=='S')  // for drill down 
         {
          if(selection[i]=="S1")
            window.location.href="../pages/SurveyClosed.jsf"
            else  if(selection[i]=="S2")
             window.location.href="../pages/Surveyscheduled.jsf"
             else  if(selection[i]=="S3")
             window.location.href="../pages/SurveyInprocess.jsf"
             else  if(selection[i]=="S4")
             window.location.href="../pages/SurveyCompleted.jsf"
        
           }  // end if for drill down
         else
         {
      //   alert (val);
          return ;
         }

             
            }
          }
          return items;
        });
  
      
                 //Chart2 Evaluation Response//
                   var pieSeries2 = evaluationstatusarray();
               this.pieSeriesValue2 = ko.observableArray(pieSeries2);
           /*   
            ////Coloring ----
            //closed
             self.color1=ko.observable('#ea6546');
             //Schedlued 
            self.color2=ko.observable('#267db3');
           //inProcess
            self.color3=ko.observable('#68c182');
          //Completed
          self.color4=ko.observable('#eecb58');
        
        self.pieSeriesValue2 = ko.computed(function() {
      for (var l=0 ;l<pieSeries2.length;l++)
            {
           if(pieSeries2[l]['name']=='Completed')
             {

              pieSeries2[l]['color']=self.color4();
              }
             else  if(pieSeries2[l]['name']=='InProcess')
             {
          
             pieSeries2[l]['color']=self.color3();
             }
              else  if(pieSeries2[l]['name']=='Closed')
              {
            
              pieSeries2[l]['color']=self.color1();
              }
             else    if(pieSeries2[l]['name']=='Scheduled')
             {
               
             pieSeries2[l]['color']=self.color2();
             
             }
              
            
            }


      return pieSeries2;
        });
         */   
             var selection2 = [];
         self.selectionValue2 = ko.observableArray(selection2);
          self.selectionInfo2 = ko.pureComputed(function() {
          var items2 = "";
          selection2 = self.selectionValue2();
          if (selection2.length > 0) {
            items2 += "items:<br/>";
            for(var ii = 0; ii < selection2.length; ii++) {
              items2 += "    " + selection2[ii] + "<br/>";
          
              if (val =='A'||val=='E')  // for drill down 
         {
             if(selection2[ii]=="S1")
           window.location.href="../pages/EvalClosed.jsf"
           else  if(selection2[ii]=="S2")
           window.location.href="../pages/EvalScheduled.jsf"     
               else  if(selection2[ii]=="S3")
                  window.location.href="../pages/EvalInProcess.jsf"
                 else  if(selection2[ii]=="S4")
             window.location.href="../pages/EvalCompleted.jsf"
         
           
        
           }  // end if for drill down
         else
         {
       //  alert (val);
          return ;
         }
       
            }
          }
          return items2;
        });





           //Chart3 Suppliers Info//
           
           var pieSeriesDate=productservicesarray();
           this.pieSeriesValue3 = ko.observableArray(pieSeriesDate);
   
        
                 ///--------Chart4 Survey FeedBack-------////



        var pieSeries4 =surveyfeedbackarray();
     this.pieSeriesValue4 = ko.observableArray(pieSeries4);
   //Satisified
       self.colorsat=ko.observable('#68c182');
       //UnSatisfied
      self.colorunsat=ko.observable('#ed6647');
      //('#512f2f');
      //;
 
     ///Colorng///
          self.pieSeriesValue4 = ko.computed(function() {
      for (var l=0 ;l<pieSeries4.length;l++)
            {
           if(pieSeries4[l]['name']=='Satisfied')
             {

              pieSeries4[l]['color']=self.colorsat();
              }
             else  if(pieSeries4[l]['name']=='UnSatisfied')
             {
          
             pieSeries4[l]['color']=self.colorunsat();
             }
           
           
              
            
            }


      return pieSeries4;
        });
 
 
//      self.pieSeriesValue4 = ko.computed(function() {
//       pieSeries4[0]['color']=self.color1();
//       pieSeries4[1]['color']=self.color2();
//      
//      return pieSeries4;
//        });





                     var selection4 = [];
         self.selectionValue4 = ko.observableArray(selection4);
          self.selectionInfo4 = ko.pureComputed(function() {
          var items4 = "";
          selection4 = self.selectionValue4();
          if (selection4.length > 0) {
            items4 += "items:<br/>";
            for(var i4 = 0; i4 < selection4.length; i4++) {
              items4 += "    " + selection4[i4] + "<br/>";
                   if (val =='A'||val=='S')  // for drill down 
         {
            //    alert(selection4[i4]);
               if(selection4[i4]=="s1")
                window.location.href="../pages/SatisifiedFeedBack.jsf"
               else  if(selection4[i4]=="s2")
                window.location.href="../pages/UnsatisifiedFeedBack.jsf"
         } 
         else 
         {
         
         
       //  alert(val);
         return ;
         }
//                
            }
          }
          return items4;
        });






                 //// Donut Evaluation Feedback//
                       self.innerRadius = ko.observable(0.5);
        self.centerLabel = ko.observable('Evaluation Feedback');
          self.labelStyle = ko.observable({fontSize:'15px',color:'#999999'});
      var pieSeries5 =evaluationfeedbackarray();
      ///Over Expectations
      self.colorover=ko.observable('#68c182');
      //Under Expecatations
      self.colorunder=ko.observable('#ed6647');
      //Meet Expecatations
      self.colormeet=ko.observable('#267db3');
    ///Coloring
      
           self.pieSeriesValue5 = ko.computed(function() {
      for (var l=0 ;l<pieSeries5.length;l++)
            {
           if(pieSeries5[l]['name']=='Over Expectations')
             {

              pieSeries5[l]['color']=self.colorover();
              }
             else  if(pieSeries5[l]['name']=='Under Expectation')
             {
          
             pieSeries5[l]['color']=self.colorunder();
             }
              else if(pieSeries5[l]['name']=='Meet Expectations') 
             
              {
             // console.log(pieSeries5[l]['name']);
            
              pieSeries5[l]['color']=self.colormeet();
              }
           
              
            
            }


      return pieSeries5;
        });
      
      
      
      
      
//      self.pieSeriesValue5 = ko.computed(function() {
//       pieSeries5[0]['color']=self.color1();
//       pieSeries5[1]['color']=self.color2();
//       pieSeries5[2]['color']=self.color3();
//      return pieSeries5;
//        });




                     var selection5 = [];
         self.selectionValue5 = ko.observableArray(selection5);
          self.selectionInfo5 = ko.pureComputed(function() {
          var items5 = "";
          selection5 = self.selectionValue5();
          if (selection5.length > 0) {
            items5 += "items:<br/>";
            for(var i5 = 0; i5 < selection5.length; i5++) {
              items5 += "    " + selection4[i5] + "<br/>";
     if (val =='A'||val=='E')  // for drill down 
         {
               if(selection5[i5]=="s1")
             window.location.href="../pages/OverExpectationFeedBack.jsf"
             else       if(selection5[i5]=="s2")
             window.location.href="../pages/UnderExpectationFeedBack.jsf"
             else       if(selection5[i5]=="s3")
             window.location.href="../pages/MeetExepecatationFeedBack.jsf"
         }
         else 
         {
         
        // alert(val);
         return ;
         }
          
            }
          }
          return items5;
        });




        }
       var piechart=new ChartPieModel();
    $(function(){

      ko.applyBindings(piechart,document.getElementById('piechartcontainer'));
    })  ;

                $(
    function(){
            ko.applyBindings(new MainViewModel(), document.getElementById('globalBody'));

         $('#globalBody').show();
 }  );


        }




);
