/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Example of Require.js boostrap javascript
 */


requirejs.config({
// Path mappings for the logical module names
    paths:
    //injector:mainReleasePaths
     {
        'knockout': 'libs/knockout/knockout-3.4.0',
        'jquery': 'libs/jquery/jquery-3.1.1.min',
        'jqueryui-amd': 'libs/jquery/jqueryui-amd-1.12.0.min',
        'promise': 'libs/es6-promise/es6-promise.min',
        'ojs': 'libs/oj/v3.1.0/min',
        'ojL10n': 'libs/oj/v3.1.0/ojL10n',
        'ojtranslations': 'libs/oj/v3.1.0/resources',
        'signals': 'libs/js-signals/signals.min',
        'text': 'libs/require/text',
        'hammerjs': 'libs/hammer/hammer-2.0.8.min',
        'moment': 'libs/moment/moment.min',
        'ojdnd': 'libs/dnd-polyfill/dnd-polyfill-1.0.0.min',
        'customElements': 'libs/webcomponents/CustomElements'
    }
    //endinjector
    ,
    // Shim configurations for modules that do not expose AMD
    shim: {
        'jquery': {
            exports: ['jQuery', '$']
        },
        'maps': {
            deps: ['jquery', 'i18n'],
            exports: ['MVMapView']
        }
    },
    // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
    // resources with a custom translation file.
    // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
    // a path that is relative to the location of this main.js file.
    config: {
        ojL10n: {
            merge: {
                //'ojtranslations/nls/ojtranslations': 'resources/nls/menu'
            }
        }
    }
});
/**
 * A top-level require call executed by the Application.
 * Although 'ojcore' and 'knockout' would be loaded in any case (they are specified as dependencies
 * by the modules themselves), we are listing them explicitly to get the references to the 'oj' and 'ko'
 * objects in the callback
 */

  require(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojgauge', 'ojs/ojchart'],
          function(oj, ko, $)
          {
            function statusmeterGaugeData() {
              var self = this;
              self.value  = ko.observable(90);
              self.value2  = ko.observable(70);
              self.value3  = ko.observable(60);
               self.value4  = ko.observable(45);
             /*    self.value5  = ko.observable(90);
              self.value6  = ko.observable(45);
              self.value7  = ko.observable(75);
               self.value8  = ko.observable(85);
                 self.value9  = ko.observable(60);
               self.value10  = ko.observable(45);*/
           self.label = {text: 'Sales'};





            }



            var statusmeterGaugeModel = new statusmeterGaugeData();

      
            $(
      function(){
          ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container'));
               ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container2'));
                     ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container3'));
                       ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container4'));
                     /*     ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container5'));
               ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container6'));
                     ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container7'));
                       ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container8'));
                           ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container9'));
                       ko.applyBindings(statusmeterGaugeModel, document.getElementById('gauge-container10'));*/
      }
    );

          });