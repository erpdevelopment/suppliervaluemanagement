/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Example of Require.js boostrap javascript
 */
 
  
              function loadsurveyfeedBack()
{
 //var url=
 //"http://http://10.10.131.34:70031/svmRest/charts/v1//svmsurveyfeedbackvo1";
// "http://http://10.10.131.34:70031/svmRest/charts/v1/svmsurveyfeedbackvo1";
  var currsurveyId = document.getElementById('CurrsurveyId');
   var curryearId = document.getElementById('CurrYearId');
   var currsupplierid=document.getElementById('CurrvendorId');
   var surveyid=currsurveyId.value;
   var year=curryearId.value;
   var supplierid=currsupplierid.value;
   var url;
   var optinalparam="";
  if(surveyid!=0)
  {
  optinalparam="psurveyid="+surveyid;
  }

  if(year!=0)
  {
  optinalparam=optinalparam+",psurveyyear="+year;
  
  }
  if(supplierid!=0)
    {
  optinalparam=optinalparam+",pvendor_id="+supplierid;
  
  }
//alert(year);
   url = "http://10.10.131.34:7003/svmRest/charts/v1/svmsurveyfeedbackvo1?limit=100&finder=RowFinder;"
                +optinalparam

 

 var SurveyFeedBack=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
            SurveyFeedBack.push({vname:jsonItemAttrs.Feedback,vNumofresponse:jsonItemAttrs.Numofresponse,vselector:jsonItemAttrs.Selector});
                  });
});

  $.ajaxSetup( {
        async : true
    });
return SurveyFeedBack;
}
function surveyfeedbackarray()
           {
           
              var series=[];
            var values = loadsurveyfeedBack();
   
    for (var i = 0;i < values.length;i++) {
   
     series.push({name: values[i].vname, items: [{value:values[i].vNumofresponse,id:values[i].vselector}]});
     
                }
                                   return series ;
           
           }


requirejs.config({
// Path mappings for the logical module names
    paths:
    //injector:mainReleasePaths
     {
        'knockout': 'libs/knockout/knockout-3.4.0',
        'jquery': 'libs/jquery/jquery-3.1.1.min',
        'jqueryui-amd': 'libs/jquery/jqueryui-amd-1.12.0.min',
        'promise': 'libs/es6-promise/es6-promise.min',
        'ojs': 'libs/oj/v3.1.0/min',
        'ojL10n': 'libs/oj/v3.1.0/ojL10n',
        'ojtranslations': 'libs/oj/v3.1.0/resources',
        'signals': 'libs/js-signals/signals.min',
        'text': 'libs/require/text',
        'hammerjs': 'libs/hammer/hammer-2.0.8.min',
        'moment': 'libs/moment/moment.min',
        'ojdnd': 'libs/dnd-polyfill/dnd-polyfill-1.0.0.min',
        'customElements': 'libs/webcomponents/CustomElements'
    }
    //endinjector
    ,
    // Shim configurations for modules that do not expose AMD
    shim: {
        'jquery': {
            exports: ['jQuery', '$']
        },
        'maps': {
            deps: ['jquery', 'i18n'],
            exports: ['MVMapView']
        }
    },
    // This section configures the i18n plugin. It is merging the Oracle JET built-in translation
    // resources with a custom translation file.
    // Any resource file added, must be placed under a directory named "nls". You can use a path mapping or you can define
    // a path that is relative to the location of this main.js file.
    config: {
        ojL10n: {
            merge: {
                //'ojtranslations/nls/ojtranslations': 'resources/nls/menu'
            }
        }
    }
});
/**
 * A top-level require call executed by the Application.
 * Although 'ojcore' and 'knockout' would be loaded in any case (they are specified as dependencies
 * by the modules themselves), we are listing them explicitly to get the references to the 'oj' and 'ko'
 * objects in the callback
 */

  require(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojgauge', 'ojs/ojchart'],
          function(oj, ko, $)
          {




                function ChartPieModel() {
         var self = this;
         self.threeDValue = ko.observable('on');
           self.modeValue = ko.observable('single');
                var pieSeries = surveyfeedbackarray();
// [{name: "Satisfied", items: [{value:65,id:'s1'}]},
// {name: "UnSatisfied", items: [{value:35,id:'s2'}]}];
   this.pieSeriesValue = ko.observableArray(pieSeries);
        var converterFactory = oj.Validation.converterFactory('number');
        var decimalConverter = converterFactory.createConverter({minimumFractionDigits: 0,maximumFractionDigits: 1});
        self.numberConverter = ko.observable(decimalConverter);
        self.formatValue = ko.observable('decimal');
        self.formatOptions = [
            {id: 'decimal', label: 'decimal', value: 'decimal'}
            ];
   
   
   
    self.color1=ko.observable('#68c182');
          self.color2=ko.observable('#ed6647');
        self.pieSeriesValue = ko.computed(function() {
       pieSeries[0]['color']=self.color1();
       pieSeries[1]['color']=self.color2();
     
      return pieSeries;
        });

    }
         var piechart=new ChartPieModel();
    $(function(){

      ko.applyBindings(piechart,document.getElementById('piechart-container'));
    })  ;


          });