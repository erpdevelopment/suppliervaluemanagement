/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
/**
 * Example of Require.js boostrap javascript
 */
   function getBusinessUnit()
  {
  
var bg= document.getElementById('businessunit').value;
 return ",pBusinessUnit="+bg;
  
  
  }
 function getCurrentQuarter()
 {
var month=new Date().getMonth()+1;
var quarter 
//if (month==0)
//month=12;
console.log(month);
if(month==1||month==2||month==3)
quarter=1;
else if(month==4||month==5||month==6)
quarter=2;
else if(month==7||month==8||month==9)
quarter=3;
else if(month==10||month==11||month==12)
quarter=4;
return quarter;
//new Date().getMonth();
//quarter;

 
 
 }
function loaddataproviderWS()
{
  var currSupplierId = document.getElementById('SupplierId');
   var currevalId = document.getElementById('CurrevalId');
    var curryearId = document.getElementById('CurrYearId');
     var currquarterId = document.getElementById('CurrQuarterId');
   // console.log('test = '+currPersonId.value);
  // alert(currSupplierId.value);
  var evalid=currevalId.value;
  var quarterid=currquarterId.value;
  var year=curryearId.value;
  var url;
  var optinalparam="";
  if(evalid!=0)
  {
  optinalparam=",pevalid="+evalid;
  }
  if(quarterid!=0)
  {
    optinalparam=optinalparam+",pquarter="+quarterid;
  }
  if(year!=0)
  {
  optinalparam=optinalparam+",pevalyear="+year;
  
  }
  if(evalid==0 && quarterid==0 && year==0)
  {
    url = 
    //"http://10.10.131.34:7003/svmRest/charts/v1/svmevaluationresponsepersuppliervo1?limit=100&finder=RowFinder;pvendorid=1214857,pBusinessUnit=MBC - Dubai"
  //",pBusinessUnit=MBC - Dubai"
   "http://10.10.131.34:7003/svmRest/charts/v1/svmevaluationresponsepersuppliervo1?limit=100&finder=RowFinder;pvendorid="
               +currSupplierId.value+getBusinessUnit();
               
            console.log(url);   
  }
  else
  {
   url = "http://10.10.131.34:7003/svmRest/charts/v1/svmevaluationresponsepersuppliervo1?limit=100&finder=RowFinder;pvendorid="
                +currSupplierId.value+optinalparam+getBusinessUnit();
                console.log(url);
  }
  
   var dp=[];
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
      
             dp.push({
        "Indicator": "Cost",
        "Score": jsonItemAttrs.Cost
//jsonItemAttrs.COST
    }, {
        "Indicator": "Delivery ",
        "Score": jsonItemAttrs.Delivery
    }, {
        "Indicator": "Quality",
        "Score": jsonItemAttrs.Quality

    }, {
        "Indicator": "Service",
        "Score": jsonItemAttrs.Service

    }) ;
                console.log ("Delivery:"+jsonItemAttrs.Cost); 
            
                  });
});

  $.ajaxSetup( {
        async : true
    });
    
     
return dp;
  
// alert(url);
}

function loaddataprovider()
           {
           var dp=[];
           dp=loaddataproviderWS();
           return dp;
           }
           
           
  function loadresponseanalysispersupplier()
{
  var currSupplierId = document.getElementById('SupplierId');
   var currevalId = document.getElementById('CurrevalId');
    var curryearId = document.getElementById('CurrYearId');
     var currquarterId = document.getElementById('CurrQuarterId');
   
  var evalid=currevalId.value;
  var quarterid=currquarterId.value;
  var year=curryearId.value;
  var url;
 
  var optinalparam="";
  if(evalid!=0)
  {
  optinalparam=",pevalid="+evalid;
  }
  if(quarterid!=0)
  {
    optinalparam=optinalparam+",pquarter="+quarterid;
  }
  if(year!=0)
  {
  optinalparam=optinalparam+",pevalyear="+year;
  
  }
 if(evalid==0 && quarterid==0 && year==0)
  {
    url = "http://10.10.131.34:7003/svmRest/charts/v1/svmresposnseanalysispersuppliervo1?limit=100&finder=RowFinder;pvendorid="
                +currSupplierId.value+getBusinessUnit();
               
  }
  else
  {
   url = "http://10.10.131.34:7003/svmRest/charts/v1/svmresposnseanalysispersuppliervo1?limit=100&finder=RowFinder;pvendorid="
                +currSupplierId.value+optinalparam+getBusinessUnit();
  }
//alert(url);
  var series=[];
  
  
 
 var jsonitems;
  $.ajaxSetup( {
        async : false
    });

$.getJSON(url,function(jsonResult){

var counter = 0;
        $.each(jsonResult, function () {
            if (counter == 0)
                jsonItems = this;
            counter++;
        });
 $.each(jsonItems, function (index, jsonItemAttrs) {
            //console.log('index=' + index + ' ,jsonItemAttrs.DayStatus=' + jsonItemAttrs.DayStatus);
           // alert();
            var jan =jsonItemAttrs.Janspend;
           var feb =jsonItemAttrs.Febspend;
            var march=jsonItemAttrs.Marspend; 
            var april=jsonItemAttrs.Aprspend;
            var may=jsonItemAttrs.Myspend;
            var june=jsonItemAttrs.Junspend;
            var july=jsonItemAttrs.Julspend;
            var august=jsonItemAttrs.Augspend;
            var sept=jsonItemAttrs.Sepspend;
            var oct=jsonItemAttrs.Octspend;
            var nov=jsonItemAttrs.Novspend;
            var dec=jsonItemAttrs.Decspend;
          var selectedyearId = document.getElementById('CurrYearId');
           var yearid=selectedyearId.value;

           if( jsonItemAttrs.Janspend==null)
           {
           jan=0;
           }
           if(jsonItemAttrs.Febspend==null)
           {
           feb=0;
           }
              if( jsonItemAttrs.Marspend==null)
           {
           march=0;
           }
           if(jsonItemAttrs.Aprspend==null)
           {
           april=0;
           }
              if( jsonItemAttrs.Myspend==null)
           {
           may=0;
           }
           if(jsonItemAttrs.Junspend==null)
           {
           june=0;
           }
              if(jsonItemAttrs.Julspend==null)
           {
           july=0;
           }
           if(jsonItemAttrs.Augspend==null)
           {
           august=0;
           }
              if(jsonItemAttrs.Sepspend==null)
           {
           sept=0;
           }
           if(jsonItemAttrs.Octspend==null)
           {
           oct=0;
           }
              if( jsonItemAttrs.Novspend==null)
           {
           nov=0;
           }
           if(jsonItemAttrs.Decspend==null)
           {
           dec=0;
           }
           // alert('test test test');
           if(quarterid==0 )
            {
           if(yearid==0 ||yearid==new Date().getFullYear())
            {
                   var currentquarter=getCurrentQuarter();
                    if(currentquarter==1)
                   series =  [{name: "Spend", items: [jan,feb,march],color:"#c14141"},
                   {name: "Score", items: [jsonItemAttrs.Januaryscore,jsonItemAttrs.Feburaryscore,jsonItemAttrs.Marchscore],color:"#267cb1" ,assignedToY2: "on"}];
                    else   if(currentquarter==2)
                   series =  [{name: "Spend", items: [jan,feb,march,april,may,june],color:"#c14141"},
                  {name: "Score", items: [jsonItemAttrs.Januaryscore,jsonItemAttrs.Feburaryscore,jsonItemAttrs.Marchscore, jsonItemAttrs.Aprilscore,jsonItemAttrs.Mayscore,jsonItemAttrs.Junescore],color:"#267cb1" ,assignedToY2: "on"}];
                       if(currentquarter==3)
                     series =  [{name: "Spend", items: [jan,feb,march,april,may,june,july,august,sept],color:"#c14141"},
                   {name: "Score", items: [jsonItemAttrs.Januaryscore,jsonItemAttrs.Feburaryscore,jsonItemAttrs.Marchscore, jsonItemAttrs.Aprilscore,jsonItemAttrs.Mayscore,jsonItemAttrs.Junescore,jsonItemAttrs.Julyscore,jsonItemAttrs.Augustscore,jsonItemAttrs.Septscore],color:"#267cb1" ,assignedToY2: "on"}];
                       if(currentquarter==4)
                  series =  [{name: "Spend", items: [jan,feb,march,april,may,june,july,august,sept, oct,nov,dec],color:"#c14141"},
                 {name: "Score", items: [jsonItemAttrs.Januaryscore,jsonItemAttrs.Feburaryscore,jsonItemAttrs.Marchscore, jsonItemAttrs.Aprilscore,jsonItemAttrs.Mayscore,jsonItemAttrs.Junescore,jsonItemAttrs.Julyscore,jsonItemAttrs.Augustscore,jsonItemAttrs.Septscore,jsonItemAttrs.Octoberscore, jsonItemAttrs.Novscore,jsonItemAttrs.Decemberscore],color:"#267cb1" ,assignedToY2: "on"}];
                 }

            else
            {
            series =  [{name: "Spend", items: [jan,feb,march,april,may,june,july,august,sept, oct,nov,dec],color:"#c14141"},
          {name: "Score", items: [jsonItemAttrs.Januaryscore,jsonItemAttrs.Feburaryscore,jsonItemAttrs.Marchscore, jsonItemAttrs.Aprilscore,jsonItemAttrs.Mayscore,jsonItemAttrs.Junescore,jsonItemAttrs.Julyscore,jsonItemAttrs.Augustscore,jsonItemAttrs.Septscore,jsonItemAttrs.Octoberscore, jsonItemAttrs.Novscore,jsonItemAttrs.Decemberscore],color:"#267cb1" ,assignedToY2: "on"}];
            }
          
            }
            if(quarterid==4)
            {
         series =  [{name: "Spend", items: [oct,nov,dec],color:"#c14141"},
          {name: "Score", items: [jsonItemAttrs.Octoberscore, jsonItemAttrs.Novscore,jsonItemAttrs.Decemberscore],color:"#267cb1" ,assignedToY2: "on"}];
         }
            else if(quarterid==3)
            {
                 series =  [{name: "Spend", items: [july,august,sept],color:"#c14141"},
          {name: "Score", items: [jsonItemAttrs.Julyscore,jsonItemAttrs.Augustscore,jsonItemAttrs.Septscore],color:"#267cb1" ,assignedToY2: "on"}];
            
            }
             else if(quarterid==2)
            {
                  series =  [{name: "Spend", items: [april,may,june],color:"#c14141"},
          {name: "Score", items: [jsonItemAttrs.Aprilscore,jsonItemAttrs.Mayscore,jsonItemAttrs.Junescore],color:"#267cb1" ,assignedToY2: "on"}];
            
            }
               else if(quarterid==1)
            {
                     series =  [{name: "Spend", items: [jan,feb,march],color:"#c14141"},
          {name: "Score", items:[jsonItemAttrs.Januaryscore,jsonItemAttrs.Feburaryscore,jsonItemAttrs.Marchscore],color:"#267cb1" ,assignedToY2: "on"}];
            
            }

            
                  });
});

  $.ajaxSetup( {
        async : true
    });
return series;
  
// alert(url);
}         
           




 


var chart = AmCharts.makeChart("chartdivbar3", {
    "theme": "light",
    "type": "serial",
    "dataProvider": loaddataprovider(),
    "startDuration": 1,
    "graphs": [{
        "balloonText": "[[category]](Score):[[value]]%",
        "fillAlphas": 0.9,
        "lineAlpha": 0.2,
        "title": "Score",
        "type": "column",
        "columnwidth":"50px" ,
        "valueField": "Score",
        "columnWidth"  :0.2

    } ],
    "plotAreaFillAlphas": 0.3,
    "depth3D": 10  ,
    "angle": 10,
    "categoryField": "Indicator",
    "categoryAxis": {
        "gridPosition": "start"
    },
    "export": {
    	"enabled": true
     }
});

















requirejs.config({
// Path mappings for the logical module names
    paths:
    //injector:mainReleasePaths
     {
        'knockout': 'libs/knockout/knockout-3.4.0',
        'jquery': 'libs/jquery/jquery-3.1.1.min',
        'jqueryui-amd': 'libs/jquery/jqueryui-amd-1.12.0.min',
        'promise': 'libs/es6-promise/es6-promise.min',
        'ojs': 'libs/oj/v3.1.0/min',
        'ojL10n': 'libs/oj/v3.1.0/ojL10n',
        'ojtranslations': 'libs/oj/v3.1.0/resources',
        'signals': 'libs/js-signals/signals.min',
        'text': 'libs/require/text',
        'hammerjs': 'libs/hammer/hammer-2.0.8.min',
        'moment': 'libs/moment/moment.min',
        'ojdnd': 'libs/dnd-polyfill/dnd-polyfill-1.0.0.min',
        'customElements': 'libs/webcomponents/CustomElements'
    }
    //endinjector
    ,
    // Shim configurations for modules that do not expose AMD
    shim: {
        'jquery': {
            exports: ['jQuery', '$']
        },
        'maps': {
            deps: ['jquery', 'i18n'],
            exports: ['MVMapView']
        }
    },
 
    config: {
        ojL10n: {
            merge: {
                //'ojtranslations/nls/ojtranslations': 'resources/nls/menu'
            }
        }
    }
});

  require(['ojs/ojcore', 'knockout', 'jquery', 'ojs/ojknockout', 'ojs/ojgauge', 'ojs/ojchart'],
          function(oj, ko, $)
          {



                  function scoreChartModel() {
        var self = this;
// alert('test');
        /* toggle button variables */
        self.orientationValue = ko.observable('vertical');
        self.dualY = ko.observable('off');

        self.splitterValue = ko.observable(0.9);

        /* chart data */
        var dualYSeries =loadresponseanalysispersupplier();
        var dualYGroups = [];
            var currquarterId = document.getElementById('CurrQuarterId');
             var selectedyearId = document.getElementById('CurrYearId');
            var quarterid=currquarterId.value;
           var yearid=selectedyearId.value;
            if(quarterid==0)
            {
            
   //  alert(quarterid);
              if(yearid==0 ||yearid== new Date().getFullYear())
            {
                   var currentquarter=getCurrentQuarter();
                   
                    if(currentquarter==1)
                     dualYGroups= ["Jan", "Feb", "March"];
                    else   if(currentquarter==2)
                     dualYGroups= ["Jan", "Feb", "March", "April","May","June"];
                       if(currentquarter==3)
                     dualYGroups= ["Jan", "Feb", "March", "April","May","June","July","Aug","Sep"];
                       if(currentquarter==4)
                     dualYGroups= ["Jan", "Feb", "March", "April","May","June","July","Aug","Sep","Oct","Nov","Dec"];
                 }
                 else
              {
                dualYGroups= ["Jan", "Feb", "March", "April","May","June","July","Aug","Sep","Oct","Nov","Dec"];
              }
            }
        if( quarterid==4)
        {
       dualYGroups= ["Oct","Nov","Dec"];
        }
        else   if(quarterid==3)
        {
       dualYGroups= ["July","Aug","Sep"];
        }
         else   if(quarterid==2)
        {
       dualYGroups= [ "April","May","June"];
        }
        else     if(quarterid==1)
        {
       dualYGroups= ["Jan", "Feb", "March"];
        }
       // dualYSeries= ["Jan", "Feb", "March", "April","May","June","July","Aug","Sep","Oct","Nov","Dec"];
        self.lineSeriesValue = ko.observableArray(dualYSeries);
        self.lineGroupsValue = ko.observableArray(dualYGroups);


    }
            var scorechartModel = new scoreChartModel();
            $(
      function(){

        ko.applyBindings(scoreChartModel, document.getElementById('score-chartcontainer'));
      }
    );

          });
          
          
          
          
          
          
          