/**
 * Copyright (c) 2014, 2017, Oracle and/or its affiliates.
 * The Universal Permissive License (UPL), Version 1.0
 */
define(['knockout'],
        function (ko)
        {
            // cookie utility classes
            function createCookie(name, value, days) {
             
            }

            function readCookie(name) {
              
            }

            function eraseCookie(name) {
                
            }


          

            function readSettings() {

             
            }

            function getSettingsFromCookie() {
                
            }

            function QueryString() {
               
            };

            return {createCookie: createCookie, readCookie: readCookie, eraseCookie: eraseCookie, readSettings: readSettings, QueryString: QueryString};

        });
