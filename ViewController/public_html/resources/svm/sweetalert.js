          function customalertmessage(vtitle,vtext,vtype,vshowcanlebutton,vconfirmbuttonclass){
   
   
  var showcancel =false ;
  if(vshowcanlebutton=='Y')
  showcancel=true;
        swal({
          title: vtitle,
          text:vtext,
          type: vtype,
          showCancelButton:showcancel ,
          confirmButtonClass:vconfirmbuttonclass
           
          
        });
      
      }
      
                function successlertmessage(){
                swal({
          title: 'Information',
          text:'Data has been Saved Successfully',
          type: 'success',
          showCancelButton:false ,
          confirmButtonClass:'btn-success'
           
          
        });

   
      }
                function customsuccesslertmessage(vtitle,vtext){
              swal({
        title: vtitle,
          text:vtext,
          type: 'success',
          showCancelButton:false ,
          confirmButtonClass:'btn-success'
           
          
        });
      
      }
      
             function warninglertmessage(){
              swal({
          title: 'Warning',
          text:'Something went wrong ,check please',
          type: 'warning',
          showCancelButton:false ,
          confirmButtonClass:'btn-warning'
           
          
        });
      
      }
          function customwarninglertmessage(vtitle,vtext){
              swal({
        title: vtitle,
          text:vtext,
          type: 'warning',
          showCancelButton:false ,
          confirmButtonClass:'btn-warning'
           
          
        });
      
      }
      
              function dangeralertmessage(){
              swal({
          title: 'Error',
          text:'Something went wrong ,check please',
          type: 'error',
          showCancelButton:false ,
          confirmButtonClass:'btn-danger'
           
          
        });
      
      }
      
             function customdangeralertmessage(vtitle,vtext){
              swal({
         title: vtitle,
          text:vtext,
          type: 'error',
          showCancelButton:false ,
          confirmButtonClass:'btn-danger'
           
          
        });
      
      }
      
      
      
            function deletemessage(){
                  var Istemplateadd = document.getElementById('IstemplateAdd');
      // var adftext=AdfPage.PAGE.findComponentByAbsoluteId('pt1:IstemplateAddtext')
            
        swal({
  title: "Add New Template",
  text: "Current Template will be Deleted to add new Template?",
  type: "warning",
  showCancelButton: true,
  confirmButtonClass: "btn-danger",
  confirmButtonText: "Yes, Add!",
  cancelButtonText: "No, cancel!",
  closeOnConfirm: true,
  closeOnCancel: true
},
function(isConfirm) {
  if (isConfirm) {

Istemplateadd.value=1;
document.getElementById('IstemplateAdd').value=1;
//AdfPage.PAGE.findComponentByAbsoluteId('pt1:IstemplateAddtext').value=1;
//adftext.value=1;
console.log('test = '+Istemplateadd.value);
// console.log('testadf = '+adftext.value);
  } else {
  
 Istemplateadd.value=0;
console.log('test = '+Istemplateadd.value);

  }
});

      
      }
  
   function customalertmessageHTML(vtitle,vtext,vtype,vshowcanlebutton,vconfirmbuttonclass){
   

  var showcancel=false  ;
  if(vshowcanlebutton=='Y')
  showcancel=true;
 
        swal({
          title: vtitle,
          text:vtext,
          html:vtext,
          type:vtype,
          showCancelButton:showcancel ,
          confirmButtonClass:vconfirmbuttonclass
           
          
        });
 
      }    
   