

CREATE  PACKAGE BODY   APPS.SVM_PKG AS
/******************************************************************************
   NAME:       SVM_PKG
   PURPOSE:

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        20/May/2018      hemohamed       1. Created this package.
******************************************************************************/

FUNCTION GetEvaluationStatus(EvalId Number )return varchar2
is
v_startdate Date;
v_enddate Date ;
v_status varchar2(200);
v_count Number ;
v_completed varchar2 (500);
begin 


select EVALUATION_START_DATE ,EVALUATION_END_DATE into v_startdate ,v_enddate   from svm_evaluation
where evaluation_id=EvalId ;
select count(eval_employee_id) into v_completed from svm_eval_evaluators  where fk_eval_id=EvalId and Response_completed in ('N' , 'X');
if(sysdate<v_startdate)
then 
return 'Scheduled';
elsif (v_enddate<sysdate)
then
return 'Closed';
elsif (v_completed=0)
then 
return 
'Completed';
else
return 'InProcess';

end if  ;


end ;


FUNCTION COUNT_EVAL_INPROCESS_YEAR RETURN NUMBER
IS
v_count Number:=0 ;
count_inporcess Number:=0 ;
v_status varchar2(500);
cursor evals is 
select * from svm_evaluation
where EVALUATION_END_DATE>sysdate  and sysdate>EVALUATION_START_DATE   and (to_char(EVALUATION_START_DATE,'yyyy')=to_char(sysdate,'yyyy') or to_char(EVALUATION_END_DATE,'yyyy')=to_char(sysdate,'yyyy'));
BEGIN

for e in evals 

loop
select count (fk_eval_id) into v_count from svm_evaluation_response where fk_eval_id=e.evaluation_id;
if v_count=0
then 
count_inporcess:=count_inporcess+1;
end if ;
if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status from svm_evaluation_response  response ,SVM_EVAL_RESPONSE_VALUES val
where fk_eval_id=e.evaluation_id
and response.response_id=val.fk_response_id
and response_rank=0;
if v_status='InProcess'
then 
count_inporcess:=count_inporcess+1;

end if ;
end if ;
end loop;
return count_inporcess ;
END;


FUNCTION COUNT_EVAL_INPROCESS_DASHBOARD(EVALID NUMBER ,EVALQUARTER NUMBER ,EVALYEAR NUMBER,SUPPLIERID NUMBER) RETURN NUMBER
IS
v_count Number:=0 ;
count_inporcess Number:=0 ;
v_status varchar2(500);
cursor evals is 
 select distinct evaluation_id from svm_evaluation ,svm_eval_suppliers sp
 where evaluation_id=sp.fk_eval_id(+) 
and  EVALUATION_END_DATE>sysdate  and sysdate>EVALUATION_START_DATE
and  (evaluation_id=EVALID or EVALID is null)    
   
and(TO_CHAR(evaluation_end_date, 'Q')=EVALQUARTER or EVALQUARTER is null)    
and(to_char(evaluation_end_date,'yyyy')=EVALYEAR or EVALYEAR is null)
and ( sp.fk_supplier_id=SUPPLIERID or SUPPLIERID is null)
;
BEGIN

for e in evals 

loop
select count (fk_eval_id) into v_count from svm_evaluation_response where fk_eval_id=e.evaluation_id;
if v_count=0
then 
count_inporcess:=count_inporcess+1;
end if ;
if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status from svm_evaluation_response  response ,SVM_EVAL_RESPONSE_VALUES val,
svm_evaluation eval 
where fk_eval_id=e.evaluation_id
and response.response_id=val.fk_response_id
and eval.evaluation_id=e.evaluation_id
and  (evaluation_id=EVALID or EVALID is null)    
   
and(TO_CHAR(evaluation_end_date, 'Q')=EVALQUARTER or EVALQUARTER is null)    
and(to_char(evaluation_end_date,'yyyy')=EVALYEAR or EVALYEAR is null)
and ( response.fk_supplier_id=SUPPLIERID or SUPPLIERID is null)
and response_rank=0;
if v_status='InProcess'
then 
count_inporcess:=count_inporcess+1;

end if ;
end if ;
end loop;
return count_inporcess ;
END;
FUNCTION COUNT_EVAL_COMPLETED_DASHBOARD(EVALID NUMBER ,EVALQUARTER NUMBER ,EVALYEAR NUMBER,SUPPLIERID NUMBER) RETURN NUMBER
IS

v_count Number:=0 ;
count_completed Number:=0 ;
v_status varchar2(500);
cursor evals is 
select distinct evaluation_id from svm_evaluation ,svm_eval_suppliers sp
 where evaluation_id=sp.fk_eval_id(+) 
and  EVALUATION_END_DATE>sysdate  and sysdate>EVALUATION_START_DATE
and  (evaluation_id=EVALID or EVALID is null)    
   
and(TO_CHAR(evaluation_end_date, 'Q')=EVALQUARTER or EVALQUARTER is null)    
and(to_char(evaluation_end_date,'yyyy')=EVALYEAR or EVALYEAR is null)
and ( sp.fk_supplier_id=SUPPLIERID or SUPPLIERID is null)

;
BEGIN

for e in evals 

loop
select count (fk_eval_id) into v_count from svm_evaluation_response where fk_eval_id=e.evaluation_id;

if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status from svm_evaluation_response  response ,SVM_EVAL_RESPONSE_VALUES val ,
svm_evaluation eval 
where fk_eval_id=e.evaluation_id
and response.response_id=val.fk_response_id
and eval.evaluation_id=e.evaluation_id
and  (evaluation_id=EVALID or EVALID is null)    
   
and(TO_CHAR(evaluation_end_date, 'Q')=EVALQUARTER or EVALQUARTER is null)    
and(to_char(evaluation_end_date,'yyyy')=EVALYEAR or EVALYEAR is null)
and ( response.fk_supplier_id=SUPPLIERID or SUPPLIERID is null)

and response_rank=0;
if v_status='Completed'
then 
count_completed:=count_completed+1;

end if ;
end if ;
end loop;
return count_completed ;
END ;
FUNCTION COUNT_EVAL_COMPLETED_YEAR RETURN NUMBER
IS

v_count Number:=0 ;
count_completed Number:=0 ;
v_status varchar2(500);
cursor evals is 
select * from svm_evaluation
where EVALUATION_END_DATE>sysdate  and sysdate>EVALUATION_START_DATE   and (to_char(EVALUATION_START_DATE,'yyyy')=to_char(sysdate,'yyyy') or to_char(EVALUATION_END_DATE,'yyyy')=to_char(sysdate,'yyyy'));
BEGIN

for e in evals 

loop
select count (fk_eval_id) into v_count from svm_evaluation_response where fk_eval_id=e.evaluation_id;

if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status from svm_evaluation_response  response ,SVM_EVAL_RESPONSE_VALUES val
where fk_eval_id=e.evaluation_id
and response.response_id=val.fk_response_id
and response_rank=0;
if v_status='Completed'
then 
count_completed:=count_completed+1;

end if ;
end if ;
end loop;
return count_completed ;
END ;

PROCEDURE UPDATE_RANK(RESPONSEVALUEID NUMBER,RANKVALUE NUMBER)
is 

begin 
 update SVM_EVAL_RESPONSE_VALUES
 set  RESPONSE_RANK=RANKVALUE 
 where RESPONSE_VALUE_ID=RESPONSEVALUEID;
 
 commit;
 

end ;
PROCEDURE DELETE_EVALUATION_TEMPLATE (EVALID NUMBER)
IS
V_EVAL_ID NUMBER;
BEGIN
SELECT COUNT(FK_EVAL_ID) INTO V_EVAL_ID FROM SVM_EVALUATION_TEMPLATE WHERE FK_EVAL_ID=EVALID;
IF V_EVAL_ID !=0
THEN
DELETE FROM SVM_EVALUATION_TEMPLATE WHERE FK_EVAL_ID=EVALID;
COMMIT;
END IF ;
END;
PROCEDURE DELETE_EVALUATION(EVALID NUMBER)
IS
BEGIN
DELETE FROM SVM_EVAL_SUPPLIERS WHERE FK_EVAL_ID=EVALID;
DELETE FROM SVM_EVAL_EVALUATORS WHERE FK_EVAL_ID=EVALID;
DELETE FROM SVM_EVALUATION_TEMPLATE WHERE FK_EVAL_ID=EVALID;
DELETE FROM SVM_EVALUATION WHERE EVALUATION_ID=EVALID;
COMMIT;
END ;
FUNCTION GET_EVALUATION_STARTED (EVALID NUMBER)RETURN VARCHAR2
IS
V_STATUS VARCHAR2(100);
BEGIN 
SELECT IS_STARTED INTO V_STATUS FROM SVM_EVALUATION WHERE EVALUATION_ID=EVALID;
 RETURN V_STATUS;
END ;



FUNCTION GETSURVEYSTATUS(SURVEYID Number )return varchar2
is
v_startdate Date;
v_enddate Date ;
v_status varchar2(200);
v_count Number;
v_completed varchar2(500);
begin 

select count(FK_SUPPLIER_ID) into v_completed from svm_survey_suppliers  where FK_SURVEY_ID=SURVEYID and Response_completed='N';
select survey_START_DATE ,survey_END_DATE into v_startdate ,v_enddate   from svm_survey
where survey_id=SURVEYID ;
if(sysdate<v_startdate)
then 
return 'Scheduled';
elsif (v_enddate<sysdate)
then
return 'Closed';
elsif (v_completed=0)
then 
return  'Completed';

else

return 'InProcess';



end if ;

end ;

 
 PROCEDURE UPDATE_RANK_SURVEY(RESPONSEVALUEID NUMBER,RANKVALUE NUMBER)
 is 

begin 
 update SVM_SURVEY_RESPONSE_VALUE
 set  RESPONSE_RANK=RANKVALUE 
 where RESPONSE_VALUE_ID=RESPONSEVALUEID;
 
 commit;
 

end ;
PROCEDURE DELETE_SURVEY_TEMPLATE (SURVEYID NUMBER)
IS
V_SURVEY_ID NUMBER;
BEGIN
SELECT COUNT(FK_SURVEY_ID) INTO V_SURVEY_ID FROM SVM_SURVEY_TEMPLATE WHERE FK_SURVEY_ID=SURVEYID;
IF V_SURVEY_ID !=0
THEN
DELETE FROM SVM_SURVEY_TEMPLATE WHERE FK_SURVEY_ID=SURVEYID;
COMMIT;
END IF ;
END;
PROCEDURE DELETE_SURVEY(SURVEYID NUMBER)
IS
BEGIN
DELETE FROM SVM_SURVEY_SUPPLIERS WHERE FK_SURVEY_ID=SURVEYID;
DELETE FROM SVM_SURVEY_TEMPLATE WHERE FK_SURVEY_ID=SURVEYID;
DELETE FROM SVM_SURVEY WHERE SURVEY_ID=SURVEYID;
COMMIT;
END;
FUNCTION GET_SURVEY_STARTED (SURVEYID NUMBER)RETURN VARCHAR2
IS
V_STATUS VARCHAR2(100);
BEGIN 
SELECT IS_STARTED INTO V_STATUS FROM SVM_SURVEY WHERE SURVEY_ID=SURVEYID;
 RETURN V_STATUS;
END ;
FUNCTION COUNT_SURVEY_INPROCESS_YEAR RETURN NUMBER
IS
v_count Number:=0 ;
count_inporcess Number:=0 ;
v_status varchar2(500);
cursor survey is 
select * from svm_survey
where SURVEY_END_DATE>sysdate  and sysdate>SURVEY_START_DATE   and (to_char(SURVEY_START_DATE,'yyyy')=to_char(sysdate,'yyyy') or to_char(SURVEY_END_DATE,'yyyy')=to_char(sysdate,'yyyy'));
BEGIN

for e in survey 

loop
select count (fk_survey_id) into v_count from svm_survey_response where fk_survey_id=e.survey_id;
if v_count=0
then 
count_inporcess:=count_inporcess+1;
end if ;
if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status from svm_survey_response  response ,SVM_survey_RESPONSE_VALUE val
where fk_survey_id=e.survey_id
and response.response_id=val.fk_response_id
and response_rank=0;
if v_status='InProcess'
then 
count_inporcess:=count_inporcess+1;

end if ;
end if ;
end loop;
return count_inporcess ;
END;

FUNCTION COUNT_SURVEY_COMPLETED_YEAR RETURN NUMBER
IS

v_count Number:=0 ;
count_completed Number:=0 ;
v_status varchar2(500);
cursor survey is 
select * from svm_survey
where SURVEY_END_DATE>sysdate  and sysdate>SURVEY_START_DATE   and (to_char(SURVEY_START_DATE,'yyyy')=to_char(sysdate,'yyyy') or to_char(SURVEY_END_DATE,'yyyy')=to_char(sysdate,'yyyy'));
BEGIN

for e in survey 

loop
select count (fk_survey_id) into v_count from svm_survey_response where fk_survey_id=e.survey_id;
if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status from svm_survey_response  response ,SVM_survey_RESPONSE_VALUE val
where fk_survey_id=e.survey_id
and response.response_id=val.fk_response_id
and response_rank=0;
if v_status='Completed'
then 
count_completed:=count_completed+1;

end if ;
end if ;
end loop;
return count_completed ;
END ;
FUNCTION COUNT_SURVY_INPRCESS_DASHBOARD(SURVEYID NUMBER ,SURVEYYEAR NUMBER) RETURN NUMBER
IS
v_count Number:=0 ;
count_inporcess Number:=0 ;
v_status varchar2(500);
cursor survey is 
select * from svm_survey
where SURVEY_END_DATE>sysdate  and sysdate>SURVEY_START_DATE
and  (survey_id=SURVEYID or SURVEYID is null)    
      and(to_char(survey_end_date,'yyyy')=SURVEYYEAR or SURVEYYEAR is null)

;
BEGIN

for e in survey

loop
select count (fk_survey_id) into v_count from svm_survey_response where fk_survey_id=e.survey_id;
if v_count=0
then 
count_inporcess:=count_inporcess+1;
end if ;
if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status from svm_survey_response  response ,SVM_survey_RESPONSE_VALUE val,
svm_survey sv 
where fk_survey_id=e.survey_id
and response.response_id=val.fk_response_id
and sv.survey_id=e.survey_id
and  (survey_id=SURVEYID or SURVEYID is null)    
     
and(to_char(survey_end_date,'yyyy')=SURVEYYEAR or SURVEYYEAR is null)

and response_rank=0;
if v_status='InProcess'
then 
count_inporcess:=count_inporcess+1;

end if ;
end if ;
end loop;
return count_inporcess ;
END ;
FUNCTION COUNT_SURVY_COMPLTED_DASHBOARD(SURVEYID NUMBER ,SURVEYYEAR NUMBER) RETURN NUMBER
IS

v_count Number:=0 ;
count_completed Number:=0 ;
v_status varchar2(500);
cursor survey is 
select * from svm_survey
where SURVEY_END_DATE>sysdate  and sysdate>SURVEY_START_DATE
and  (survey_id=SURVEYID or SURVEYID is null)    
      and(to_char(survey_end_date,'yyyy')=SURVEYYEAR or SURVEYYEAR is null)

;
BEGIN

for e in survey

loop
select count (fk_survey_id) into v_count from svm_survey_response where fk_survey_id=e.survey_id;

if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status  from svm_survey_response  response ,SVM_survey_RESPONSE_VALUE val,
svm_survey sv 
where fk_survey_id=e.survey_id
and response.response_id=val.fk_response_id
and sv.survey_id=e.survey_id
and  (survey_id=SURVEYID or SURVEYID is null)    
     
and(to_char(survey_end_date,'yyyy')=SURVEYYEAR or SURVEYYEAR is null)

and response_rank=0;
if v_status='Completed'
then 
count_completed:=count_completed+1;

end if ;
end if ;
end loop;
return count_completed ;
END ;
FUNCTION COUNT_SURVY_INPRCESS_SUPPLIER(SURVEYID NUMBER ,SURVEYYEAR NUMBER,SUPPLIERID NUMBER )RETURN NUMBER
IS
v_count Number:=0 ;
count_inporcess Number:=0 ;
v_status varchar2(500);
cursor survey is 
select distinct survey_id  from svm_survey s ,svm_survey_suppliers sp
where s.survey_id=sp.fk_survey_id(+) 
and  SURVEY_END_DATE>sysdate  and sysdate>SURVEY_START_DATE
and  (survey_id=SURVEYID or SURVEYID is null)    
      and(to_char(survey_end_date,'yyyy')=SURVEYYEAR or SURVEYYEAR is null)
       and (sp.fk_supplier_id=SUPPLIERID or SUPPLIERID is null) 

;
BEGIN

for e in survey

loop
select count (fk_survey_id) into v_count from svm_survey_response where fk_survey_id=e.survey_id;
if v_count=0
then 
count_inporcess:=count_inporcess+1;
end if ;
if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status from svm_survey_response  response ,SVM_survey_RESPONSE_VALUE val,
svm_survey sv 
where fk_survey_id=e.survey_id
and response.response_id=val.fk_response_id
and sv.survey_id=e.survey_id
and  (survey_id=SURVEYID or SURVEYID is null)    
     
and(to_char(survey_end_date,'yyyy')=SURVEYYEAR or SURVEYYEAR is null)
  and (response.fk_supplier_id=SUPPLIERID or SUPPLIERID is null) 
and response_rank=0;
if v_status='InProcess'
then 
count_inporcess:=count_inporcess+1;

end if ;
end if ;
end loop;
return count_inporcess ;
END ;
FUNCTION COUNT_SURVY_COMPLTED_SUPPLIER(SURVEYID NUMBER ,SURVEYYEAR NUMBER,SUPPLIERID NUMBER) RETURN NUMBER
IS

v_count Number:=0 ;
count_completed Number:=0 ;
v_status varchar2(500);
cursor survey is 
select distinct survey_id from svm_survey s , svm_survey_suppliers sp
where s.survey_id=sp.fk_survey_id(+) 
and  SURVEY_END_DATE>sysdate  and sysdate>SURVEY_START_DATE
and  (survey_id=SURVEYID or SURVEYID is null)    
 and(to_char(survey_end_date,'yyyy')=SURVEYYEAR or SURVEYYEAR is null)
      and (sp.fk_supplier_id=SUPPLIERID or SUPPLIERID is null) 

;
BEGIN

for e in survey

loop
select count (fk_survey_id) into v_count from svm_survey_response where fk_survey_id=e.survey_id;

if v_count!=0
then 
select  decode (count (response_rank),0,'Completed','InProcess') into v_status  from svm_survey_response  response ,SVM_survey_RESPONSE_VALUE val,
svm_survey sv 
where fk_survey_id=e.survey_id
and response.response_id=val.fk_response_id
and sv.survey_id=e.survey_id
and  (survey_id=SURVEYID or SURVEYID is null)    
and(to_char(survey_end_date,'yyyy')=SURVEYYEAR or SURVEYYEAR is null)
  and (response.fk_supplier_id=SUPPLIERID or SUPPLIERID is null) 
and response_rank=0;
if v_status='Completed'
then 
count_completed:=count_completed+1;

end if ;
end if ;
end loop;
return count_completed ;
END ;

FUNCTION COUNT_EVAL_PROGRESS_SUPPLIER(EVALID NUMBER ,USERID NUMBER,SUPPLIERID NUMBER) RETURN NUMBER
IS

v_count Number:=0 ;
v_divisor Number;
v_supplier_id Number :=0;
BEGIN

IF SUPPLIERID is null
then 
 SELECT  FK_SUPPLIER_ID into  v_supplier_id
  FROM (SELECT FK_SUPPLIER_ID,
               row_number() OVER (ORDER BY VENDOR_NAME) rn
          FROM 
          
  ( select distinct RESPONSE_ID,FK_SUPPLIER_ID,VENDOR_NAME
    from SVM_EVALUATION_RESPONSE ,(select distinct  s.vendor_id,s.vendor_name from ap_suppliers s ,ap_supplier_sites_all ss       
where s.vendor_id=ss.vendor_id       
and ss.org_id=161)suppliers ,svm_evaluation ev      
where suppliers.vendor_id=fk_supplier_id      
and ev.evaluation_id=fk_eval_id    
and  fk_eval_id=EVALID and fk_evaluator_user_id=USERID
order by VENDOR_NAME asc)
          )
 WHERE  rn=1;
else 
v_supplier_id:=SUPPLIERID;
END IF ;

select count(response_id) into v_divisor from SVM_EVALUATION_RESPONSE  rs,SVM_EVAL_RESPONSE_VALUES rsv   
 where fk_evaluator_user_id=USERID  and fk_eval_id=EVALID
 and fk_supplier_id=v_supplier_id
 and rs.response_id=rsv.fk_response_id  ;
 if v_divisor !=0
 THEN 
 

select round( ((select count(response_id) from SVM_EVALUATION_RESPONSE  rs,SVM_EVAL_RESPONSE_VALUES rsv   
 where fk_evaluator_user_id=USERID  and fk_eval_id=EVALID
  and fk_supplier_id=v_supplier_id
 and rs.response_id=rsv.fk_response_id and response_rank !=0)*100)/v_divisor) into v_count 
 
 from dual;
END IF;
 return v_count;


END ;
FUNCTION COUNT_EVAL_SCORE_SUPPLIER(EVALID NUMBER ,USERID NUMBER,SUPPLIERID NUMBER) RETURN NUMBER
IS
v_count Number:=0 ;
v_divisor Number;
v_supplier_id Number :=0;
BEGIN

IF SUPPLIERID is null
then 
 SELECT  FK_SUPPLIER_ID into  v_supplier_id
  FROM (SELECT FK_SUPPLIER_ID,
               row_number() OVER (ORDER BY VENDOR_NAME) rn
          FROM 
          
  ( select distinct RESPONSE_ID,FK_SUPPLIER_ID,VENDOR_NAME
    from SVM_EVALUATION_RESPONSE ,(select distinct  s.vendor_id,s.vendor_name from ap_suppliers s ,ap_supplier_sites_all ss       
where s.vendor_id=ss.vendor_id       
and ss.org_id=161)suppliers ,svm_evaluation ev      
where suppliers.vendor_id=fk_supplier_id      
and ev.evaluation_id=fk_eval_id    
and  fk_eval_id=EVALID and fk_evaluator_user_id=USERID
order by VENDOR_NAME asc)
          )
 WHERE  rn=1;
else 
v_supplier_id:=SUPPLIERID;
END IF ;

select (count(response_value_id)*5) into v_divisor from SVM_EVALUATION_RESPONSE  rs,SVM_EVAL_RESPONSE_VALUES rsv   
 where fk_evaluator_user_id=USERID  and fk_eval_id=EVALID
 and fk_supplier_id=v_supplier_id
 and rs.response_id=rsv.fk_response_id  ;
 if v_divisor !=0
 THEN 
 

select round( ((select nvl(sum(response_rank),0) from SVM_EVALUATION_RESPONSE  rs,SVM_EVAL_RESPONSE_VALUES rsv   
 where fk_evaluator_user_id=USERID  and fk_eval_id=EVALID
  and fk_supplier_id=v_supplier_id
 and rs.response_id=rsv.fk_response_id and response_rank !=0)*100)/v_divisor) into v_count 
 
 from dual;
END IF;
 return v_count;

 
 END ;
 FUNCTION COUNT_SURVEY_SCORE_SUPPLIER(SVID NUMBER ,USERID NUMBER) RETURN NUMBER
IS
v_count Number:=0 ;
v_divisor Number;

BEGIN



select( count(response_value_id)*5) into v_divisor   from SVM_SURVEY_RESPONSE  rs,SVM_SURVEY_RESPONSE_VALUE rsv        
where fk_supplier_user_id=userid  and fk_survey_id=svid      
 and rs.response_id=rsv.fk_response_id ;
 if v_divisor !=0
 THEN 
 

select  round( ((select nvl(sum(response_rank),0) from SVM_SURVEY_RESPONSE  rs,SVM_SURVEY_RESPONSE_VALUE rsv        
 where fk_supplier_user_id=userid  and fk_survey_id=svid      
 and rs.response_id=rsv.fk_response_id and response_rank !=0)*100)/v_divisor) into v_count    
 
 from dual;
END IF;
 return v_count;

 
 END ;
 
 function get_svm_menu(p_resp_id varchar2 , p_pages out varchar2  ) return varchar2
is
v_main_node_open varchar2(500):='<li class="nav-item"> <a href="" class="nav-link nav-toggle"> <i class="#icone-name#"></i> <span class="title">#node-title#</span> <span class="arrow"></span></a> <ul class="sub-menu">';
v_main_node_close varchar2(50):=' </ul></li>';
v_sub_node varchar2(500):='<li class="nav-item"> <a href="#page-name#" class="nav-link "><span class="title">#page-title#</span></a></li>';
v_menu varchar2(4000);
v_temp  varchar2(4000);
v_menu_begin varchar2(500):='<li class="sidebar-search-wrapper"></li><li class="#{attrs.HomeMenuItem}"><a href="Home.jsf" class="nav-link nav-toggle"><i class="icon-home"></i> <span class="title">Home</span><span class="selected"></span></a> </li><li class="heading"> <h3 class="uppercase">Navigation</h3>';
v_pages varchar2(4000):=';#Home.jsf#;#SurveyReportDetails.jsf#;#SurveyFeedBackDetails.jsf#;#buyerFeedbackDetails.jsf#;#Questions.jsf#;#SurveyFeedBackDetails.jsf#;#SurveysearchDetails.jsf#;#questionRankDetails.jsf#;#Eval.jsf#;#Survey.jsf#;#EvalClosed.jsf#;#EvalInProcess.jsf#;#EvalCompleted.jsf#;#EvalScheduled.jsf#;#DraftSuppliers.jsf#;#EvalSummaryDetails.jsf#;#EvaluationFeedBackDetails.jsf#;#EvaluationResponse.jsf#;#InProcessSuppliers.jsf#;#RegSuppliers.jsf#;#RejectedSuppliers.jsf#;#MeetExepecatationFeedBack.jsf#;#OverExpectationFeedBack.jsf#;#SatisifiedFeedBack.jsf#;#SurveyClosed.jsf#;#SurveyCompleted.jsf#;#SurveyFeedBackDetails.jsf#;#SurveyInprocess.jsf#;#SurveyReportDetails.jsf#;#Surveyscheduled.jsf#;#UnderExpectationFeedBack.jsf#;#UnsatisifiedFeedBack.jsf#;';
cursor menu is 
select rownum seq
      ,tree.menu_chain
      ,tree.prompts
      ,tree.menu_seq
      ,tree.type
      ,tree.page_name
      --,tree.icon_name
      ,web_icon
      ,description node_name
from
(
select entries.menu_id
     ,menu.menu_name
     ,connect_by_root entries.menu_id root_menu_id
     ,LEVEL Pathlevel
     ,SYS_CONNECT_BY_PATH('('||entries.prompt||')'||menu.menu_name, ' -> ') menu_chain
     ,entries.sub_menu_id
     ,entries.function_id
     ,entries.prompt prompts
      ,entries.ENTRY_SEQUENCE
      ,SYS_CONNECT_BY_PATH( entries.entry_sequence, '.') menu_seq
      ,decode(entries.SUB_MENU_ID, null, 'F', 'M') type
      ,(select substr(parameters, 10) from fnd_form_functions where function_id = entries.function_id) page_name
      ,decode(entries.SUB_MENU_ID, null, null, entries.description) icon_name , 
      sub_menu.web_icon
      ,sub_menu.description
from fnd_menu_entries_vl entries
    ,fnd_menus_vl menu
    ,fnd_menus_vl sub_menu
where 1 = 1
  and menu.menu_id = entries.menu_id
  and sub_menu.menu_id(+) = entries.sub_menu_id
  --start with entries.menu_id = (select menu_id from fnd_menus_vl where menu_name = :menu_name)
  start with entries.menu_id = (select menu_id from fnd_responsibility where responsibility_id = p_resp_id)
connect by prior entries.sub_menu_id = entries.menu_id
) 
tree;
begin
for i in menu loop
if i.type='M' then
        
        if(i.seq=1) then
                v_temp:=replace(v_main_node_open,'#icone-name#',i.web_icon);
                v_temp:=replace(v_temp,'#node-title#',i.node_name);
                v_menu:=v_menu||v_temp;
        else
                v_menu:=v_menu||v_main_node_close;
                v_temp:=replace(v_main_node_open,'#icone-name#',i.web_icon);
                v_temp:=replace(v_temp,'#node-title#',i.node_name);
                v_menu:=v_menu||v_temp;
        end if;
         
end if;
if i.type='F' then
                if (i.page_name!='-1') then
                v_temp:=replace(v_sub_node,'#page-name#',i.page_name);
                v_temp:=replace(v_temp,'#page-title#',i.prompts);
                v_menu:=v_menu||v_temp;
                v_pages:=v_pages||'#'||i.page_name||'#'||',';
                end if;
end if;
end loop;
v_menu:=v_menu_begin||v_menu||v_main_node_close;
p_pages:=v_pages;
return v_menu;
end get_svm_menu;

function get_svm_session_user(p_session_id IN VARCHAR2 ) return VARCHAR2
 as
   l_user_id   fnd_user.user_id%TYPE;
  
 begin
  APP_SESSION.VALIDATE_ICX_SESSION (
         p_icx_cookie_value   => p_session_id);

      SELECT fu.user_id
        INTO l_user_id
        FROM icx_sessions icx, fnd_user fu
       WHERE icx.XSID = p_session_id 
         AND icx.USER_ID = fu.USER_ID;

      RETURN l_user_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
 end get_svm_session_user; 

function get_svm_session_user(p_session_id IN VARCHAR2 , p_user_name out VARCHAR2) return VARCHAR2
 as
   l_user_id   fnd_user.user_id%TYPE;
   l_user_name   fnd_user.user_name%TYPE;
 begin
  APP_SESSION.VALIDATE_ICX_SESSION (
         p_icx_cookie_value   => p_session_id);

      SELECT fu.user_id , user_name
        INTO l_user_id , l_user_name
        FROM icx_sessions icx, fnd_user fu
       WHERE icx.XSID = p_session_id 
         AND icx.USER_ID = fu.USER_ID;

    p_user_name:= l_user_name;
   RETURN l_user_id;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'N';
 end get_svm_session_user; 
 
 FUNCTION image_Resize (P_BLOB IN BLOB) RETURN BLOB
   IS
     
      vSizedImage BLOB;

   BEGIN
      IF (P_BLOB IS NULL)
      THEN
         RETURN NULL;
      END IF;
        DBMS_Lob.createTemporary(vSizedImage, FALSE, DBMS_LOB.CALL);
       ORDSYS.OrdImage.processCopy(P_BLOB, 'maxScale=40 40', vSizedImage);
       
       return vSizedImage;
       
       
      end image_Resize;
END SVM_PKG;
/
