DROP PACKAGE APPS.XXSVM_WF;

CREATE OR REPLACE PACKAGE APPS.XXSVM_WF

AS
  PROCEDURE XXSVM_RUN_EVAL_WORKFLOW(P_EVAL_ID IN  NUMBER ,P_EVAL_NAME varchar2 ,P_PEROFRMER_NAME varchar2,P_EMPLOYEE_FULL_NAME varchar2,P_USER_ID NUMBER ,  RE_EVALUATION_SUPPLIER NUMBER DEFAULT NULL) ;
  PROCEDURE CREATE_EVALUATION_SUPPLIERS  (P_EVAL_ID IN NUMBER);
--    PROCEDURE SET_FROMROLE_CREATOR_NOTIFY  (
--      itemtype   IN       VARCHAR2,
--      itemkey    IN       VARCHAR2,
--      actid      IN       NUMBER,
--      funcmode   IN       VARCHAR2,
--      RESULT     IN OUT   VARCHAR2
--   );
    PROCEDURE SET_FROMROLE_EVALUATION_NOTIFY  (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   );
PROCEDURE XXSVM_START_EVALUATION (EVAL_ID NUMBER  ,OUT_RESULT OUT VARCHAR2) ;
PROCEDURE XXSVM_START_EVALUATION_CUSTOM (EVAL_ID NUMBER  ,OUT_RESULT OUT VARCHAR2) ;
   FUNCTION GET_EVAL_CREATOR(EVAL_ID NUMBER) RETURN VARCHAR2;
   PROCEDURE NOTIFICATION_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                 RESULTOUT   IN OUT VARCHAR2);
                                    PROCEDURE NOTIFICATION_ACTION_ADF (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                              
                                 
                                 RESULTOUT   IN OUT VARCHAR2);
                                      PROCEDURE XXSVM_START_ALL_EVALUATION (ERRBUF OUT VARCHAR2,
                  RETCODE OUT VARCHAR2) ;
                                      
                                      
                                      
                                        PROCEDURE XXSVM_RUN_SURVEY_WORKFLOW(P_SURVEY_ID IN  NUMBER ,P_SURVEY_NAME varchar2 ,P_PEROFRMER_NAME varchar2,P_EMPLOYEE_FULL_NAME varchar2,P_USER_ID NUMBER) ;
  PROCEDURE CREATE_SURVEY_SUPPLIERS  (P_SURVEY_ID IN NUMBER);
--    PROCEDURE SET_FROMROLE_CREATOR_NOTIFY  (
--      itemtype   IN       VARCHAR2,
--      itemkey    IN       VARCHAR2,
--      actid      IN       NUMBER,
--      funcmode   IN       VARCHAR2,
--      RESULT     IN OUT   VARCHAR2
--   );
    PROCEDURE SET_FROMROLE_SURVEY_NOTIFY  (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   );
PROCEDURE XXSVM_START_SURVEY (PSURVEY_ID NUMBER  ,OUT_RESULT OUT VARCHAR2) ;
   FUNCTION GET_SURVEY_CREATOR(PSURVEY_ID NUMBER) RETURN VARCHAR2;
   PROCEDURE SURVEY_NOTIFICATION_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                 RESULTOUT   IN OUT VARCHAR2);
                                      PROCEDURE XXSVM_START_ALL_SURVEY (ERRBUF OUT VARCHAR2,
                  RETCODE OUT VARCHAR2) ;
        FUNCTION  XXSVM_GET_REMINDER_COUNT RETURN NUMBER ;
        PROCEDURE XXSVM_RUN_REMINDER_WORKFLOW(ERRBUF OUT VARCHAR2,
                  RETCODE OUT VARCHAR2);
procedure LoopCounter(  itemtype   in varchar2,
                        itemkey    in varchar2,
                        actid      in number,
                        funcmode   in varchar2,
                        resultout  in out nocopy varchar2);
                        procedure SET_ESCALTE_ATTRIBUTES(ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                 RESULTOUT   IN OUT VARCHAR2);
--                                                 procedure LoopCounterSuppliers(  itemtype   in varchar2,
--                        itemkey    in varchar2,
--                        actid      in number,
--                        funcmode   in varchar2,
--                        resultout  in out nocopy varchar2);
                       PROCEDURE XXSVM_RUN_SUPP_APPROV_WORKFLOW (vcost varchar2,vdelivery varchar2,vquality varchar2,vservice varchar2,vweightscore varchar2,vmeanvalue varchar2,vsuppliername varchar2,vsuppemail varchar2,vyear varchar2);
                           PROCEDURE APRROVE_REJECT (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2);
function RE_EVALUATE_SUPPLIER (P_EVAL_ID NUMBER ,P_EVAL_NAME varchar2,  P_EVALUATER_USER_ID NUMBER, P_EVALUATER_USER_NAME varchar2 ,P_EVALUATER_FULL_NAME varchar2 , P_SUPPLIER_ID NUMBER , P_RESPONSE_ID NUMBER,P_USER_ID NUMBER , P_justification  varchar2) RETURN VARCHAR2;
END ;
/
