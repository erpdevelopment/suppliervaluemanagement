DROP PACKAGE BODY APPS.XXSVM_WF;

CREATE OR REPLACE PACKAGE BODY APPS.XXSVM_WF

AS
 PROCEDURE XXSVM_RUN_EVAL_WORKFLOW(P_EVAL_ID IN  NUMBER ,P_EVAL_NAME varchar2 ,P_PEROFRMER_NAME varchar2,P_EMPLOYEE_FULL_NAME varchar2,P_USER_ID NUMBER , RE_EVALUATION_SUPPLIER number DEFAULT null) 
 is
v_itemtype varchar2(1000);
v_itemkey varchar2(1000);
v_process varchar2(1000);
 v_creator varchar2(400); 
--v_role varchar2(100) := 'SVM_EVALUATOR_ROLE_4';
--v_role_desc varchar2(100) := ' EVALUATOR_ROLE';

cursor c is 
select user_id,user_name ,person_id,full_name 
from fnd_user usr,per_people_f papf ,SVM_EVAL_EVALUATORS evaluators
where usr.EMPLOYEE_ID=papf.person_id
and evaluators.FK_EMPLOYEE_ID=papf.person_id
and evaluators.FK_EVAL_ID=P_EVAL_ID
 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1) ;

begin 
--- update item key 
--- check roles and delete used roles and check how to create roles aotmatically
v_itemtype :='SVM_EVAL';
v_itemkey:='EVAL'||EVAL_WORKFLOW_seq.nextval;
--||'/'||to_char(sysdate,'dd')||to_char(sysdate,'mm')||to_char(sysdate,'yy');
v_process:='SVM_EVAL';
  -- Create Process
  wf_engine.createprocess (v_itemtype, v_itemkey, v_process);
  --- Set Attributes 
  -- role Attribute  for the message 
     wf_engine.setitemowner (itemtype      => v_itemtype,
                              itemkey       => v_itemkey,
                              owner         => 'SYSADMIN'
                             );
      wf_engine.setitemattrnumber (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'P_EVAL_ID',
                                   avalue        => P_EVAL_ID);
                                   
       wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'EVAL_NAME',
                                 avalue        => P_EVAL_NAME|| ' - Number '||  lpad ( P_EVAL_ID, 6, '0' ) || case when  RE_EVALUATION_SUPPLIER is null then  '' else  '(Re-Evalaution)' end);
                                    wf_engine.setitemattrnumber (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'P_USER_ID', 
                                   avalue        => P_USER_ID);
                                   
          wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'EMPLOYEENAME',
                                   avalue        => P_EMPLOYEE_FULL_NAME);       

       v_creator:=XXSVM_WF.GET_EVAL_CREATOR(P_EVAL_ID);
                                      wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#FROM_ROLE',
                                   avalue        => 'SYSADMIN');
                                      wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#CREATOR',
                                   avalue        => v_creator);                    

                  wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#EVALUATORS_ROLES',
                                   avalue        => P_PEROFRMER_NAME);
            if (RE_EVALUATION_SUPPLIER is not null) then
                   wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'RE_EVALUATION_SUPPLIER',
                                   avalue        => RE_EVALUATION_SUPPLIER);
            end if;
            
   wf_engine.startprocess (v_itemtype, v_itemkey);
     COMMIT;
   
end ;
 PROCEDURE CREATE_EVALUATION_SUPPLIERS  (P_EVAL_ID IN NUMBER)
      
      
   
 is 
 cursor evals is
 select user_id,user_name ,person_id,EVAL_EMPLOYEE_ID,full_name 
from fnd_user usr,per_people_f papf ,SVM_EVAL_EVALUATORS evaluators
where usr.EMPLOYEE_ID=papf.person_id
and evaluators.FK_EMPLOYEE_ID=papf.person_id
and evaluators.FK_EVAL_ID=P_EVAL_ID
 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1) ;
   cursor suppliers is
    
                          select fk_supplier_id from SVM_EVAL_SUPPLIERS
                          where fk_eval_id=P_EVAL_ID;
   cursor evalresponse is 
    select * from  SVM_EVALUATION_RESPONSE
 where fk_eval_id=P_EVAL_ID;
  cursor questions is 
   select TEMPLATE_ID,QUESTION_ID  from   SVM_EVALUATION_TEMPLATE ev ,svm_templates tp ,svm_template_questions qt
where  EV.FK_TEMPLATE_ID=tp.template_id
and qt.fk_template_id=tp.template_id
and fk_eval_id=P_EVAL_ID;

 
 
 begin 
 -- Insert Evaluation Response rows
 for e in evals
 loop
 for s in suppliers 
 loop
  insert into SVM_EVALUATION_RESPONSE values(SVM_EVALUATION_RESPONSE_seq.nextval ,P_EVAL_ID,e.EVAL_EMPLOYEE_ID,e.user_id,e.person_id,s.fk_supplier_id,0);
 
 
 
 end loop;----end loop of suppliers
 commit;
 end loop ;--- end loop of evals
 --- Responses Values
 
for er in  evalresponse
loop
for q in questions 
loop
  insert into SVM_EVAL_RESPONSE_VALUES values(SVM_EVAL_RESPONSE_VALUES_seq.nextval,er.RESPONSE_ID,q.TEMPLATE_Id,q.QUESTION_ID,0);

end loop; --end questios
commit;
end loop ;--end evaluation response 
 end;
 
-- PROCEDURE SET_FROMROLE_CREATOR_NOTIFY  (
--      itemtype   IN       VARCHAR2,
--      itemkey    IN       VARCHAR2,
--      actid      IN       NUMBER,
--      funcmode   IN       VARCHAR2,
--      RESULT     IN OUT   VARCHAR2
--   )
-- is 
-- v_eval_id Number ;
-- v_creator varchar2(400);
-- begin 
--
-- v_eval_id:=wf_engine.getitemattrnumber (itemtype      => itemtype,
--                                      itemkey       => itemkey,
--                                      aname         => 'P_EVAL_ID'
--                                     );
--                                      v_creator:=XXSVM_WORKFLOW.GET_EVAL_CREATOR(v_eval_id);
--                                      wf_engine.setitemattrtext (itemtype      => itemtype,
--                                   itemkey       => itemkey,
--                                   aname         => '#FROM_ROLE',
--                                   avalue        => 'SYSADMIN');
--                                      wf_engine.setitemattrtext (itemtype      => itemtype,
--                                   itemkey       => itemkey,
--                                   aname         => '#CREATOR',
--                                   avalue        => v_creator);  
--                                     --- Create rows in tables EvaluationResponse
--                                     
--                          RESULT := 'COMPLETE:';             
--                                     
--                                     --- Responses Values
-- end;
  PROCEDURE SET_FROMROLE_EVALUATION_NOTIFY  (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   )
 is 
 v_eval_id Number ;
-- v_notification_id Number;
-- v_access_key varchar2(4000);
 v_re_eval_supplier number;
 v_creator varchar2(400);
 V_URL VARCHAR2(2000);
 V_ADFFUNCTION varchar2(500);
 begin 

 v_eval_id:=wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'P_EVAL_ID'
                                     );
                                     
                                      v_re_eval_supplier:=wf_engine.getitemattrtext (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'RE_EVALUATION_SUPPLIER'
                                     );
--                              v_access_key :=wf_engine.getitemattrtext (itemtype      => itemtype,
--                                      itemkey       => itemkey,
--                                      aname         => '#NID'
--                                     );
                                -- V_ADFFUNCTION:='XXMBC_ADF_SVM_EVAL_PAGE';
                           
--V_URL :=V_URL||'JSP:/OA_HTML/OA.jsp?OAFunc=XXMBC_SVM_EVAL&evID='||v_eval_id;

if v_re_eval_supplier is not NULL then

V_URL :=V_URL||'JSP:/OA_HTML/OA.jsp?OAFunc=XXMBC_SVM_EVAL&evID='||v_eval_id||'&ssId='||v_re_eval_supplier;
else
V_URL :=V_URL||'JSP:/OA_HTML/OA.jsp?OAFunc=XXMBC_SVM_EVAL&evID='||v_eval_id||'&ssId=-1'||v_re_eval_supplier;
end if;
                                      v_creator:=XXSVM_WF.GET_EVAL_CREATOR(v_eval_id);
                                      wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => '#FROM_ROLE',
                                   avalue        => v_creator);
                                     
 wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => 'EMEDDED_REGION',
                                   avalue        => V_URL);
                                     --- Create rows in tables EvaluationResponse
                                 --  CREATE_EVALUATION_SUPPLIERS(v_eval_id);  
                          RESULT := 'COMPLETE:';             
                                     
                                     --- Responses Values
 end;
 
    PROCEDURE XXSVM_START_EVALUATION (EVAL_ID NUMBER  ,OUT_RESULT OUT VARCHAR2) 
      is
    v_eval_id Number;
   -- v_eval_name varchar2(500);
  -- v_result varchar2(200);
    
    cursor  emp (v_id Number)is 
select user_id,user_name ,person_id,full_name 
from fnd_user usr,per_people_f papf ,SVM_EVAL_EVALUATORS evaluators
where usr.EMPLOYEE_ID=papf.person_id
and evaluators.FK_EMPLOYEE_ID=papf.person_id
and evaluators.FK_EVAL_ID=v_id
 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1);
                         
                          
                           
    cursor evals is 
     select evaluation_id ,evaluation_name  ,is_started ,EVALUATION_START_DATE
  from SVM_EVALUATION
   -- where EVALUATION_START_DATE=to_date(sysdate,'dd/mon/yy') 
     where evaluation_id=EVAL_ID ;
    
    begin 

    --and rownum=1;
    
    --loop through the evals and check if eval is_statred don't create evaluation response rows and start the workflow ,else create rows ans start workflow
    for i in evals 
    loop
    if(i.EVALUATION_START_DATE=to_date(sysdate,'dd/mon/yy'))
    then 
    
    if(i.is_started='N')
    then 
    update svm_evaluation
    set is_started='Y'
    where evaluation_id=i.evaluation_id;
   CREATE_EVALUATION_SUPPLIERS(i.evaluation_id);
       for ii in emp(i.evaluation_id)
    loop
    -- start process for each employee
        XXSVM_WF.XXSVM_RUN_EVAL_WORKFLOW(i.evaluation_id ,i.evaluation_name,ii.user_name,ii.full_name,ii.user_id,null );
      end loop ;
        OUT_RESULT:='S';
         elsif (i.is_started='Y')
        then 
        OUT_RESULT:='N';
                 
    end if ;
    elsif(i.EVALUATION_START_DATE!=to_date(sysdate,'dd/mon/yy'))
    then 
    OUT_RESULT:='U';
end if ;-- end match date
    end loop ;
    
   
     
    
    
--     EXCEPTION
--      WHEN NO_DATA_FOUND
--      THEN
--         DBMS_OUTPUT.PUT_LINE('Error: No data found.');
--   OUT_RESULT:='Y';
  --DBMS_OUTPUT.PUT_LINE("result"+OUT_RESULT)
  -- return v_result;
  
   end ;
  
         PROCEDURE XXSVM_START_EVALUATION_CUSTOM (EVAL_ID NUMBER  ,OUT_RESULT OUT VARCHAR2) 
      is
    v_eval_id Number;
   -- v_eval_name varchar2(500);
  -- v_result varchar2(200);
    
    cursor  emp (v_id Number)is 
select user_id,user_name ,person_id,full_name 
from fnd_user usr,per_people_f papf ,SVM_EVAL_EVALUATORS evaluators
where usr.EMPLOYEE_ID=papf.person_id
and evaluators.FK_EMPLOYEE_ID=papf.person_id
and evaluators.FK_EVAL_ID=v_id
 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1);
                         
                          
                           
    cursor evals is 
     select evaluation_id ,evaluation_name  ,is_started ,EVALUATION_START_DATE
  from SVM_EVALUATION
   -- where EVALUATION_START_DATE=to_date(sysdate,'dd/mon/yy') 
     where evaluation_id=EVAL_ID ;
    
    begin 

    --and rownum=1;
    
    --loop through the evals and check if eval is_statred don't create evaluation response rows and start the workflow ,else create rows ans start workflow
    for i in evals 
    loop
    if(i.EVALUATION_START_DATE!=to_date(sysdate,'dd/mon/yy'))
    then 
    
   if(i.is_started='N')
    then 
    update svm_evaluation
    set is_started='Y'
    where evaluation_id=i.evaluation_id;
   CREATE_EVALUATION_SUPPLIERS(i.evaluation_id);
       for ii in emp(i.evaluation_id)
    loop
    -- start process for each employee
        XXSVM_WF.XXSVM_RUN_EVAL_WORKFLOW(i.evaluation_id ,i.evaluation_name,ii.user_name,ii.full_name,ii.user_id,null );
      end loop ;
        OUT_RESULT:='S';
         elsif (i.is_started='Y')
        then 
        OUT_RESULT:='N';
                 
    end if ;
    elsif(i.EVALUATION_START_DATE=to_date(sysdate,'dd/mon/yy'))
    then 
    OUT_RESULT:='U';
end if ;-- end match date
    end loop ;
    
   
     
    
    
--     EXCEPTION
--      WHEN NO_DATA_FOUND
--      THEN
--         DBMS_OUTPUT.PUT_LINE('Error: No data found.');
--   OUT_RESULT:='Y';
  --DBMS_OUTPUT.PUT_LINE("result"+OUT_RESULT)
  -- return v_result;
  
   end ; 
       FUNCTION GET_EVAL_CREATOR(EVAL_ID NUMBER) RETURN VARCHAR2
        is 
        V_CREATOR varchar2(400);
        begin 
        
                        select user_name into V_CREATOR 
  from SVM_EVALUATION ev ,fnd_user usr
  
 where 
 -- commented by amr 02-01-2019
 --EVALUATION_START_DATE=
 -- to_date(sysdate,'dd/mon/yy') and
     usr.user_id=ev.created_by
        and evaluation_id=EVAL_ID;
      
       
        return V_CREATOR;
        end ;   
        
           PROCEDURE NOTIFICATION_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
   IS
     v_eval_id Number ;
     v_userid Number;
     v_count Number ;
     
   BEGIN
   
    v_eval_id:=wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'P_EVAL_ID'
                                     );
                                v_userid := wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'P_USER_ID'
                                     );
--  
  insert into XX_TEST_WORKFLOW values(v_eval_id);
  insert into XX_TEST_WORKFLOW values(v_userid);
  -- commit;
    IF FUNCMODE  in('RUN','VALIDATE','RESPOND')
   THEN 
   insert into XX_TEST_WORKFLOW values(FUNCMODE);
  
     select count(response_rank) into v_count from  SVM_EVALUATION_RESPONSE response ,SVM_EVAL_RESPONSE_VALUES val 
     --,fnd_user usr
where response.response_id=val.fk_response_id
--and usr.user_id=response.FK_EVALUATOR_USER_ID
and response.FK_EVALUATOR_USER_ID=v_userid
and fk_eval_id=v_eval_id
and response_rank=0;
if v_count !=0
then
raise_application_error(-20002,'You  need to Rank whole Evaluation Questions');
         -- WF_CORE.TOKEN('ROLE', WF_ENGINE.context_new_role);
         -- WF_CORE.RAISE('WFNTF_TRANSFER_FAIL');
        end if;
        if(v_count=0)
        THEN
   UPDATE SVM_EVAL_EVALUATORS
   SET RESPONSE_COMPLETED='Y'

where  FK_EMPLOYEE_ID=(select person_id
from fnd_user usr,per_people_f papf 
where usr.EMPLOYEE_ID=papf.person_id
and user_id=v_userid

 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1))
and FK_EVAL_ID=v_eval_id ;
---- For Reminder notifcation
  wf_engine.SetItemAttrText(itemtype => ITEMTYPE,
                                              itemkey  => ITEMKEY,
                                              aname    => 'REMINDER_NOTIF_VAL_ATTR',
                                              avalue   => 'APPROVE_REJECT');    

        commit;
        END IF ;
        
 END IF ;
   END NOTIFICATION_ACTION; 

   PROCEDURE NOTIFICATION_ACTION_ADF (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                              
                               
                                  RESULTOUT   IN OUT VARCHAR2)
   IS
     v_eval_id Number ;
     v_userid Number;
     v_count Number ;
     
   BEGIN
   
    v_eval_id:=wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'P_EVAL_ID'
                                     );
                                v_userid := wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'P_USER_ID'
                                     );
--  

  -- commit;

  
     select count(response_rank) into v_count from  SVM_EVALUATION_RESPONSE response ,SVM_EVAL_RESPONSE_VALUES val 
     --,fnd_user usr
where response.response_id=val.fk_response_id
--and usr.user_id=response.FK_EVALUATOR_USER_ID
and response.FK_EVALUATOR_USER_ID=v_userid
and fk_eval_id=v_eval_id
and response_rank=0;
if v_count !=0
then
RESULTOUT:='UNCOMPLETE';
--raise_application_error(-20002,'You  need to Rank whole Evaluation Questions');
    
        end if;
        if(v_count=0)
        THEN
   UPDATE SVM_EVAL_EVALUATORS
   SET RESPONSE_COMPLETED='Y'

where  FK_EMPLOYEE_ID=(select person_id
from fnd_user usr,per_people_f papf 
where usr.EMPLOYEE_ID=papf.person_id
and user_id=v_userid

 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1))
and FK_EVAL_ID=v_eval_id ;
---- For Reminder notifcation
  wf_engine.SetItemAttrText(itemtype => ITEMTYPE,
                                              itemkey  => ITEMKEY,
                                              aname    => 'REMINDER_NOTIF_VAL_ATTR',
                                              avalue   => 'APPROVE_REJECT');    

       
        ---- stop activivty
        WF_ENGINE.COMPLETEACTIVITY (
            ITEMTYPE   => ITEMTYPE,
            ITEMKEY    => ITEMKEY,
            ACTIVITY   => 'EVALRESPONSENOTIFY',
            RESULT     => 'SEND_RESPONSE');

        RESULTOUT:='COMPLETE';
         commit;
        END IF ;
        

   END NOTIFICATION_ACTION_ADF; 

 PROCEDURE XXSVM_START_ALL_EVALUATION (ERRBUF OUT VARCHAR2,
                  RETCODE OUT VARCHAR2) 
    is
  --  v_eval_id Number;
   -- v_eval_name varchar2(500);
    
    cursor  emp (v_id Number)is 
select user_id,user_name ,person_id,full_name 
from fnd_user usr,per_people_f papf ,SVM_EVAL_EVALUATORS evaluators
where usr.EMPLOYEE_ID=papf.person_id
and evaluators.FK_EMPLOYEE_ID=papf.person_id
and evaluators.FK_EVAL_ID=v_id
 AND SYSDATE BETWEEN NVL (PAPF.EFFECTIVE_START_DATE, SYSDATE - 1)
                          AND NVL (PAPF.EFFECTIVE_END_DATE, SYSDATE + 1)
                         
                          
                           ;
    cursor evals is 
     select evaluation_id ,evaluation_name  ,is_started
  from SVM_EVALUATION
    where EVALUATION_START_DATE=to_date(sysdate,'dd/mon/yy') and is_started='N'  ;
    -- and evaluation_id=106 ;
    
    begin 

    --and rownum=1;
    
    --loop through the evals and check if eval is_statred don't create evaluation response rows and start the workflow ,else create rows ans start workflow
    for i in evals 
    loop
    --if(i.is_started='N')
    --then 
    update svm_evaluation
    set is_started='Y'
    where evaluation_id=i.evaluation_id;
    CREATE_EVALUATION_SUPPLIERS(i.evaluation_id);
    --end if ;
    for ii in emp(i.evaluation_id)
    loop
    -- start process for each employee
    
    XXSVM_WF.XXSVM_RUN_EVAL_WORKFLOW(i.evaluation_id ,i.evaluation_name,ii.user_name,ii.full_name,ii.user_id,null );
    
    end loop ;
    end loop ;
    
    
  --   ;
    
    
     EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE('Error: No data found.');
  
  
   end ;
   
   /********************************Survey WorkFlow*****************************************************/
   PROCEDURE XXSVM_RUN_SURVEY_WORKFLOW(P_SURVEY_ID IN  NUMBER ,P_SURVEY_NAME varchar2 ,P_PEROFRMER_NAME varchar2,P_EMPLOYEE_FULL_NAME varchar2,P_USER_ID NUMBER) 
 is
v_itemtype varchar2(1000);
v_itemkey varchar2(1000);
v_process varchar2(1000);
 v_creator varchar2(400); 


begin 

v_itemtype :='SVM_EVAL';
v_itemkey:='SURVEY'||SURVEY_WORKFLOW_seq.nextval;
--||'/'||to_char(sysdate,'dd')||to_char(sysdate,'mm')||to_char(sysdate,'yy');
v_process:='SVM_SURVEY_PRC';
  -- Create Process
  wf_engine.createprocess (v_itemtype, v_itemkey, v_process);
  --- Set Attributes 
  -- role Attribute  for the message 
     wf_engine.setitemowner (itemtype      => v_itemtype,
                              itemkey       => v_itemkey,
                              owner         => 'SYSADMIN'
                             );
      wf_engine.setitemattrnumber (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'SURVEY_ID',
                                   avalue        => P_SURVEY_ID);
                                  
       wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'SURVEY_NAME',
                                  avalue        => P_SURVEY_NAME|| ' - Number '||  lpad ( P_SURVEY_ID, 6, '0' ));
                                  
                                    wf_engine.setitemattrnumber (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'S_USER_ID',
                                   avalue        => P_USER_ID);
                                   
          wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'S_FULL_NAME',
                                   avalue        => P_EMPLOYEE_FULL_NAME);       

       v_creator:=XXSVM_WF.GET_SURVEY_CREATOR(P_SURVEY_ID);
                                      wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#S_FROM_ROLE',
                                   avalue        => 'SYSADMIN');
                                      wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => '#S_CREATOR',
                                   avalue        => v_creator);                    

                  wf_engine.setitemattrtext (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'S_SUPPLIER_ROLE',
                                   avalue        => P_PEROFRMER_NAME);
--P_PEROFRMER_NAME
                 
  
   wf_engine.startprocess (v_itemtype, v_itemkey);
     COMMIT;
   
end ;
   
 PROCEDURE CREATE_SURVEY_SUPPLIERS  (P_SURVEY_ID IN NUMBER)
      
      
   
 is 

   cursor suppliers is
      select fk_supplier_id ,user_id 
      from XXSVM_REGISTERED_SUPPLIERS rs ,svm_survey_suppliers ss
where rs.vendor_id=ss.fk_supplier_id
and fk_survey_id=P_SURVEY_ID;
                       
   cursor surveyresponse is 
    select * from  SVM_SURVEY_RESPONSE
 where fk_survey_id=P_SURVEY_ID;
  cursor questions is 
   select TEMPLATE_ID,QUESTION_ID  from   SVM_SURVEY_TEMPLATE sv ,svm_templates tp ,svm_template_questions qt
where  sv.FK_TEMPLATE_ID=tp.template_id
and qt.fk_template_id=tp.template_id
 and fk_survey_id=P_SURVEY_ID;

 
 
 begin 
 -- Insert Evaluation Response rows

 for s in suppliers 
 loop
  insert into SVM_SURVEY_RESPONSE values(SVM_SURVEY_RESPONSE_seq.nextval ,P_SURVEY_ID ,s.fk_supplier_id,s.user_id);
 
   end loop;----end loop of suppliers
 commit;

 --- Responses Values
 
for er in  surveyresponse
loop
for q in questions 
loop
  insert into SVM_SURVEY_RESPONSE_VALUE values(SVM_EVAL_RESPONSE_VALUES_seq.nextval,er.RESPONSE_ID,q.TEMPLATE_Id,q.QUESTION_ID,0);

end loop; --end questios
commit;
end loop ;--end evaluation response 
 end;
 

  PROCEDURE SET_FROMROLE_SURVEY_NOTIFY  (
      itemtype   IN       VARCHAR2,
      itemkey    IN       VARCHAR2,
      actid      IN       NUMBER,
      funcmode   IN       VARCHAR2,
      RESULT     IN OUT   VARCHAR2
   )
 is 
 v_survey_id Number ;
 v_creator varchar2(400);
 V_URL VARCHAR2(2000);
 V_ADFFUNCTION varchar2(500);
 begin 

 v_survey_id:=wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'SURVEY_ID'
                                     );
                                -- V_ADFFUNCTION:='XXMBC_ADF_SVM_SURVEY_PAGE';
                                     
 V_URL :='JSP:/OA_HTML/OA.jsp?OAFunc=XXMBC_SVM_SURVEY&svID='||v_survey_id;
                                      v_creator:=XXSVM_WF.GET_SURVEY_CREATOR(v_survey_id);
                                      wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => '#S_FROM_ROLE',
                                   avalue        => v_creator);
                                     
                                     wf_engine.setitemattrtext (itemtype      => itemtype,
                                   itemkey       => itemkey,
                                   aname         => 'SURVEY_EMBED_REGION',
                                   avalue        => V_URL);
                                     --- Create rows in tables EvaluationResponse
                                 --  CREATE_EVALUATION_SUPPLIERS(v_eval_id);  
                          RESULT := 'COMPLETE:';             
                                     
                                     --- Responses Values
 end;
 
--    PROCEDURE XXSVM_START_SURVEY (SURVEY_ID NUMBER  ,OUT_RESULT OUT VARCHAR2) 
--      is
--    v_survey_id Number;
--    v_survey_start_date Date;
--   -- v_eval_name varchar2(500);
--  -- v_result varchar2(200);
--    
----    cursor  supp (sv_id Number)is 
----select user_id,UPPER(username) username ,suppliername 
---- from XXSVM_REGISTERED_SUPPLIERS rs ,svm_survey_suppliers ss
----where rs.vendor_id=ss.fk_supplier_id
----and fk_survey_id=sv_id;
--                         
--                          
--                           
--   -- cursor surveys is 
--     
--     --survey_id ,survey_name  ,is_started ,
--   
--    -- SURVEY_ID;
--     -- ;
--    
--    begin 
--
--    --and rownum=1;
--    
--    --loop through the evals and check if eval is_statred don't create evaluation response rows and start the workflow ,else create rows ans start workflow
--    --for i in surveys 
--   -- loop
--    select  survey_START_DATE into v_survey_start_date
--  from SVM_survey
--   -- where EVALUATION_START_DATE=to_date(sysdate,'dd/mon/yy') 
--     where survey_id=1;
--     --SURVEY_ID;
--     --;
--    -- OUT_RESULT:='R';
--   -- if(v_survey_start_date=to_date(sysdate,'dd/mon/yy'))
--   -- then 
--    
----    if(i.is_started='N')
----    then 
----    update svm_survey
----    set is_started='Y'
----    where survey_id=i.survey_id;
----   CREATE_SURVEY_SUPPLIERS(i.survey_id);
----       for ii in supp(i.survey_id)
----    loop
----    -- start process for each employee
----        XXSVM_WF.XXSVM_RUN_SURVEY_WORKFLOW(i.survey_id ,i.survey_name,ii.username,ii.suppliername,ii.user_id );
----      end loop ;
--     --  OUT_RESULT:='S';
----         elsif (i.is_started='Y')
----        then 
----        OUT_RESULT:='N';
----                 
-- -- end if ;
--    --elsif(v_survey_start_date!=to_date(sysdate,'dd/mon/yy'))
--    --then 
--  --  OUT_RESULT:='U';
----end if ;-- end match date
--   -- end loop ;
--    
--   
--     
--    
--    OUT_RESULT:='12';
--           DBMS_OUTPUT.PUT_LINE(OUT_RESULT);
--    --to_char(SURVEY_ID);
--
--  
--   end ;
  PROCEDURE XXSVM_START_SURVEY (PSURVEY_ID NUMBER ,OUT_RESULT OUT VARCHAR2 ) 
      is
    v_survey_id Number;
    v_survey_name varchar2(500);
    v_survey_start_date Date;
    v_is_started varchar(200);
   -- OUT_RESULT VARCHAR2(500);
   -- v_eval_name varchar2(500);
  -- v_result varchar2(200);
    
    cursor  supp (sv_id Number)is 
    --- change username from registered suppliers to SUPPLIER_USER_NAME from table svm_survey_suppliers and get user id of user email in field SUPPLIER_USER_NAME
select user_id,UPPER(SUPPLIER_USER_NAME) username ,suppliername 
 from XXSVM_REGISTERED_SUPPLIERS rs ,svm_survey_suppliers ss
where rs.vendor_id=ss.fk_supplier_id
and rs.username=SUPPLIER_USER_NAME
and fk_survey_id=sv_id;
                         
                          
                           
  
    
    begin 

    
    select  survey_START_DATE ,is_started  ,survey_name into v_survey_start_date ,v_is_started ,v_survey_name
  from SVM_survey
      where survey_id=PSURVEY_ID;
     --SURVEY_ID;
     --;
    -- OUT_RESULT:='R';
    if(v_survey_start_date=to_date(sysdate,'dd/mon/yy'))
    then 
    
   if(v_is_started='N')
    then 
   update svm_survey
   set is_started='Y'
   where survey_id=PSURVEY_ID;
  CREATE_SURVEY_SUPPLIERS(PSURVEY_ID);
       for ii in supp(PSURVEY_ID)
    loop
    -- start process for each employee
        XXSVM_WF.XXSVM_RUN_SURVEY_WORKFLOW(Psurvey_id ,v_survey_name,ii.username,ii.suppliername,ii.user_id );
      end loop ;
   OUT_RESULT:='S';
         elsif (v_is_started='Y')
        then 
       OUT_RESULT:='N';
                 
  end if ;
    elsif(v_survey_start_date!=to_date(sysdate,'dd/mon/yy'))
    then 
   OUT_RESULT:='U';
   end if ;-- end match date
   -- end loop ;
    
   
     
    
   -- OUT_RESULT:='12';
           DBMS_OUTPUT.PUT_LINE(OUT_RESULT);
    --to_char(SURVEY_ID);

  
   end ;
  
         
       FUNCTION GET_SURVEY_CREATOR(PSURVEY_ID NUMBER) RETURN VARCHAR2
        is 
        V_CREATOR varchar2(400);
        begin 
        
                        select user_name into V_CREATOR 
  from SVM_SURVEY sv ,fnd_user usr
    where SURVEY_START_DATE=to_date(sysdate,'dd/mon/yy') 
    and usr.user_id=sv.create_by
        and survey_id=PSURVEY_ID;
      
       
        return V_CREATOR;
        end ;   
        
           PROCEDURE SURVEY_NOTIFICATION_ACTION (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
   IS
     v_survey_id Number ;
     v_userid Number;
     v_count Number ;
     
   BEGIN
   
    v_survey_id:=wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'SURVEY_ID'
                                     );
                                v_userid := wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'S_USER_ID'
                                     );
--  
  insert into XX_TEST_WORKFLOW values(v_survey_id);
  insert into XX_TEST_WORKFLOW values(v_userid);
  -- commit;
    IF FUNCMODE  in('RUN','VALIDATE','RESPOND')
   THEN 
   insert into XX_TEST_WORKFLOW values(FUNCMODE);
  
     select count(response_rank) into v_count from  SVM_SURVEY_RESPONSE response ,SVM_SURVEY_RESPONSE_VALUE val 
     --,fnd_user usr
where response.response_id=val.fk_response_id
--and usr.user_id=response.FK_EVALUATOR_USER_ID
and response.FK_SUPPLIER_USER_ID=v_userid
and fk_survey_id=v_survey_id
and response_rank=0;
if v_count !=0
then
raise_application_error(-20002,'You  need to Rank whole Survey Questions');
         -- WF_CORE.TOKEN('ROLE', WF_ENGINE.context_new_role);
         -- WF_CORE.RAISE('WFNTF_TRANSFER_FAIL');
        end if;
        if v_count=0
        then 
        UPDATE SVM_SURVEY_SUPPLIERS
        
        SET  RESPONSE_COMPLETED='Y'
        WHERE  fk_survey_id=v_survey_id
        and FK_SUPPLIER_ID=
        
      (  select vendor_id 
 from XXSVM_REGISTERED_SUPPLIERS rs 
where user_id=v_userid) ;

commit ;
        
        
        end if ;
 END IF ;
   END SURVEY_NOTIFICATION_ACTION; 


 PROCEDURE XXSVM_START_ALL_SURVEY (ERRBUF OUT VARCHAR2,
                  RETCODE OUT VARCHAR2)
    is
  --  v_eval_id Number;
   -- v_eval_name varchar2(500);
    

   
   
   
   cursor  supp (sv_id Number)is 
--select user_id,UPPER(username) username ,suppliername 
-- from XXSVM_REGISTERED_SUPPLIERS rs ,svm_survey_suppliers ss
--where rs.vendor_id=ss.fk_supplier_id
--and fk_survey_id=sv_id;
              --- change username from registered suppliers to SUPPLIER_USER_NAME from table svm_survey_suppliers and get user id of user email in field SUPPLIER_USER_NAME
select user_id,UPPER(SUPPLIER_USER_NAME) username ,suppliername 
 from XXSVM_REGISTERED_SUPPLIERS rs ,svm_survey_suppliers ss
where rs.vendor_id=ss.fk_supplier_id
and rs.username=SUPPLIER_USER_NAME
and fk_survey_id=sv_id;               
                          
                           
    cursor survey is 
     select survey_id ,survey_name  ,is_started
  from SVM_survey
    where survey_START_DATE=to_date(sysdate,'dd/mon/yy') and is_started='N' ;
    -- and evaluation_id=106 ;
    
    begin 

    --and rownum=1;
    
    --loop through the surveys and check ifsurvey is_statred don't create survey response rows and start the workflow ,else create rows ans start workflow
    for i in survey 
    loop
   -- if(i.is_started='N')
    --then 
    update svm_survey
    set is_started='Y'
    where survey_id=i.survey_id;
    CREATE_SURVEY_SUPPLIERS(i.survey_id);
   -- end if ;
    for ii in supp(i.survey_id)
    loop
    -- start process for each employee
    
    XXSVM_WF.XXSVM_RUN_SURVEY_WORKFLOW(i.survey_id ,i.survey_name,ii.username,ii.suppliername,ii.user_id );
    
    end loop ;
    end loop ;
    
    
  --   ;
    
    
     EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         DBMS_OUTPUT.PUT_LINE('Error: No data found.');
  
  
   end ;
   
   
   /******************************** End Survey WorkFlow*****************************************************/
     /***********************************Escalation Process******************************************/
             FUNCTION  XXSVM_GET_REMINDER_COUNT RETURN NUMBER 
             is 
             v_count NUMBER;
             begin    
             select count (EVAL_EMPLOYEE_ID) into v_count  from 
             (
select   ROW_NUMBER() OVER (ORDER BY EVV.EVAL_EMPLOYEE_ID) rn , EVV.EVAL_EMPLOYEE_ID,evaluation_id ,evaluation_name ,evaluation_start_date ,evaluation_end_date ,evv.fk_employee_id,p1.full_name
,p1.employee_number ,u.user_name employeeusername ,supervisor_user.user_name SupervisorUserName, Creator_user.user_name  CreatorUserName ,
trunc (EVALUATION_END_DATE-EVALUATION_START_DATE)starttoendDuration
,trunc (EVALUATION_END_DATE-trunc(sysdate)) DurationtoEnd 
from svm_evaluation  evl  ,svm_eval_evaluators  evv ,per_all_people_f p1 ,fnd_user u ,
per_all_assignments_f paaf ,per_all_people_f p2 ,fnd_user supervisor_user ,fnd_user Creator_user

where  evl.evaluation_id=evv.fk_eval_id
and   is_started='Y' 
and evl.created_by=Creator_user.user_id
and evv.RESPONSE_COMPLETED='N'
and sysdate<evaluation_end_date
and p1.person_id=evv.fk_employee_id
and (sysdate between p1.effective_start_date and p1.effective_end_date)
and p1.PERSON_ID=u.employee_id
AND p1.person_id=paaf.person_id
AND p1.employee_number IS NOT NULL
and supervisor_user.employee_id=p2.person_id
AND TRUNC(SYSDATE) BETWEEN TRUNC(p1.effective_start_date) AND   TRUNC(NVL(p1.effective_end_date,SYSDATE))
AND TRUNC(SYSDATE) BETWEEN TRUNC(p2.effective_start_date) AND  TRUNC(NVL(p2.effective_end_date,SYSDATE))
AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
AND paaf.supervisor_id=p2.person_id

and trunc (EVALUATION_END_DATE-EVALUATION_START_DATE) >=14
 and trunc (EVALUATION_END_DATE-trunc(sysdate))=7
             ) ;
             
             
             return v_count ;
             end ;
             
              PROCEDURE XXSVM_RUN_REMINDER_WORKFLOW(ERRBUF OUT VARCHAR2,
                  RETCODE OUT VARCHAR2)
 is
v_itemtype varchar2(1000);
v_itemkey varchar2(1000);
v_process varchar2(1000);
 v_numofevalutors Number; 



begin 

v_itemtype :='SVM_EVAL';
v_itemkey:='REMIN'||REMINDER_WORKFLOW_SEQ.nextval;

v_process:='SVM_ESCALATION';
  -- Create Process
  wf_engine.createprocess (v_itemtype, v_itemkey, v_process);
  --- Set Attributes 
 
     wf_engine.setitemowner (itemtype      => v_itemtype,
                              itemkey       => v_itemkey,
                              owner         => 'SYSADMIN'
                             );
 

       v_numofevalutors:=XXSVM_WF.XXSVM_GET_REMINDER_COUNT;
                              --WF_ENGINE.SETITEMATTRNUMBER 
                                      wf_engine.SETITEMATTRNUMBER  (itemtype      => v_itemtype,
                                   itemkey       => v_itemkey,
                                   aname         => 'SVM_LOOP_NUM',
                                   avalue        => v_numofevalutors);                    



                 
  
   wf_engine.startprocess (v_itemtype, v_itemkey);
     COMMIT;
   
end ;

procedure LoopCounter(  itemtype   in varchar2,
                        itemkey    in varchar2,
                        actid      in number,
                        funcmode   in varchar2,
                        resultout  in out nocopy varchar2) is
    max_times       pls_integer;
    loop_count      pls_integer;
begin
    --
    -- Do nothing in cancel mode
    --
    if (funcmode <> wf_engine.eng_run) then
        resultout := wf_engine.eng_null;
        return;
    end if;

    -- Get maximum times activity can be executed.
    max_times := wf_engine.GetActivityAttrNumber(
                     itemtype, itemkey, actid, 'MAX_TIMES');
    if ( max_times is null ) then
        wf_core.token('MAX_TIMES',max_times);
        wf_core.raise('WFSQL_ARGS');
    end if;

    begin
        loop_count := wf_engine.GetItemAttrNumber(
                          itemtype, itemkey, 'LOOP_COUNT'||':'||actid);
    exception
        when others then
            --
            -- If item attribute does not exist then create it;
            --
            if ( wf_core.error_name = 'WFENG_ITEM_ATTR' ) then
                wf_engine.AddItemAttr(
                    itemtype,itemkey, 'LOOP_COUNT'||':'||actid);
                loop_count := 0;
                   wf_engine.SetItemAttrNumber (itemtype,
                                            itemkey,
                                            'SVM_LOOP_COUNT',
                                            0);
            else
                raise;
            end if;
    end;

    if ( loop_count >= max_times ) then
        loop_count := 0;
        resultout := 'EXIT';
    else
        loop_count := loop_count +1;
        resultout := 'LOOP';
    end if;

    wf_engine.SetItemAttrNumber(
        itemtype, itemkey, 'LOOP_COUNT'||':'||actid,loop_count);
           wf_engine.SetItemAttrNumber (itemtype,
                                            itemkey,
                                            'SVM_LOOP_COUNT',
                                           loop_count);
exception
    when others then
        wf_core.context('XXSVM_WF','LoopCount',
                        itemtype, itemkey, to_char(actid), funcmode);
        raise;
end loopcounter;
                 procedure SET_ESCALTE_ATTRIBUTES(ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
                 
                 is 
                 v_creator varchar2(2000);
                 v_employee_name varchar2(2000);
                 v_employee_user_name varchar2(2000);
                 v_supervisor_user_name varchar2(2000);
                 v_evaluation_name varchar2(2000);
                 v_evaluation_date varchar2(2000);
                 v_num Number ;
                 begin 
                   v_num:=wf_engine.getitemattrnumber (itemtype      => itemtype,
                                      itemkey       => itemkey,
                                      aname         => 'SVM_LOOP_COUNT'
                                     );
                                     
   select CreatorUserName,full_name,employeeusername ,SupervisorUserName,evaluation_name,evaluation_end_date into v_creator,v_employee_name,v_employee_user_name,v_supervisor_user_name,v_evaluation_name,v_evaluation_date from 
                                     (select   ROW_NUMBER() OVER (ORDER BY EVV.EVAL_EMPLOYEE_ID) rn , EVV.EVAL_EMPLOYEE_ID,evaluation_id ,evaluation_name ,evaluation_start_date ,evaluation_end_date ,evv.fk_employee_id,p1.full_name
,p1.employee_number ,u.user_name employeeusername ,supervisor_user.user_name SupervisorUserName, Creator_user.user_name  CreatorUserName ,
trunc (EVALUATION_END_DATE-EVALUATION_START_DATE)starttoendDuration
,trunc (EVALUATION_END_DATE-trunc(sysdate)) DurationtoEnd 
from svm_evaluation  evl  ,svm_eval_evaluators  evv ,per_all_people_f p1 ,fnd_user u ,
per_all_assignments_f paaf ,per_all_people_f p2 ,fnd_user supervisor_user ,fnd_user Creator_user

where  evl.evaluation_id=evv.fk_eval_id
and   is_started='Y' 
and evl.created_by=Creator_user.user_id
and evv.RESPONSE_COMPLETED='N'
and sysdate<evaluation_end_date
and p1.person_id=evv.fk_employee_id
and (sysdate between p1.effective_start_date and p1.effective_end_date)
and p1.PERSON_ID=u.employee_id
AND p1.person_id=paaf.person_id
AND p1.employee_number IS NOT NULL
and supervisor_user.employee_id=p2.person_id
AND TRUNC(SYSDATE) BETWEEN TRUNC(p1.effective_start_date) AND   TRUNC(NVL(p1.effective_end_date,SYSDATE))
AND TRUNC(SYSDATE) BETWEEN TRUNC(p2.effective_start_date) AND  TRUNC(NVL(p2.effective_end_date,SYSDATE))
AND sysdate BETWEEN paaf.effective_start_date AND paaf.effective_end_date
AND paaf.supervisor_id=p2.person_id
and trunc (EVALUATION_END_DATE-EVALUATION_START_DATE) >=14
and trunc (EVALUATION_END_DATE-trunc(sysdate))=7
)
where rn=v_num;
                 
 wf_engine.setitemattrtext (itemtype,
                                            itemkey,
                                            '#CREATOR',
                                           v_creator);
                                            wf_engine.setitemattrtext (itemtype,
                                            itemkey,
                                            'EMPLOYEE_ESACALTE_USER',
                                           v_employee_user_name);
                                            wf_engine.setitemattrtext (itemtype,
                                            itemkey,
                                            'SUPERVISOR_ESACALTE_USER',
                                           v_supervisor_user_name);
                                            wf_engine.setitemattrtext (itemtype,
                                            itemkey,
                                            'ESACALTE_EMPLOYEE_NAME',
                                           v_employee_name);
                                            wf_engine.setitemattrtext (itemtype,
                                            itemkey,
                                            'EVAL_NAME',
                                           v_evaluation_name);
                                            wf_engine.setitemattrtext(itemtype,
                                            itemkey,
                                            'SVM_EVAL_END_DATE',
                                           v_evaluation_date);
                                             RESULTOUT := 'COMPLETE:';  
                 end       ;   
             
--                procedure LoopCounterSuppliers(  itemtype   in varchar2,
--                        itemkey    in varchar2,
--                        actid      in number,
--                        funcmode   in varchar2,
--                        resultout  in out nocopy varchar2) is
--    max_times       pls_integer;
--    loop_count      pls_integer;
--begin
--null;
--    
--    -- Do nothing in cancel mode
--    --
--    if (funcmode <> wf_engine.eng_run) then
--        resultout := wf_engine.eng_null;
--        return;
--    end if;
--
--    -- Get maximum times activity can be executed.
--    max_times := wf_engine.GetActivityAttrNumber(
--                     itemtype, itemkey, actid, 'NUM_OF_FEED_BACK');
--    if ( max_times is null ) then
--        wf_core.token('NUM_OF_FEED_BACK',max_times);
--        wf_core.raise('WFSQL_ARGS');
--    end if;
--
--    begin
--        loop_count := wf_engine.GetItemAttrNumber(
--                          itemtype, itemkey, 'LOOP_COUNT'||':'||actid);
--    exception
--        when others then
--            --
--            -- If item attribute does not exist then create it;
--            --
--            if ( wf_core.error_name = 'WFENG_ITEM_ATTR' ) then
--                wf_engine.AddItemAttr(
--                    itemtype,itemkey, 'LOOP_COUNT'||':'||actid);
--                loop_count := 0;
--                   wf_engine.SetItemAttrNumber (itemtype,
--                                            itemkey,
--                                            'SVM_LOOP_COUNT',
--                                            0);
--            else
--                raise;
--            end if;
--    end;
--
--    if ( loop_count >= max_times ) then
--        loop_count := 0;
--        resultout := 'EXIT';
--    else
--        loop_count := loop_count +1;
--        resultout := 'LOOP';
--    end if;
--
--    wf_engine.SetItemAttrNumber(
--        itemtype, itemkey, 'LOOP_COUNT'||':'||actid,loop_count);
--           wf_engine.SetItemAttrNumber (itemtype,
--                                            itemkey,
--                                            'SVM_LOOP_COUNT',
--                                           loop_count);
--exception
--    when others then
--        wf_core.context('XXSVM_WF','LoopCount',
--                        itemtype, itemkey, to_char(actid), funcmode);
--        raise;
--end LoopCounterSuppliers;
      PROCEDURE XXSVM_RUN_SUPP_APPROV_WORKFLOW (vcost varchar2,vdelivery varchar2,vquality varchar2,vservice varchar2,vweightscore varchar2,vmeanvalue varchar2,vsuppliername varchar2,vsuppemail varchar2,vyear varchar2)
 is
itemtype varchar2(1000);
itemkey varchar2(1000);
v_process varchar2(1000);
 v_numofevalutors Number; 
 v_year varchar2(5);



begin 

itemtype :='SVM_EVAL';
itemkey:='FeedBackApproval'||XXSVM_APPROVAL_WORKFLOW_SEQ.nextval;

v_process:='SVM_SUPP_APPROVAL';
-- add to check notifictaion year like email Amr Helaly 24-Dec-2018
 select decode(nvl(vyear,0),'0',to_char(sysdate,'YYYY'),vyear) into v_year from dual ;
  -- Create Process
  wf_engine.createprocess (itemtype, itemkey, v_process);
  --- Set Attributes 
 
     wf_engine.setitemowner (itemtype      => itemtype,
                              itemkey       => itemkey,
                              owner         => 'SYSADMIN'
                             );
 

 wf_engine.setitemattrtext (itemtype,   itemkey,  'COST',  vcost);
      wf_engine.setitemattrtext (itemtype, itemkey,'DELIVERY',  vdelivery);
          wf_engine.setitemattrtext (itemtype,  itemkey, 'QUALITY', vquality);
          wf_engine.setitemattrtext (itemtype,  itemkey, 'SERIVCE', vservice);
          wf_engine.setitemattrtext (itemtype,  itemkey,  'WEIGHTSCORE',  vweightscore);
   wf_engine.setitemattrtext(itemtype,  itemkey,  'MEANVALUE',  vmeanvalue);      
    wf_engine.setitemattrtext(itemtype,  itemkey,  'SUPPLIERNAME',vsuppliername);              
   wf_engine.setitemattrtext(itemtype,  itemkey,  'SUPPEMAIL',  vsuppemail);      
    wf_engine.setitemattrtext(itemtype,  itemkey,  'FEEDBACKYEAR',v_year);   


                 
  
   wf_engine.startprocess (itemtype, itemkey);
     COMMIT;
   
end ;
 PROCEDURE APRROVE_REJECT (ITEMTYPE    IN     VARCHAR2,
                                  ITEMKEY     IN     VARCHAR2,
                                  ACTID       IN     NUMBER,
                                  FUNCMODE    IN     VARCHAR2,
                                  RESULTOUT   IN OUT VARCHAR2)
                                  IS
                                    l_result               VARCHAR2 (3000);
                                  v_org_id Number;
                                  V_COST             VARCHAR2 (3000);
                                  V_DELIVERY         VARCHAR2 (3000);
                                  V_QUALITY          VARCHAR2 (3000);
                                  V_SERVICE          VARCHAR2 (3000);
                                  V_WEIGHTSCORE      VARCHAR2 (3000);
                                  V_MEANVALUE        VARCHAR2 (3000);
                                  V_SUPPLIERNAME     VARCHAR2 (3000);
                                  V_SUPPEMAIL        VARCHAR2 (3000);
                                  V_YEAR             VARCHAR2 (3000);
                                  
                                  BEGIN
                                        l_result :=
                 wf_notification.getattrtext (wf_engine.context_nid, 'RESULT');
          
       
                                       IF l_result = 'APPROVED' 
                                       -- added by Amr 24-Dec-2018 duue to multiple calls  
                                       and FUNCMODE='RUN'
      THEN
    ------- get the following attributes as these are the patamters for sendig email 
    ---------
    
--     wf_engine.setitemattrtext (itemtype,   itemkey,  'COST',  vcost);
--      wf_engine.setitemattrtext (itemtype, itemkey,'DELIVERY',  vdelivery);
--          wf_engine.setitemattrtext (itemtype,  itemkey, 'QUALITY', vquality);
--          wf_engine.setitemattrtext (itemtype,  itemkey, 'SERIVCE', vservice);
--          wf_engine.setitemattrtext (itemtype,  itemkey,  'WEIGHTSCORE',  vweightscore);
--   wf_engine.setitemattrtext(itemtype,  itemkey,  'MEANVALUE',  vmeanvalue);      
--    wf_engine.setitemattrtext(itemtype,  itemkey,  'SUPPLIERNAME',vsuppliername);  
--     wf_engine.setitemattrtext(itemtype,  itemkey,  'suppemail',  vsuppemail);      
--    wf_engine.setitemattrtext(itemtype,  itemkey,  'feedbackyear',vyear);  
---- call function svm_notifciation_pkg.send_mail 
    

    V_COST:= wf_engine.getitemattrtext (itemtype,   itemkey,  'COST');
    V_DELIVERY:= wf_engine.getitemattrtext (itemtype,   itemkey,  'DELIVERY');
    V_QUALITY:= wf_engine.getitemattrtext (itemtype,   itemkey,  'QUALITY');
    V_SERVICE:= wf_engine.getitemattrtext (itemtype,   itemkey,  'SERIVCE');
    V_WEIGHTSCORE:= wf_engine.getitemattrtext (itemtype,   itemkey,  'WEIGHTSCORE');
    V_MEANVALUE:= wf_engine.getitemattrtext (itemtype,   itemkey,  'MEANVALUE');
    V_SUPPLIERNAME:= wf_engine.getitemattrtext (itemtype,   itemkey,  'SUPPLIERNAME');
    V_SUPPEMAIL:= wf_engine.getitemattrtext (itemtype,   itemkey,  'SUPPEMAIL');
    V_YEAR:= wf_engine.getitemattrtext (itemtype,   itemkey,  'FEEDBACKYEAR');
    insert into xx_test101 values (V_SUPPLIERNAME);
    SVM_NOTIFICATIONS_PKG.SEND_MAIL (V_SUPPEMAIL,V_COST  ,V_DELIVERY  ,V_QUALITY ,V_SERVICE ,V_WEIGHTSCORE ,V_MEANVALUE ,V_YEAR ) ;



                                         RESULTOUT:= 'APPROVED';
                                        END IF;
                                   IF l_result = 'REJECTED'
                                   
                                   THEN
                                   
                                   if (FUNCMODE='RUN')
                                   then
                                   insert into xx_test101 values ('Rejected');
                                   end if;
                                   
                                    RESULTOUT := 'REJECTED';
                                   
                                   END IF ;
                                   
                                  
                                  END;


function RE_EVALUATE_SUPPLIER (P_EVAL_ID NUMBER ,P_EVAL_NAME varchar2,  P_EVALUATER_USER_ID NUMBER, P_EVALUATER_USER_NAME varchar2 ,P_EVALUATER_FULL_NAME varchar2 , P_SUPPLIER_ID NUMBER , P_RESPONSE_ID NUMBER,P_USER_ID NUMBER, P_justification  varchar2) RETURN VARCHAR2
IS
v_supp_count number :=0;
begin
     
  
select count(*) 
into v_supp_count
from SVM_EVALUATION_RESPONSE
where fk_eval_id=P_EVAL_ID
and fk_supplier_id=P_SUPPLIER_ID 
and nvl(is_re_evalouted,0)>0;

if v_supp_count!=0 then
rollback;
return 'You cannot  evaluate  same supplier in the same evauation again '||v_supp_count||' sup is '||P_SUPPLIER_ID||' eval is '||P_EVAL_ID;

end if;
 INSERT INTO SVM_RE_EVALAUTION VALUES (XXSVM_SVM_RE_EVALAUTION_SEQ.NEXTVAL,P_EVAL_ID,P_RESPONSE_ID,P_USER_ID,SYSDATE,P_justification);
update SVM_EVAL_EVALUATORS
set response_completed='X'
where fk_eval_id=P_EVAL_ID
and fk_employee_id= (select employee_id from fnd_user where user_id =P_EVALUATER_USER_ID and rownum=1);

-- back up old results 
 insert into 
 SVM_RE_EVAL_RESPONSE_VALUES
 (select   *
 from SVM_EVAL_RESPONSE_VALUES
 where fk_response_id=P_RESPONSE_ID);

update SVM_EVAL_RESPONSE_VALUES
set response_rank=0
where fk_response_id=P_RESPONSE_ID;

update SVM_EVALUATION_RESPONSE
set  is_re_evalouted=decode(fk_evaluator_user_id,P_EVALUATER_USER_ID, 1,2)
where fk_eval_id=P_EVAL_ID
and fk_supplier_id=P_SUPPLIER_ID; 
--raise_application_error('-20001','P_SUPPLIER_ID is '||P_SUPPLIER_ID);
  XXSVM_RUN_EVAL_WORKFLOW ( P_EVAL_ID,P_EVAL_NAME ,P_EVALUATER_USER_NAME ,P_EVALUATER_FULL_NAME ,P_EVALUATER_USER_ID ,P_SUPPLIER_ID); 
  commit;    
    RETURN 'SUCEESS';

EXCEPTION WHEN OTHERS
 THEN RETURN SQLERRM;
 END RE_EVALUATE_SUPPLIER;
END ;
/
