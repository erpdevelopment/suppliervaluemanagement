

/* Formatted on 03/Feb/2019 2:39:59 PM (QP5 v5.215.12089.38647) */
CREATE VIEW APPS.XXSVM_REGISTERED_SUPPLIERS
(
   VENDOR_ID,
   SUPPLIERNAME,
   USERNAME,
   USER_ID
)
AS
   SELECT DISTINCT vendor_id,
                   vendor_name SupplierName,
                   username,
                   user_id
     FROM (SELECT hzr.object_id,
                  hp.party_id,
                  hp.person_first_name,
                  hp.person_last_name,
                  hp.party_name,
                  hcpe.email_address,
                  TRIM (
                     (   NVL2 (hcpp.PHONE_AREA_CODE,
                               hcpp.PHONE_AREA_CODE || '-',
                               '')
                      || NVL2 (hcpp.PHONE_NUMBER,
                               hcpp.PHONE_NUMBER || '-',
                               '')
                      || hcpp.PHONE_EXTENSION))
                     AS primary_phone_number
             FROM hz_parties hp,
                  fnd_user fu,
                  hz_relationships hzr,
                  --hz_parties hzr_hp,
                  hz_party_usg_assignments hpua,
                  HZ_CONTACT_POINTS hcpp,
                  HZ_CONTACT_POINTS hcpe
            WHERE     hp.party_id = hzr.subject_id
                  --and hzr.object_id = :1 -- party of supplier
                  AND hzr.relationship_type = 'CONTACT'
                  AND hzr.relationship_code = 'CONTACT_OF'
                  AND hzr.subject_type = 'PERSON'
                  AND hzr.object_type = 'ORGANIZATION'
                  -- commenting for Bug 6076854
                  -- and (hzr.end_date is null or hzr.end_date > sysdate)
                  AND hzr.status IN ('A', 'I')
                  --and hzr_hp.party_id = hzr.party_id
                  AND fu.person_party_id(+) = hp.party_id
                  AND hp.party_id NOT IN
                         (SELECT contact_party_id
                            FROM pos_contact_requests pcr,
                                 pos_supplier_mappings psm
                           WHERE     pcr.request_status = 'PENDING'
                                 AND psm.mapping_id = pcr.mapping_id
                                 --and psm.PARTY_ID = :2
                                 AND contact_party_id IS NOT NULL)
                  AND hpua.party_id = hp.party_id
                  AND hpua.status_flag IN ('A', 'I')
                  AND hpua.party_usage_code = 'SUPPLIER_CONTACT'
                  AND hcpp.OWNER_TABLE_NAME(+) = 'HZ_PARTIES'
                  AND hcpp.OWNER_TABLE_ID(+) = hzr.PARTY_ID
                  AND hcpp.PHONE_LINE_TYPE(+) = 'GEN'
                  AND hcpp.CONTACT_POINT_TYPE(+) = 'PHONE'
                  AND hcpp.primary_flag(+) = 'Y'
                  AND hcpe.OWNER_TABLE_NAME(+) = 'HZ_PARTIES'
                  AND hcpe.OWNER_TABLE_ID(+) = hzr.PARTY_ID
                  AND hcpe.CONTACT_POINT_TYPE(+) = 'EMAIL'
                  AND hcpe.primary_flag(+) = 'Y'
                  AND (hcpe.status IS NULL OR hcpe.status IN ('A', 'I'))
                  AND (hcpp.status IS NULL OR hcpp.status IN ('A', 'I'))) suppliersdetails,
          (SELECT * FROM ap_suppliers) suppliers,
          (SELECT DISTINCT usr.user_id,
                           LOWER (usr.user_name) username,
                           res.RESPONSIBILITY_ID,
                           res.RESPONSIBILITY_NAME
             FROM apps.FND_USER usr,
                  apps.FND_RESPONSIBILITY_TL res,
                  apps.FND_USER_RESP_GROUPS grp
            WHERE     UPPER (res.RESPONSIBILITY_NAME) LIKE
                         UPPER ('MBC Supplier Administrator')
                  AND (SYSDATE BETWEEN usr.start_date
                                   AND NVL (
                                          usr.end_date,
                                          TO_DATE ('31/12/3470',
                                                   'dd/mm/yyyy')))
                  AND res.LANGUAGE = 'US'
                  AND UPPER (res.RESPONSIBILITY_NAME) NOT LIKE '%AX%'
                  AND UPPER (res.RESPONSIBILITY_NAME) NOT LIKE '%OPM%'
                  AND grp.responsibility_id = res.responsibility_id
                  AND grp.user_id = usr.user_id) registered_suppliers,
          (SELECT hps.party_id,
                  hps.party_site_id,
                  hps.party_site_name AS address_type,
                  hzl.address1 AS address
             FROM hz_party_sites hps,
                  hz_locations hzl,
                  fnd_territories_vl fvl,
                  hz_contact_points email,
                  hz_contact_points phone,
                  hz_contact_points fax,
                  hz_party_site_uses pay,
                  hz_party_site_uses pur,
                  hz_party_site_uses rfq
            WHERE     hps.status = 'A'
                  --and hps.party_id = :1
                  --and hps.created_by_module like 'POS%'
                  AND hzl.COUNTRY = fvl.TERRITORY_CODE
                  AND email.owner_table_id(+) = hps.party_site_id
                  AND email.owner_table_name(+) = 'HZ_PARTY_SITES'
                  AND email.status(+) = 'A'
                  AND email.contact_point_type(+) = 'EMAIL'
                  AND email.primary_flag(+) = 'Y'
                  AND phone.owner_table_id(+) = hps.party_site_id
                  AND phone.owner_table_name(+) = 'HZ_PARTY_SITES'
                  AND phone.status(+) = 'A'
                  AND phone.contact_point_type(+) = 'PHONE'
                  AND phone.phone_line_type(+) = 'GEN'
                  AND phone.primary_flag(+) = 'Y'
                  AND fax.owner_table_id(+) = hps.party_site_id
                  AND fax.owner_table_name(+) = 'HZ_PARTY_SITES'
                  AND fax.status(+) = 'A'
                  AND fax.contact_point_type(+) = 'PHONE'
                  AND fax.phone_line_type(+) = 'FAX'
                  AND hps.location_id = hzl.location_id
                  AND pay.party_site_id(+) = hps.party_site_id
                  AND pur.party_site_id(+) = hps.party_site_id
                  AND rfq.party_site_id(+) = hps.party_site_id
                  AND pay.status(+) = 'A'
                  AND pur.status(+) = 'A'
                  AND rfq.status(+) = 'A'
                  AND NVL (pay.end_date(+), SYSDATE) >= SYSDATE
                  AND NVL (pur.end_date(+), SYSDATE) >= SYSDATE
                  AND NVL (rfq.end_date(+), SYSDATE) >= SYSDATE
                  AND NVL (pay.begin_date(+), SYSDATE) <= SYSDATE
                  AND NVL (pur.begin_date(+), SYSDATE) <= SYSDATE
                  AND NVL (rfq.begin_date(+), SYSDATE) <= SYSDATE
                  AND pay.site_use_type(+) = 'PAY'
                  AND pur.site_use_type(+) = 'PURCHASING'
                  AND rfq.site_use_type(+) = 'RFQ'
                  AND NOT EXISTS
                             (SELECT 1
                                FROM pos_address_requests par,
                                     pos_supplier_mappings psm
                               WHERE     psm.party_id = hps.party_id
                                     AND psm.mapping_id = par.MAPPING_ID
                                     AND party_site_id = hps.party_site_id
                                     AND request_status = 'PENDING'
                                     AND request_type IN ('UPDATE', 'DELETE'))) Suppliers_addresses
    WHERE     suppliersdetails.email_address = registered_suppliers.username
          AND suppliers.party_id = suppliersdetails.object_id
          --and suppliersdetails.email_address like '%cp%'
          AND Suppliers_addresses.party_id = suppliersdetails.object_id;
