/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package xxmbc.oracle.apps.pos.svm.webui;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OABodyBean;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;

/**
 * Controller for ...
 */
public class SvmHomeCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
    
         
 
       
       String customJSFunction = "     function callADF(instanceName, adfurl, respId, functionId, pProfileSvm){\n     console.log('url=' + adfurl);\n     console.log('instanceName=' + instanceName);\n     console.log('instanceName Lower=' + instanceName.toLowerCase());\n     console.log('respId=' + respId);\n     console.log('functionId=' + functionId);\n var cookie_string = document.cookie ;\n var actualCookieValue = '';\nconsole.log('cookie_string',cookie_string); \n    if (cookie_string.length != 0) {\n    var cookie_array = cookie_string.split( '; ' );\n    for (i = 0 ; i < cookie_array.length ; i++) {\n console.log('cookie_name=' + instanceName); \n      var cookie_value = cookie_array[i].match ( instanceName + '=(.*)' );\n console.log('cookie_value=' + cookie_value); \n      if (cookie_value != null) {\nconsole.log('actual cookie_value=' + cookie_value[1]); \nactualCookieValue=cookie_value[1]; \nbreak; \n      }\n    }\n  }\n console.log('url=' + adfurl+'?ICXSESSIONID='+actualCookieValue+'&pRespId='+respId+'&pFunctionId='+functionId+'&pProfileSvm='+pProfileSvm);\n     var win = window.open(adfurl+'?ICXSESSIONID='+actualCookieValue+'&pRespId='+respId+'&pFunctionId='+functionId+'&pProfileSvm='+pProfileSvm, '_blank');\n     win.focus();\n      }";
       
       pageContext.putJavaScriptFunction("callADF", customJSFunction);
       OABodyBean bodyBean = (OABodyBean)pageContext.getRootWebBean();
      
 
       String instanceName =pageContext.getProfile("XXMBC_SVM_INSTANCE_NAME") ;
       
       System.out.println("instanceName:" + instanceName);
       String url = pageContext.getProfile("FND_EXTERNAL_ADF_URL");
       int respId = pageContext.getResponsibilityId();
       int functionId = pageContext.getFunctionId();
       System.out.println("respId:" + respId);
       String profileOption = pageContext.getProfile("MBC_SVM_PROF");

       String jsCall = "callADF('" + instanceName + "','" + url + "','" + respId + "','" + functionId + "' , '" + profileOption + "');";
       bodyBean.setOnLoad(jsCall);


  }
  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
  }

}
