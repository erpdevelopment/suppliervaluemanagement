/*===========================================================================+
 |   Copyright (c) 2001, 2005 Oracle Corporation, Redwood Shores, CA, USA    |
 |                         All rights reserved.                              |
 +===========================================================================+
 |  HISTORY                                                                  |
 +===========================================================================*/
package xxmbc.oracle.apps.pos.svm.webui;

import oracle.apps.fnd.common.VersionInfo;
import oracle.apps.fnd.framework.OAException;
import oracle.apps.fnd.framework.webui.OAControllerImpl;
import oracle.apps.fnd.framework.webui.OAPageContext;
import oracle.apps.fnd.framework.webui.beans.OAWebBean;
import oracle.apps.fnd.framework.webui.beans.layout.OARichContainerBean;
import oracle.apps.fnd.framework.webui.beans.message.OAMessageStyledTextBean;

/**
 * Controller for ...
 */
public class xxsvmSurveyCO extends OAControllerImpl
{
  public static final String RCS_ID="$Header$";
  public static final boolean RCS_ID_RECORDED =
        VersionInfo.recordClassVersion(RCS_ID, "%packagename%");

  /**
   * Layout and page setup logic for a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processRequest(pageContext, webBean);
      pageContext.changeResponsibility("MBC_POS_SM_EXTERNAL_ADMIN","POS");
      String UserId=String.valueOf(pageContext.getUserId());
      String UserName=String.valueOf(pageContext.getUserName()); 
      String SueveyID= pageContext.getParameter("svID");
      OARichContainerBean richBean = (OARichContainerBean) webBean.findChildRecursive("adfsurveyrichcontainer");
      richBean.setParameters("&svId="+SueveyID +"&usId="+UserId);
    
//             OAMessageStyledTextBean txt1=(OAMessageStyledTextBean)webBean.findChildRecursive("txt_userid");
//             OAMessageStyledTextBean txt2=(OAMessageStyledTextBean)webBean.findChildRecursive("txt_username");
//             OAMessageStyledTextBean txt3=(OAMessageStyledTextBean)webBean.findChildRecursive("txt_svID");
//             txt1.setValue(pageContext,UserId);
//             txt2.setValue(pageContext,UserName);
//             txt3.setValue(pageContext,SueveyID);
//             OAException message = new OAException(SueveyID,OAException.ERROR);
//             pageContext.putDialogMessage(message);
  }

  /**
   * Procedure to handle form submissions for form elements in
   * a region.
   * @param pageContext the current OA page context
   * @param webBean the web bean corresponding to the region
   */
  public void processFormRequest(OAPageContext pageContext, OAWebBean webBean)
  {
    super.processFormRequest(pageContext, webBean);
  }

}
