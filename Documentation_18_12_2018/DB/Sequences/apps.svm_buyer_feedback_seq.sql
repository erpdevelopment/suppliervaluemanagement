DROP SEQUENCE APPS.SVM_BUYER_FEEDBACK_SEQ;

CREATE SEQUENCE APPS.SVM_BUYER_FEEDBACK_SEQ
  START WITH 41
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
