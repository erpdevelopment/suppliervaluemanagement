DROP SEQUENCE APPS.SVMTEMPLATE_SEQ;

CREATE SEQUENCE APPS.SVMTEMPLATE_SEQ
  START WITH 196
  MAXVALUE 9999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  CACHE 20
  NOORDER;
