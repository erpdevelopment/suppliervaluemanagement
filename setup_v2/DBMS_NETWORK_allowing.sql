Begin
DBMS_NETWORK_ACL_ADMIN.add_privilege (acl          => 'CONNECT-PERMISSIONS.xml',
                                         principal    => 'APPS',
                                         is_grant     => TRUE,
                                         privilege    => 'connect',
                                         start_date   => NULL,
                                         end_date     => NULL);


   DBMS_NETWORK_ACL_ADMIN.ASSIGN_ACL (acl          => 'CONNECT-PERMISSIONS.xml',
                                      HOST         => �SSLSMTP.MBC.NET�,
                                      lower_port   => NULL,
                                      upper_port   => NULL);

   BEGIN
      DBMS_JAVA.grant_permission (
         grantee             => 'APPS',
         permission_type     => 'SYS:java.net.SocketPermission',
         permission_name     => '*',
         permission_action   => 'connect,resolve');
   END;
END;
