CREATE OR REPLACE PACKAGE APPS.SVM_NOTIFICATIONS_PKG
IS
   GC$CRLF                           VARCHAR2 (2) := CHR (13) || CHR (10);
   GC$MIME_TYPE                      VARCHAR2 (128) := 'text/html; charset="windows-1256"';
   --SMTP_HOST                         VARCHAR2 (256) := 'smtpssl.mbc.net';
   SMTP_HOST                         VARCHAR2 (256) := '10.10.98.63';
   --SMTP_HOST                         VARCHAR2 (256) := 'smtp.office365.com';
   SMTP_PORT                         PLS_INTEGER := 25;
--   SMTP_PORT                         PLS_INTEGER := 587;
   --SMTP_DOMAIN                       VARCHAR2 (256) := 'smtpssl.mbc.net';
   SMTP_DOMAIN                       VARCHAR2 (256) := '10.10.98.63';
--   SMTP_DOMAIN                       VARCHAR2 (256) := 'smtp.office365.com';
   MAILER_ID                CONSTANT VARCHAR2 (256) := 'Mailer by Oracle UTL_SMTP';
   GC$BOUNDARY              CONSTANT VARCHAR2 (256) := '__7D81B75CCC90D2974F7A1CBD__';
   FIRST_BOUNDARY           CONSTANT VARCHAR2 (256)
                                        := GC$CRLF || '--' || GC$BOUNDARY || GC$CRLF ;
   LAST_BOUNDARY            CONSTANT VARCHAR2 (256)
      := GC$CRLF || '--' || GC$BOUNDARY || '--' || GC$CRLF ;
   GC$MULTIPART_MIME_TYPE   CONSTANT VARCHAR2 (256)
      :=    'multipart/mixed; '
         || UTL_TCP.CRLF
         || ' boundary= "'
         || GC$BOUNDARY
         || '"'
         || GC$CRLF ;
   MAX_BASE64_LINE_WIDTH    CONSTANT PLS_INTEGER := 76 / 4 * 3;

   FUNCTION GET_ADDRESS (ADDR_LIST IN OUT VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION INIT_MAIL (SENDER       IN VARCHAR2,
                       RECIPIENTS   IN VARCHAR2,
                       CC           IN VARCHAR2 DEFAULT NULL,
                       BCC          IN VARCHAR2 DEFAULT NULL,
                       SUBJECT      IN VARCHAR2,
                       MIME_TYPE    IN VARCHAR2 DEFAULT GC$MIME_TYPE,
                       PRIORITY     IN PLS_INTEGER DEFAULT NULL)
      RETURN UTL_SMTP.CONNECTION;

   PROCEDURE SEND_EMAIL (
      P_FROM_EMAIL       IN     VARCHAR2,
      P_RECEIPTS_EMAIL   IN     VARCHAR2,
      P_CC_EMAIL         IN     VARCHAR2,
      P_BCC_EMAIL        IN     VARCHAR2,
      P_NTF_SUBJECT      IN     VARCHAR2,
      P_NTF_BODY         IN     CLOB,
      P_MIME_TYPE        IN     VARCHAR2 DEFAULT GC$MULTIPART_MIME_TYPE,
      OUT_STATUS            OUT VARCHAR2);

   PROCEDURE PUSH_NOTIFICATION (IN_NOTIFICATION_ID NUMBER);

   PROCEDURE MAIL_JOB;

   PROCEDURE SEND_CALENDAR (P_FROM_EMAIL       IN     VARCHAR2,
                            P_RECEIPTS_EMAIL   IN     VARCHAR2,
                            P_NTF_SUBJECT      IN     VARCHAR2,
                            P_NTF_BODY         IN     CLOB,
                            OUT_STATUS            OUT VARCHAR2);

   PROCEDURE INSERT_NOTIFICATION_API (P_NOTIFICATION_TYPE          VARCHAR2,
                                      P_SEND_SYSTEM                VARCHAR2,
                                      P_SYSTEM_KEY                 VARCHAR2,
                                      P_FROM_EMAIL                 VARCHAR2,
                                      P_RECEIPTS_PERSONID          NUMBER,
                                      P_RECEIPTS_EMAIL             VARCHAR2,
                                      P_CC_EMAIL                   VARCHAR2,
                                      P_BCC_EMAIL                  VARCHAR2,
                                      P_ACCESS_KEY                 VARCHAR2,
                                      P_NTF_SUBJECT                VARCHAR2,
                                      P_NTF_BODY                   CLOB,
                                      OUT_NOTIFICATION_ID   IN OUT NUMBER,
                                      OUT_STATUS               OUT VARCHAR2);

   PROCEDURE SEND_MAIL (pto_email       VARCHAR2,
                        pcost           VARCHAR2,
                        pdelivery       VARCHAR2,
                        pquality        VARCHAR2,
                        pservice        VARCHAR2,
                        pweightscore    VARCHAR2,
                        pmeanvalue      VARCHAR2,
                        pyear           VARCHAR2);

   PROCEDURE SEND_MAIL_TEST;
END SVM_NOTIFICATIONS_PKG;
/